package main

import (
	"log"
	"os"

	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/cli/presenter"
	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/cli/repo"
	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/cli/service"
)

func main() {
	fdb, err := os.OpenFile("./db.json", os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer fdb.Close()

	todoRepo := repo.NewFileTodoRepository(fdb.Name())
	todoService := service.NewTodoService(todoRepo)
	todoPresenter := presenter.NewPresenter(todoService)

	if err = todoPresenter.Present(); err != nil {
		log.Fatal(err)
	}
}
