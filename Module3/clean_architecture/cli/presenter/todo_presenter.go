package presenter

import (
	"fmt"
	"strconv"

	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/cli/model"
	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/cli/service"

	"github.com/rivo/tview"
)

type todoPresenter struct {
	service          service.TodoService
	presenter        *tview.Application
	pages            *tview.Pages
	menu             *tview.List
	listTodosList    *tview.List
	addTodoForm      *tview.Form
	deleteTodoForm   *tview.Form
	completeTodoForm *tview.Form
	updateTodoForm   *tview.Form
}

func NewPresenter(sr service.TodoService) service.TodoPresenter {
	return &todoPresenter{
		service:          sr,
		presenter:        tview.NewApplication(),
		pages:            tview.NewPages(),
		listTodosList:    tview.NewList(),
		menu:             tview.NewList(),
		addTodoForm:      tview.NewForm(),
		deleteTodoForm:   tview.NewForm(),
		completeTodoForm: tview.NewForm(),
		updateTodoForm:   tview.NewForm(),
	}
}

func (tp *todoPresenter) newMenu() {
	tp.menu.
		AddItem("Show todos list", "", 'a', func() {
			tp.listTodosList.Clear()
			tp.ListTodos()
			tp.pages.SwitchToPage("List todos")
		}).
		AddItem("Add todo", "", 'b', func() {
			tp.addTodoForm.Clear(true)
			tp.AddTodo()
			tp.pages.SwitchToPage("Add todo")
		}).
		AddItem("Delete todo", "", 'c', func() {
			tp.deleteTodoForm.Clear(true)
			tp.DeleteTodo()
			tp.pages.SwitchToPage("Delete todo")
		}).
		AddItem("Edit todo", "", 'd', func() {
			tp.updateTodoForm.Clear(true)
			tp.UpdateTodo()
			tp.pages.SwitchToPage("Edit todo")
		}).
		AddItem("Complite todo", "", 'e', func() {
			tp.completeTodoForm.Clear(true)
			tp.CompleteTodo()
			tp.pages.SwitchToPage("Complete todo")
		}).
		AddItem("Exit", "Press to exit", 'f', func() {
			tp.presenter.Stop()
		})
	tp.menu.SetBorder(true).SetTitle(" Make your choice, meatbag ").SetTitleAlign(tview.AlignLeft)
}

func (tp *todoPresenter) Present() error {
	tp.newMenu()
	tp.pages.AddPage("List todos", tp.listTodosList, true, true)
	tp.pages.AddPage("Add todo", tp.addTodoForm, true, true)
	tp.pages.AddPage("Delete todo", tp.deleteTodoForm, true, true)
	tp.pages.AddPage("Edit todo", tp.updateTodoForm, true, true)
	tp.pages.AddPage("Complete todo", tp.completeTodoForm, true, true)
	tp.pages.AddPage("Menu", tp.menu, true, true)

	if err := tp.presenter.SetRoot(tp.pages, true).SetFocus(tp.pages).EnableMouse(true).Run(); err != nil {
		return err
	}
	return nil
}

func (tp *todoPresenter) ListTodos() {
	todos, err := tp.service.ListTodos()
	if err != nil {
		panic(err)
	}

	tp.listTodosList.AddItem("Back", "Press to back", 'f', func() {
		tp.pages.SwitchToPage("Menu")
	})
	for i, todo := range todos {
		tp.listTodosList.AddItem(
			fmt.Sprintf("%d) ID: %d: Title: %s", i, todo.ID, todo.Title),
			fmt.Sprintf("Descr.: %s, Done: %t", todo.Description, todo.IsDone), rune(todo.ID), nil).
			SetSelectedFunc(func(listElIdx int, s1, s2 string, r rune) {
				tp.selected(todos, listElIdx, r)
			})
	}
}

func (tp *todoPresenter) AddTodo() {
	var title string

	tp.addTodoForm.SetBorder(true).SetTitle(" Add todo ").SetTitleAlign(tview.AlignLeft)
	tp.addTodoForm.AddInputField("Title: ", "", 20, nil, func(t string) {
		title = t
	})
	tp.addTodoForm.AddButton("Save", func() {
		if title == "" {
			tp.message("Add todo", "Input field cannot be empty!")
			return
		}

		err := tp.service.CreateTodo(title)
		if err != nil {
			tp.message("Add todo", err.Error())
			return
		}
		tp.message("Menu", "Created")
	})
	tp.addTodoForm.AddButton("Back", func() {
		tp.pages.SwitchToPage("Menu")
	})
}

func (tp *todoPresenter) DeleteTodo() {
	var id int
	var err error

	tp.deleteTodoForm.SetBorder(true).SetTitle(" Delete todo ").SetTitleAlign(tview.AlignLeft)
	tp.deleteTodoForm.AddInputField("ID: ", "", 20, nil, func(t string) {
		if t != "" {
			id, err = strconv.Atoi(t)
			if err != nil {
				tp.message("Delete todo", "Only digits allowed")
				tp.deleteTodoForm.Clear(true)
				tp.DeleteTodo()
			}
		}
	})
	tp.deleteTodoForm.AddButton("Delete", func() {
		if id == 0 {
			tp.message("Delete todo", "Input field cannot be empty!")
			return
		}

		err := tp.service.RemoveTodo(model.Todo{ID: id})
		if err != nil {
			tp.message("Delete todo", err.Error())
			return
		}
		tp.message("Menu", "Deleted")
	})
	tp.deleteTodoForm.AddButton("Back", func() {
		tp.pages.SwitchToPage("Menu")
	})
}

func (tp *todoPresenter) UpdateTodo() {
	var err error
	var todo model.Todo

	tp.updateTodoForm.SetBorder(true).SetTitle(" Edit todo ").SetTitleAlign(tview.AlignLeft)
	tp.updateTodoForm.AddInputField("ID: ", "", 20, nil, func(id string) {
		if id != "" {
			todo.ID, err = strconv.Atoi(id)
			if err != nil {
				tp.message("Edit todo", "Only digits allowed")
				tp.updateTodoForm.Clear(true)
				tp.UpdateTodo()
			}
		}
	})
	tp.updateTodoForm.AddInputField("Tittle: ", "", 20, nil, func(title string) {
		todo.Title = title
	})
	tp.updateTodoForm.AddInputField("Description: ", "", 20, nil, func(desc string) {
		todo.Description = desc
	})
	tp.updateTodoForm.AddCheckbox("IsDone: ", false, func(checked bool) {
		todo.IsDone = checked
	})
	tp.updateTodoForm.AddButton("Update", func() {
		if todo.ID == 0 {
			tp.message("Edit todo", "ID field cannot be empty!")
			return
		}
		if todo.Title == "" {
			tp.message("Edit todo", "Title field cannot be empty!")
			return
		}

		err := tp.service.EditTodo(todo)
		if err != nil {
			tp.message("Edit todo", err.Error())
			return
		}
		tp.message("Menu", "Updated")
	})
	tp.updateTodoForm.AddButton("Back", func() {
		tp.pages.SwitchToPage("Menu")
	})
}

func (tp *todoPresenter) CompleteTodo() {
	var id int
	var err error

	tp.completeTodoForm.SetBorder(true).SetTitle(" Complete todo ").SetTitleAlign(tview.AlignLeft)
	tp.completeTodoForm.AddInputField("ID: ", "", 20, nil, func(t string) {
		if t != "" {
			id, err = strconv.Atoi(t)
			if err != nil {
				tp.message("Complete todo", "Only digits allowed")
				tp.completeTodoForm.Clear(true)
				tp.CompleteTodo()
			}
		}
	})

	tp.completeTodoForm.AddButton("Complete", func() {
		if id == 0 {
			tp.message("Complete todo", "Input field cannot be empty!")
			return
		}

		err := tp.service.CompleteTodo(model.Todo{ID: id})
		if err != nil {
			tp.message("Complete todo", err.Error())
			return
		}
		tp.message("Menu", "Completed")
	})
	tp.completeTodoForm.AddButton("Back", func() {
		tp.pages.SwitchToPage("Menu")
	})
}

func (tp *todoPresenter) selected(todos model.Todos, listElIdx int, r rune) {
	if listElIdx == 0 || r == 'f' {
		tp.pages.SwitchToPage("Menu")
		return
	}
	listElIdx-- // remove "Press to back" item index from list
	tp.pages.AddPage("dd", tview.NewModal().
		SetText(fmt.Sprintf("(Esc) back / Todo ID: %d", todos[listElIdx].ID)).
		AddButtons([]string{"Edit", "Delete", "Complete"}).
		SetDoneFunc(func(buttonIndex int, buttonLabel string) {
			switch buttonIndex {

			// Edit
			case 0:
				form := tview.NewForm()
				form.SetBorder(true).SetTitle(fmt.Sprintf("Update todo ID: %d", todos[listElIdx].ID))
				form.AddInputField("Tittle: ", "", 20, nil, func(title string) {
					todos[listElIdx].Title = title
				})
				form.AddInputField("Description: ", "", 20, nil, func(desc string) {
					todos[listElIdx].Description = desc
				})
				form.AddCheckbox("IsDone: ", false, func(checked bool) {
					todos[listElIdx].IsDone = checked
				})
				form.AddButton("Update", func() {
					if todos[listElIdx].Title == "" {
						tp.message("upd", "Title field cannot be empty!")
						return
					}
					err := tp.service.EditTodo(todos[listElIdx])
					if err != nil {
						tp.message("upd", err.Error())
						return
					}
					tp.listTodosList.Clear()
					tp.ListTodos()
					tp.message("List todos", "Updated")
				})
				form.AddButton("Back", func() {
					tp.pages.SwitchToPage("List todos")
				})
				tp.pages.AddPage("upd", form, true, true)

			// Delete
			case 1:
				err := tp.service.RemoveTodo(todos[listElIdx])
				if err != nil {
					tp.message("List todos", err.Error())
					return
				}
				tp.listTodosList.Clear()
				tp.ListTodos()
				tp.message("List todos", "Deleted")

			// Complete
			case 2:
				err := tp.service.CompleteTodo(todos[listElIdx])
				if err != nil {
					tp.message("List todos", err.Error())
					return
				}
				tp.listTodosList.Clear()
				tp.ListTodos()
				tp.message("List todos", "Completed")
			default:
				tp.pages.SwitchToPage("List todos")
			}
		}), true, true)
}

func (tp *todoPresenter) message(nextPage, message string) {
	tp.pages.AddPage("Warning", tview.NewModal().
		SetText(message).
		AddButtons([]string{"OK"}).
		SetDoneFunc(func(buttonIndex int, buttonLabel string) {
			if buttonIndex == 0 {
				tp.pages.SwitchToPage(nextPage)
			}
		}), true, true)
}
