package repo

import (
	"encoding/json"
	"fmt"
	"io"
	"os"

	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/cli/model"
	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/cli/service"
)

const (
	errNotFoundTodo = "cannot find todo with id %d"
)

// FileTodoRepository is a file-based implementation of TodoRepository
type fileTodoRepository struct {
	FilePath string
}

// NewFileTodoRepository creates a new TodoRepository
func NewFileTodoRepository(filePath string) service.TodoRepository {
	return &fileTodoRepository{
		FilePath: filePath,
	}
}

// GetTodos returns all todos from the repository
func (r *fileTodoRepository) GetTodos() (model.Todos, error) {
	file, err := os.Open(r.FilePath)
	if err != nil {
		return nil, err
	}
	defer func() {
		err = file.Close()
	}()

	fileData, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	if len(fileData) == 0 {
		return nil, nil
	}

	var todos model.Todos

	err = json.Unmarshal(fileData, &todos)
	if err != nil {
		return nil, err
	}

	return todos, nil
}

// GetTodo returns a todo by it ID
func (r *fileTodoRepository) GetTodo(id int) (model.Todo, error) {
	var todo model.Todo

	todos, err := r.GetTodos()
	if err != nil {
		return todo, err
	}

	for _, t := range todos {
		if t.ID == id {
			return t, nil
		}
	}

	return todo, fmt.Errorf(errNotFoundTodo, id)
}

// CreateTodo creates a new todo in the repository
func (r *fileTodoRepository) CreateTodo(todo model.Todo) error {
	todos, err := r.GetTodos()
	if err != nil {
		return err
	}

	todo.ID = len(todos) + 1
	todos = append(todos, todo)

	return r.saveTodos(todos)
}

// UpdateTodo updates an existing todo in the repository
func (r *fileTodoRepository) UpdateTodo(todo model.Todo) error {
	todos, err := r.GetTodos()
	if err != nil {
		return err
	}

	var found bool

	for i, t := range todos {
		if t.ID == todo.ID {
			todos[i] = todo
			found = true
			break
		}
	}

	if !found {
		return fmt.Errorf(errNotFoundTodo, todo.ID)
	}

	return r.saveTodos(todos)
}

// DeleteTodo deletes todo from the repository
func (r *fileTodoRepository) DeleteTodo(id int) error {
	todos, err := r.GetTodos()
	if err != nil {
		return err
	}

	var found bool

	for i, t := range todos {
		if t.ID == id {
			todos = append(todos[:i], todos[i+1:]...)
			found = true
			break
		}
	}

	if !found {
		return fmt.Errorf(errNotFoundTodo, id)
	}

	return r.saveTodos(todos)
}

func (r *fileTodoRepository) saveTodos(todos model.Todos) error {
	fileData, err := json.Marshal(todos)
	if err != nil {
		return err
	}

	return os.WriteFile(r.FilePath, fileData, 0644)
}
