package service

import (
	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/cli/model"
)

// TodoRepository is a repository interface for todos
type TodoRepository interface {
	GetTodos() (model.Todos, error)
	GetTodo(id int) (model.Todo, error)
	CreateTodo(todo model.Todo) error
	UpdateTodo(todo model.Todo) error
	DeleteTodo(id int) error
}

type TodoPresenter interface {
	Present() error
	ListTodos()
	AddTodo()
	DeleteTodo()
	CompleteTodo()
	UpdateTodo()
}
