package service

import (
	"fmt"

	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/cli/model"
)

const (
	errEmptyId    = "todo id cannot be empty"
	errEmptyTitle = "todo title cannot be empty"
)

type TodoService interface {
	ListTodos() (model.Todos, error)
	CreateTodo(title string) error
	EditTodo(todo model.Todo) error
	CompleteTodo(todo model.Todo) error
	RemoveTodo(todo model.Todo) error
}

type todoService struct {
	todoRepo TodoRepository
}

func NewTodoService(tr TodoRepository) TodoService {
	return &todoService{todoRepo: tr}
}

func (s *todoService) ListTodos() (model.Todos, error) {
	todos, err := s.todoRepo.GetTodos()

	if err != nil {
		return nil, err
	}

	return todos, nil
}

func (s *todoService) CreateTodo(title string) error {
	if title == "" {
		return fmt.Errorf(errEmptyTitle)
	}

	var todo model.Todo
	todo.Title = title

	return s.todoRepo.CreateTodo(todo)
}

func (s *todoService) EditTodo(todo model.Todo) error {
	if todo.ID == 0 {
		return fmt.Errorf(errEmptyId)
	}

	return s.todoRepo.UpdateTodo(todo)
}

func (s *todoService) CompleteTodo(todo model.Todo) error {
	if todo.ID == 0 {
		return fmt.Errorf(errEmptyId)
	}

	todo, err := s.todoRepo.GetTodo(todo.ID)
	if err != nil {
		return err
	}

	todo.IsDone = true
	err = s.todoRepo.UpdateTodo(todo)
	if err != nil {
		return err
	}

	return nil
}

func (s *todoService) RemoveTodo(todo model.Todo) error {
	if todo.ID == 0 {
		return fmt.Errorf(errEmptyId)
	}

	return s.todoRepo.DeleteTodo(todo.ID)
}
