package test

import (
	"os"
	"testing"

	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/cli/presenter"
)

func TestListTodos_ReturnsNoError(t *testing.T) {
	t.Parallel()
	testDB, err := os.CreateTemp("", "*_test_todo.json")
	check(err)

	todoService, _ := getTestService(*testDB)
	todoPresenter := presenter.NewPresenter(todoService)

	todos := generateTodos(10)
	for _, todo := range todos {
		check(todoService.CreateTodo(todo.Title))
	}

	todoPresenter.ListTodos()
	todoPresenter.AddTodo()
	todoPresenter.CompleteTodo()
	todoPresenter.DeleteTodo()
	todoPresenter.UpdateTodo()

	check(testDB.Close())
	check(os.Remove(testDB.Name()))
}
