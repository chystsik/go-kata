package test

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/cli/model"
	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/cli/repo"
)

func Test_GetTodos_ReturnsErrorFilePath(t *testing.T) {
	t.Parallel()
	todoRepo := repo.NewFileTodoRepository("")

	todos, err := todoRepo.GetTodos()

	assert.Error(t, err)
	assert.Nil(t, todos)
}

func Test_GetTodos_ReturnsTodos(t *testing.T) {
	t.Parallel()
	testFile, err := os.CreateTemp("", "*_test_todo.json")
	check(err)

	todoRepo := repo.NewFileTodoRepository(testFile.Name())
	todos := generateTodos(10)
	for _, todo := range todos {
		check(todoRepo.CreateTodo(todo))
	}

	actualTodos, errGetTodos := todoRepo.GetTodos()

	check(testFile.Close())
	check(os.Remove(testFile.Name()))

	assert.NoError(t, errGetTodos)
	assert.Equal(t, todos, actualTodos)
}

func Test_GetTodo_ReturnsErrorFilePath(t *testing.T) {
	t.Parallel()
	todoRepo := repo.NewFileTodoRepository("")

	actualTodo, err := todoRepo.GetTodo(0)

	assert.Error(t, err)
	assert.Equal(t, model.Todo{}, actualTodo)
}

func Test_GetTodo_ReturnsErrorNotFoundTodo(t *testing.T) {
	t.Parallel()
	testFile, err := os.CreateTemp("", "*_test_todo.json")
	check(err)

	todoRepo := repo.NewFileTodoRepository(testFile.Name())
	todos := generateTodos(10)
	for _, todo := range todos {
		check(todoRepo.CreateTodo(todo))
	}

	actualTodo, errGetTodo := todoRepo.GetTodo(11)

	check(testFile.Close())
	check(os.Remove(testFile.Name()))

	assert.Error(t, errGetTodo)
	assert.Equal(t, model.Todo{}, actualTodo)
}

func Test_GetTodo_ReturnsTodo(t *testing.T) {
	t.Parallel()
	testFile, err := os.CreateTemp("", "*_test_todo.json")
	check(err)

	todoRepo := repo.NewFileTodoRepository(testFile.Name())
	todos := generateTodos(10)
	for _, todo := range todos {
		check(todoRepo.CreateTodo(todo))
	}

	expectedTodo := todos[4]
	actualTodo, errGetTodo := todoRepo.GetTodo(5)

	check(testFile.Close())
	check(os.Remove(testFile.Name()))

	assert.NoError(t, errGetTodo)
	assert.Equal(t, expectedTodo, actualTodo)
}

func Test_CreateTodo_ReturnsErrFilePath(t *testing.T) {
	t.Parallel()
	todoRepo := repo.NewFileTodoRepository("")

	err := todoRepo.CreateTodo(model.Todo{})

	assert.Error(t, err)
}

func Test_CreateTodoRepo_ReturnsNoError(t *testing.T) {
	t.Parallel()
	testFile, err := os.CreateTemp("", "*_test_todo.json")
	check(err)

	todoRepo := repo.NewFileTodoRepository(testFile.Name())
	expectedTodo := model.Todo{
		Title:       "test title",
		Description: "test description",
	}

	errCreateTodo := todoRepo.CreateTodo(expectedTodo)
	actualTodo, err := todoRepo.GetTodo(1)
	check(err)
	expectedTodo.ID = 1

	check(testFile.Close())
	check(os.Remove(testFile.Name()))

	assert.NoError(t, errCreateTodo)
	assert.Equal(t, expectedTodo, actualTodo)
}

func Test_UpdateTodo_ReturnsErrFilePath(t *testing.T) {
	t.Parallel()
	todoRepo := repo.NewFileTodoRepository("")

	err := todoRepo.UpdateTodo(model.Todo{})

	assert.Error(t, err)
}

func Test_UpdateTodo_ReturnsErrorNotFound(t *testing.T) {
	t.Parallel()
	testFile, err := os.CreateTemp("", "*_test_todo.json")
	check(err)

	todoRepo := repo.NewFileTodoRepository(testFile.Name())
	todos := generateTodos(10)
	for _, todo := range todos {
		check(todoRepo.CreateTodo(todo))
	}

	errUpdateTodo := todoRepo.UpdateTodo(model.Todo{ID: 20})

	check(testFile.Close())
	check(os.Remove(testFile.Name()))

	assert.Error(t, errUpdateTodo)
}

func Test_UpdateTodo_ReturnsNoError(t *testing.T) {
	t.Parallel()
	testFile, err := os.CreateTemp("", "*_test_todo.json")
	check(err)

	todoRepo := repo.NewFileTodoRepository(testFile.Name())
	todos := generateTodos(10)
	for _, todo := range todos {
		check(todoRepo.CreateTodo(todo))
	}

	expectedTodo := model.Todo{
		ID:          7,
		Title:       "updated title",
		Description: "updated description",
	}
	errUpdateTodo := todoRepo.UpdateTodo(expectedTodo)
	actualTodo, err := todoRepo.GetTodo(7)
	check(err)

	check(testFile.Close())
	check(os.Remove(testFile.Name()))

	assert.NoError(t, errUpdateTodo)
	assert.Equal(t, expectedTodo, actualTodo)
}

func Test_DeleteTodo_ReturnsErrFilePath(t *testing.T) {
	t.Parallel()
	todoRepo := repo.NewFileTodoRepository("")

	err := todoRepo.DeleteTodo(1)

	assert.Error(t, err)
}

func Test_DeleteTodo_ReturnsErrNotFoundTodo(t *testing.T) {
	t.Parallel()
	testFile, err := os.CreateTemp("", "*_test_todo.json")
	check(err)

	todoRepo := repo.NewFileTodoRepository(testFile.Name())
	todos := generateTodos(10)
	for _, todo := range todos {
		check(todoRepo.CreateTodo(todo))
	}

	errDeleteTodo := todoRepo.DeleteTodo(20)

	check(testFile.Close())
	check(os.Remove(testFile.Name()))

	assert.Error(t, errDeleteTodo)
}

func Test_DeleteTodo_ReturnsNoError(t *testing.T) {
	t.Parallel()
	testFile, err := os.CreateTemp("", "*_test_todo.json")
	check(err)

	todoRepo := repo.NewFileTodoRepository(testFile.Name())
	todos := generateTodos(10)
	for _, todo := range todos {
		check(todoRepo.CreateTodo(todo))
	}

	expectedTodos := append(todos[:3], todos[4:]...)
	errDeleteTodo := todoRepo.DeleteTodo(4)
	actualTodos, err := todoRepo.GetTodos()
	check(err)

	check(testFile.Close())
	check(os.Remove(testFile.Name()))

	assert.NoError(t, errDeleteTodo)
	assert.Equal(t, expectedTodos, actualTodos)
}
