package main

import (
	"fmt"
	"os"

	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/repo/repository"
)

func main() {
	users := []repository.User{
		{
			ID:   123,
			Name: "Vasily",
		}, {
			ID:   456,
			Name: "Viktor",
		}}

	fdb, err := os.OpenFile("./repository/db.json", os.O_RDWR|os.O_CREATE, 0644)
	check(err)
	defer fdb.Close()

	repo, err := repository.NewUserRepository(fdb)
	check(err)
	fmt.Println(repo.Cache)

	err = repo.Save(repository.User{ID: 999, Name: "John"})
	check(err)

	err = repo.Save(users)
	check(err)

	all, err := repo.FindAll()
	check(err)
	fmt.Println("All", all)

	record, err := repo.Find(999)
	check(err)
	fmt.Println(record)

	nilRecord, err := repo.Find(789789)
	check(err)
	fmt.Println(nilRecord)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
