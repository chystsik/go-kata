// You can edit this code!
// Click here and start typing.
package repository

import (
	"fmt"
	"io"
	"strings"

	jsoniter "github.com/json-iterator/go"
)

const (
	errNotAUser = "input argument should be a User struct or a []User"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File  io.ReadWriteSeeker
	Cache []User
}

func NewUserRepository(file io.ReadWriteSeeker) (*UserRepository, error) {
	fileData, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var cache []User

	if len(fileData) > 0 {
		err = jsoniter.Unmarshal(fileData, &cache)
		if err != nil {
			return nil, err
		}
	}

	return &UserRepository{
		File:  file,
		Cache: cache,
	}, nil
}

type User struct {
	ID   int    `json:"id" fake:"{number:100,999}"`
	Name string `json:"name" fake:"{firstname}"`
}

// Загружать данные в конструкторе

func (r *UserRepository) Save(record interface{}) (err error) {
	// logic to write a record as a JSON object to a file at r.FilePath
	emptyFile := len(r.Cache) == 0

	// defining the type of received data
	switch v := record.(type) {
	case User:
		record = v
		r.Cache = append(r.Cache, v)
	case []User:
		record = v
		r.Cache = append(r.Cache, v...)
	default:
		return fmt.Errorf("%s", errNotAUser)
	}

	// manipulations with brackets when appending users to file without overwriting it
	if emptyFile {
		_, err = r.File.Write([]byte("["))
		if err != nil {
			return err
		}
	} else {
		_, err = r.File.Seek(int64(-1), io.SeekEnd)
		if err != nil {
			return err
		}
		_, err = r.File.Write([]byte(","))
		if err != nil {
			return err
		}
	}
	defer func() {
		_, err = r.File.Write([]byte("]"))
	}()

	// marshaling data
	dataBytes, err := jsoniter.Marshal(record)
	if err != nil {
		return err
	}

	// remove square brackets when marshaling slice of users
	dataBytes = []byte(strings.Trim(string(dataBytes), "[]"))

	// write to file
	_, err = r.File.Write(dataBytes)
	if err != nil {
		return err
	}

	return nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	// logic to read a JSON object from a file at r.FilePath and return the corresponding record
	for _, record := range r.Cache {
		if record.ID == id {
			return record, nil
		}
	}
	return nil, nil
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	// logic to read all JSON objects from a file at r.FilePath and return a slice of records
	result := make([]interface{}, len(r.Cache))
	for i := range r.Cache {
		result[i] = r.Cache[i]
	}
	return result, nil
}
