// You can edit this code!

// Click here and start typing.

package repository

import (
	"encoding/json"
	"io"
	"os"
	"reflect"
	"testing"

	"github.com/brianvoe/gofakeit/v6"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func generateUsers(count int) []User {
	faker := gofakeit.New(0)
	users := make([]User, count)

	for i := 0; i < count; i++ {
		err := faker.Struct(&users[i])
		if err != nil {
			panic(err)
		}
	}
	return users
}

func generateNotUsers(count int) []NotUser {
	faker := gofakeit.New(0)
	users := make([]NotUser, count)

	for i := 0; i < count; i++ {
		err := faker.Struct(&users[i])
		if err != nil {
			panic(err)
		}
	}
	return users
}

func saveUsersToFile(f io.ReadWriteSeeker, u interface{}) {
	err := json.NewEncoder(f).Encode(&u)
	if err != nil {
		panic(err)
	}
}

type NotUser struct {
	UUID  string `json:"id" fake:"{uuid}"`
	Name  string `json:"name" fake:"{firstname}"`
	Color string `json:"color" fake:"{color}"`
}

func TestNewUserRepository(t *testing.T) {
	var wrongFilePath *os.File

	// prepare empty file
	emptyFile, err := os.CreateTemp("", "*_fdb.json")
	check(err)
	emptyFileName := emptyFile.Name()
	defer emptyFile.Close()

	// prepare valid file with valid users
	notEmptyFile, err := os.CreateTemp("", "*u_fdb.json")
	check(err)
	defer notEmptyFile.Close()

	notEmptyFileName := notEmptyFile.Name()
	users := generateUsers(10)

	saveUsersToFile(notEmptyFile, users)
	notEmptyFile.Close()

	fdb, err := os.OpenFile(notEmptyFileName, os.O_RDWR, 0644)
	check(err)
	defer fdb.Close()

	// prepare valid file with not valid records
	notUserStructFile, err := os.CreateTemp("", "*u_fdb.json")
	check(err)
	defer notEmptyFile.Close()

	notUserStructFileName := notUserStructFile.Name()
	notUsers := generateNotUsers(10)

	saveUsersToFile(notUserStructFile, notUsers)
	notEmptyFile.Close()

	nfdb, err := os.OpenFile(notUserStructFileName, os.O_RDWR, 0644)
	check(err)
	defer nfdb.Close()

	// remove all temp files
	defer os.Remove(notEmptyFileName)
	defer os.Remove(emptyFileName)
	defer os.Remove(notUserStructFileName)

	type args struct {
		file io.ReadWriteSeeker
	}
	tests := []struct {
		name    string
		args    args
		want    *UserRepository
		wantErr bool
	}{
		{
			name: "file not found",
			args: args{
				file: wrongFilePath,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "empty file",
			args: args{
				file: emptyFile,
			},
			want:    &UserRepository{File: emptyFile},
			wantErr: false,
		},
		{
			name: "not empty file",
			args: args{
				file: fdb,
			},
			want:    &UserRepository{File: fdb, Cache: users},
			wantErr: false,
		},
		{
			name: "not user struct",
			args: args{
				file: nfdb,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewUserRepository(tt.args.file)
			if (err != nil) != tt.wantErr {
				t.Errorf("Test: %s, NewUserRepository() error = %v, wantErr %v", tt.name, err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, NewUserRepository() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}

func TestUserRepository_Save(t *testing.T) {
	var wrongFilePath *os.File

	// prepare empty file
	emptyFile, err := os.CreateTemp("", "*_fdb.json")
	check(err)
	emptyFileName := emptyFile.Name()
	defer emptyFile.Close()

	emptyFileRepo, err := NewUserRepository(emptyFile)
	check(err)

	// prepare valid file with valid users
	notEmptyFile, err := os.CreateTemp("", "*u_fdb.json")
	check(err)
	defer notEmptyFile.Close()

	notEmptyFileName := notEmptyFile.Name()
	users := generateUsers(10)

	saveUsersToFile(notEmptyFile, users)
	notEmptyFile.Close()

	fdb, err := os.OpenFile(notEmptyFileName, os.O_RDWR, 0644)
	check(err)
	defer fdb.Close()

	notEmptyFileRepo, err := NewUserRepository(fdb)
	check(err)

	// prepare valid file with not valid records
	notUserStructFile, err := os.CreateTemp("", "*u_fdb.json")
	check(err)
	defer notEmptyFile.Close()

	notUserStructFileName := notUserStructFile.Name()
	notUsers := generateNotUsers(10)

	saveUsersToFile(notUserStructFile, notUsers)
	notEmptyFile.Close()

	nfdb, err := os.OpenFile(notUserStructFileName, os.O_RDWR, 0644)
	check(err)
	defer nfdb.Close()

	// remove all temp files
	defer os.Remove(notEmptyFileName)
	defer os.Remove(emptyFileName)
	defer os.Remove(notUserStructFileName)

	type args struct {
		record interface{}
	}
	tests := []struct {
		name    string
		r       *UserRepository
		args    args
		wantErr bool
	}{
		{
			name: "append to wrong file path",
			r:    &UserRepository{File: wrongFilePath, Cache: []User{}},
			args: args{
				record: User{
					ID:   123,
					Name: "Vasily",
				},
			},
			wantErr: true,
		},
		{
			name: "append to empty file",
			r:    emptyFileRepo,
			args: args{
				record: User{
					ID:   123,
					Name: "Vasily",
				},
			},
			wantErr: false,
		},
		{
			name: "append User to file with existing records",
			r:    notEmptyFileRepo,
			args: args{
				record: User{
					ID:   123,
					Name: "Vasily",
				},
			},
			wantErr: false,
		},
		{
			name: "append []User to file with existing records",
			r:    notEmptyFileRepo,
			args: args{
				record: users,
			},
			wantErr: false,
		},
		{
			name: "append not User struct to file",
			r:    notEmptyFileRepo,
			args: args{
				record: notUsers,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.r.Save(tt.args.record); (err != nil) != tt.wantErr {
				t.Errorf("Test: %s, UserRepository.Save() error = %v, wantErr %v", tt.name, err, tt.wantErr)
			}
		})
	}
}

func TestUserRepository_Find(t *testing.T) {
	// prepare valid file with valid users
	notEmptyFile, err := os.CreateTemp("", "*u_fdb.json")
	check(err)
	defer notEmptyFile.Close()

	notEmptyFileName := notEmptyFile.Name()
	users := generateUsers(10)

	saveUsersToFile(notEmptyFile, users)
	notEmptyFile.Close()

	fdb, err := os.OpenFile(notEmptyFileName, os.O_RDWR, 0644)
	check(err)
	defer fdb.Close()

	notEmptyFileRepo, err := NewUserRepository(fdb)
	check(err)

	os.Remove(notEmptyFileName)

	type args struct {
		id int
	}
	tests := []struct {
		name    string
		r       *UserRepository
		args    args
		want    interface{}
		wantErr bool
	}{
		{
			name:    "returns record",
			r:       notEmptyFileRepo,
			args:    args{users[0].ID},
			want:    users[0],
			wantErr: false,
		},
		{
			name:    "returns nil",
			r:       notEmptyFileRepo,
			args:    args{1000},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.r.Find(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Test: %s, UserRepository.Find() error = %v, wantErr %v", tt.name, err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, UserRepository.Find() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	// prepare valid file with valid users
	notEmptyFile, err := os.CreateTemp("", "*u_fdb.json")
	check(err)
	defer notEmptyFile.Close()

	notEmptyFileName := notEmptyFile.Name()
	users := generateUsers(10)

	expected := make([]interface{}, len(users))
	for i := range users {
		expected[i] = users[i]
	}

	saveUsersToFile(notEmptyFile, users)
	notEmptyFile.Close()

	fdb, err := os.OpenFile(notEmptyFileName, os.O_RDWR, 0644)
	check(err)
	defer fdb.Close()

	notEmptyFileRepo, err := NewUserRepository(fdb)
	check(err)

	os.Remove(notEmptyFileName)

	tests := []struct {
		name    string
		r       *UserRepository
		want    []interface{}
		wantErr bool
	}{
		{
			name:    "returns all records",
			r:       notEmptyFileRepo,
			want:    expected,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.r.FindAll()
			if (err != nil) != tt.wantErr {
				t.Errorf("Test: %s, UserRepository.FindAll() error = %v, wantErr %v", tt.name, err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, UserRepository.FindAll() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
