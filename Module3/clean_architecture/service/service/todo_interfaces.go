package service

import "gitlab.com/chystsik/go-kata/Module3/clean_architecture/service/model"

// TodoRepository is a repository interface for todos
type TodoRepository interface {
	GetTodos() (model.Todos, error)
	GetTodo(id int) (model.Todo, error)
	CreateTodo(todo model.Todo) error
	UpdateTodo(todo model.Todo) error
	DeleteTodo(id int) error
}
