package test

import (
	"os"
	"testing"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/stretchr/testify/assert"
	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/service/model"
	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/service/repo"
	"gitlab.com/chystsik/go-kata/Module3/clean_architecture/service/service"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func Test_ListTodo_ReturnsTodos(t *testing.T) {
	t.Parallel()
	testFile, err := os.CreateTemp("", "*_test_todo.json")
	check(err)

	todoService, todoRepo := getTestService(*testFile)
	todos := generateTodos(10)
	for _, todo := range todos {
		check(todoRepo.CreateTodo(todo))
	}

	actual, errListTodo := todoService.ListTodos()

	check(testFile.Close())
	check(os.Remove(testFile.Name()))

	assert.NoError(t, errListTodo)
	assert.Equal(t, todos, actual)
}

func Test_ListTodo_WhenRepo_ReturnsError(t *testing.T) {
	t.Parallel()
	todoService := service.NewTodoService(repo.NewFileTodoRepository(""))

	actual, errListTodo := todoService.ListTodos()

	assert.Error(t, errListTodo)
	assert.Nil(t, actual)
}

func Test_CreateTodo_WhenService_ReturnsEmptyTitleError(t *testing.T) {
	t.Parallel()
	todoService := service.NewTodoService(repo.NewFileTodoRepository(""))

	err := todoService.CreateTodo("")

	assert.Error(t, err)
}

func Test_CreateTodo_ReturnsNoError(t *testing.T) {
	t.Parallel()
	testFile, err := os.CreateTemp("", "*_test_todo.json")
	if err != nil {
		panic(err)
	}

	expectedTodoId := 1
	expextedTodoTitle := "test"
	todoService, todoRepo := getTestService(*testFile)
	errCreateTodo := todoService.CreateTodo(expextedTodoTitle)

	actualTodo, err := todoRepo.GetTodo(expectedTodoId)
	check(err)

	check(testFile.Close())
	check(os.Remove(testFile.Name()))

	assert.NoError(t, errCreateTodo)
	assert.Equal(t, expectedTodoId, actualTodo.ID)
	assert.Equal(t, expextedTodoTitle, actualTodo.Title)
}

func Test_CopmleteToto_WhenService_ReturnsErrorEmptyId(t *testing.T) {
	t.Parallel()
	todoService := service.NewTodoService(repo.NewFileTodoRepository(""))

	err := todoService.CompleteTodo(model.Todo{})

	assert.Error(t, err)
}

func Test_CopmleteToto_WhenRepo_ReturnsError(t *testing.T) {
	t.Parallel()
	todoService := service.NewTodoService(repo.NewFileTodoRepository(""))

	err := todoService.CompleteTodo(model.Todo{ID: 1})

	assert.Error(t, err)
}

func Test_CopmleteToto_ReturnsNoError(t *testing.T) {
	t.Parallel()
	testFile, err := os.CreateTemp("", "*_test_todo.json")
	check(err)

	todoService, todoRepo := getTestService(*testFile)
	todos := generateTodos(10)
	for _, todo := range todos {
		check(todoRepo.CreateTodo(todo))
	}
	expectedTodo := todos[5]

	errCompleteTodo := todoService.CompleteTodo(expectedTodo)
	actualTodo, err := todoRepo.GetTodo(expectedTodo.ID)
	check(err)
	expectedTodo.IsDone = true

	check(testFile.Close())
	check(os.Remove(testFile.Name()))

	assert.NoError(t, errCompleteTodo)
	assert.Equal(t, expectedTodo, actualTodo)
}

func Test_RemoveTodo_WhenService_ReturnsErrorEmptyId(t *testing.T) {
	t.Parallel()
	todoService := service.NewTodoService(repo.NewFileTodoRepository(""))

	err := todoService.RemoveTodo(model.Todo{})

	assert.Error(t, err)
}

func Test_RemoveTodo_ReturnsNoError(t *testing.T) {
	testFile, err := os.CreateTemp("", "*_test_todo.json")
	check(err)

	todoService, todoRepo := getTestService(*testFile)
	todos := generateTodos(10)
	for _, todo := range todos {
		check(todoRepo.CreateTodo(todo))
	}
	expectedTodo := todos[5]
	expectedTodos := append(todos[:5], todos[6:]...)

	errRemoveTodo := todoService.RemoveTodo(expectedTodo)
	actualTodos, err := todoRepo.GetTodos()
	check(err)

	check(testFile.Close())
	check(os.Remove(testFile.Name()))

	assert.NoError(t, errRemoveTodo)
	assert.Equal(t, expectedTodos, actualTodos)
}

func getTestService(testFile os.File) (service.TodoService, service.TodoRepository) {
	repo := repo.NewFileTodoRepository(testFile.Name())
	return service.NewTodoService(repo), repo
}

func generateTodos(count int) model.Todos {
	todos := make(model.Todos, count)

	for i := range todos {
		todos[i].ID = i + 1
		todos[i].Title = gofakeit.VerbAction()
		todos[i].Description = gofakeit.Sentence(3)
	}
	return todos
}
