package easy

// https://leetcode.com/problems/n-th-tribonacci-number/

func tribonacci(n int) int {
	if n < 2 {
		return n
	} else if n == 2 {
		return 1
	}

	zero, one, two := 0, 1, 1
	var tri int

	for ; n > 2; n-- {
		tri = zero + one + two
		zero, one, two = one, two, tri
	}

	return tri
}
