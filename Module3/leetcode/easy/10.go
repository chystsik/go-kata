package easy

// https://leetcode.com/problems/shuffle-the-array/

func shuffle(nums []int, n int) []int {
	result := make([]int, len(nums))
	for i, j := 0, 0; i < len(result); i, j = i+2, j+1 {
		result[i], result[i+1] = nums[j], nums[j+n]
	}
	return result
}
