package easy

// https://leetcode.com/problems/jewels-and-stones/

func numJewelsInStones(jewels string, stones string) int {
	jewelsMap := [58]bool{}
	jewelsSum := 0

	for i := 0; i < len(jewels); i++ {
		jewelsMap[jewels[i]-'A'] = true
	}

	for i := 0; i < len(stones); i++ {
		if jewelsMap[stones[i]-'A'] {
			jewelsSum++
		}
	}

	return jewelsSum
}

func numJewelsInStonesMap(jewels string, stones string) int {
	jewelsMap := make(map[byte]struct{})
	jewelsSum := 0

	for i := 0; i < len(jewels); i++ {
		jewelsMap[jewels[i]] = struct{}{}
	}

	for i := 0; i < len(stones); i++ {
		if _, ok := jewelsMap[stones[i]]; ok {
			jewelsSum++
		}
	}

	return jewelsSum
}
