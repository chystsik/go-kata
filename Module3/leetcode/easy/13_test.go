package easy

import (
	"math/rand"
	"testing"
)

var genJewels, genStones = generate(3, 10000)

func BenchmarkMap(b *testing.B) {
	for i := 0; i < b.N; i++ {
		numJewelsInStonesMap(genJewels, genStones)
	}
}

func BenchmarkArray(b *testing.B) {
	for i := 0; i < b.N; i++ {
		numJewelsInStones(genJewels, genStones)
	}
}

func generate(jLen, sLen int) (string, string) {
	jewels := make([]byte, jLen)
	stones := make([]byte, sLen)
	unique := [58]bool{}

	for i := 0; i < jLen; i++ {
		jewel := byte(rand.Intn(58)) + 'A'
		if unique[jewel-'A'] || (90 < jewel && jewel < 97) {
			i--
			continue
		}
		unique[jewel-'A'] = true
		jewels[i] = jewel
	}

	for i := 0; i < sLen; i++ {
		stone := byte(rand.Intn(58)) + 'A'
		if 90 < stone && stone < 97 {
			i--
			continue
		}
		stones[i] = stone
	}

	return string(jewels), string(stones)
}

func Test_numJewelsInStones(t *testing.T) {
	type args struct {
		jewels string
		stones string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "jewels = \"aA\", stones = \"aAAbbbb\"",
			args: args{
				jewels: "aA",
				stones: "aAAbbbb",
			},
			want: 3,
		},
		{
			name: "jewels = \"z\", stones = \"ZZ\"",
			args: args{
				jewels: "z",
				stones: "ZZ",
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := numJewelsInStones(tt.args.jewels, tt.args.stones); got != tt.want {
				t.Errorf("Test: %s, numJewelsInStones() = %v, want %v", tt.name, got, tt.want)
			}
		})
		t.Run(tt.name, func(t *testing.T) {
			if got := numJewelsInStonesMap(tt.args.jewels, tt.args.stones); got != tt.want {
				t.Errorf("Test: %s, numJewelsInStones() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
