package easy

// https://leetcode.com/problems/richest-customer-wealth/

func maximumWealth(accounts [][]int) int {
	maxWealth := 0

	for _, account := range accounts {
		var currWealth int
		for _, wealth := range account {
			currWealth += wealth
		}
		if currWealth > maxWealth {
			maxWealth = currWealth
		}
	}

	return maxWealth
}
