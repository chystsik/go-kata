package easy

import "testing"

func Test_maximumWealth(t *testing.T) {
	type args struct {
		accounts [][]int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "accounts = [[1,2,3],[3,2,1]]",
			args: args{[][]int{{1, 2, 3}, {3, 2, 1}}},
			want: 6,
		},
		{
			name: "accounts = [[1,5],[7,3],[3,5]]",
			args: args{[][]int{{1, 5}, {7, 3}, {3, 5}}},
			want: 10,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := maximumWealth(tt.args.accounts); got != tt.want {
				t.Errorf("Test: %s, maximumWealth() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
