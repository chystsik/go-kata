package easy

// https://leetcode.com/problems/design-parking-system/
// big, medium, or small, which are represented by 1, 2, and 3 respectively

type ParkingSystem struct {
	spaces [3]slots
}

type slots int

func Constructor(big int, medium int, small int) ParkingSystem {
	return ParkingSystem{spaces: [3]slots{slots(big), slots(medium), slots(small)}}
}

func (p *ParkingSystem) AddCar(carType int) bool {
	if p.spaces[carType-1] > 0 {
		p.spaces[carType-1]--
		return true
	}
	return false
}

/**
 * Your ParkingSystem object will be instantiated and called as such:
 * obj := Constructor(big, medium, small);
 * param_1 := obj.AddCar(carType);
 */
