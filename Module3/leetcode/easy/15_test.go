package easy

import "testing"

func TestParkingSystem_AddCar(t *testing.T) {
	type args struct {
		carType []int
	}
	tests := []struct {
		name string
		p    ParkingSystem
		args args
		want []bool
	}{
		{
			name: "test 1",
			p:    Constructor(1, 1, 0),
			args: args{carType: []int{1, 2, 3, 1}},
			want: []bool{true, true, false, false},
		},
		{
			name: "test 2",
			p:    Constructor(1, 1, 1),
			args: args{carType: []int{1, 2, 3, 1, 3}},
			want: []bool{true, true, true, false, false},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for i, car := range tt.args.carType {
				if got := tt.p.AddCar(car); got != tt.want[i] {
					t.Errorf("Test: %s, ParkingSystem.AddCar(%d) = %v, want %v", tt.name, car, got, tt.want[i])
				}
			}
		})
	}
}
