package easy

import "testing"

func Test_smallestEvenMultiple(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "n = 5",
			args: args{5},
			want: 10,
		},
		{
			name: "n = 6",
			args: args{6},
			want: 6,
		},
		{
			name: "n = 38",
			args: args{38},
			want: 38,
		},
		{
			name: "n = 39",
			args: args{39},
			want: 78,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := smallestEvenMultiple(tt.args.n); got != tt.want {
				t.Errorf("Test: %s, smallestEvenMultiple() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
