package easy

import (
	"strings"
)

// https://leetcode.com/problems/maximum-number-of-words-found-in-sentences/

func mostWordsFound(sentences []string) int {
	max := 0
	for _, sentence := range sentences {
		curr := strings.Split(sentence, " ")
		if len(curr) > max {
			max = len(curr)
		}
	}
	return max
}
