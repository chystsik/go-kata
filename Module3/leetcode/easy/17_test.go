package easy

import "testing"

func Test_mostWordsFound(t *testing.T) {
	type args struct {
		sentences []string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "test 1",
			args: args{[]string{"alice and bob love leetcode", "i think so too", "this is great thanks very much"}},
			want: 6,
		},
		{
			name: "test 2",
			args: args{[]string{"please wait", "continue to fight", "continue to win"}},
			want: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := mostWordsFound(tt.args.sentences); got != tt.want {
				t.Errorf("Test: %s, mostWordsFound() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
