package easy

// https://leetcode.com/problems/difference-between-element-sum-and-digit-sum-of-an-array/

func differenceOfSum(nums []int) int {
	elementSum, digitSum := 0, 0

	for _, num := range nums {
		elementSum += num
		for num >= 10 {
			digitSum += num % 10
			num = num / 10
		}
		digitSum += num
	}
	return elementSum - digitSum
}
