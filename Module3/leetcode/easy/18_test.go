package easy

import "testing"

func Test_differenceOfSum(t *testing.T) {
	type args struct {
		nums []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "nums = [1,15,6,3]",
			args: args{[]int{1, 15, 6, 3}},
			want: 9,
		},
		{
			name: "nums = [1,2,3,4]",
			args: args{[]int{1, 2, 3, 4}},
			want: 0,
		},
		{
			name: "nums = [2,7,8,10,8,10,1,10,5,9]",
			args: args{[]int{2, 7, 8, 10, 8, 10, 1, 10, 5, 9}},
			want: 27,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := differenceOfSum(tt.args.nums); got != tt.want {
				t.Errorf("Test: %s, differenceOfSum() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
