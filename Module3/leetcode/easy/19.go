package easy

import (
	"sort"
)

// https://leetcode.com/problems/minimum-sum-of-four-digit-number-after-splitting-digits/

func minimumSum(num int) int {
	res := [4]int{}
	for i := 0; num > 0; i, num = i+1, num/10 {
		res[i] = num % 10
	}

	sort.Ints(res[:])
	return (res[0]+res[1])*10 + res[2] + res[3]
}
