package easy

import "testing"

func Test_minimumSum(t *testing.T) {
	type args struct {
		num int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "num = 2932",
			args: args{2932},
			want: 52,
		},
		{
			name: "num = 4009",
			args: args{4009},
			want: 13,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := minimumSum(tt.args.num); got != tt.want {
				t.Errorf("Test: %s, minimumSum() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
