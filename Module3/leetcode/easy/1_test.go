package easy

import "testing"

func Test_tribonacci(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "n is 0",
			args: args{0},
			want: 0,
		},
		{
			name: "n is 1",
			args: args{1},
			want: 1,
		},
		{
			name: "n is 2",
			args: args{2},
			want: 1,
		},
		{
			name: "n is 3",
			args: args{3},
			want: 2,
		},
		{
			name: "n is 4",
			args: args{4},
			want: 4,
		},
		{
			name: "n is 25",
			args: args{25},
			want: 1389537,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tribonacci(tt.args.n); got != tt.want {
				t.Errorf("Test: %s, tribonacci() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
