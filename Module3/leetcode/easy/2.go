package easy

// https://leetcode.com/problems/concatenation-of-array/

func getConcatenation(nums []int) []int {
	return append(nums, nums...)
}
