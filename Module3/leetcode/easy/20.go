package easy

// https://leetcode.com/problems/kids-with-the-greatest-number-of-candies/description/

func kidsWithCandies(candies []int, extraCandies int) []bool {
	greatest := 0

	for _, candy := range candies {
		if candy > greatest {
			greatest = candy
		}
	}

	result := make([]bool, len(candies))

	for i, candy := range candies {
		if candy+extraCandies >= greatest {
			result[i] = true
		}
	}

	return result
}
