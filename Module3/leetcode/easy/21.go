package easy

// https://leetcode.com/problems/subtract-the-product-and-sum-of-digits-of-an-integer/

func subtractProductAndSum(n int) int {
	product, sum := 1, 0

	for ; n > 0; n = n / 10 {
		product *= n % 10
		sum += n % 10
	}

	return product - sum
}
