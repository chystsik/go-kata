package easy

import "testing"

func Test_subtractProductAndSum(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "n = 234",
			args: args{234},
			want: 15,
		},
		{
			name: "n = 4421",
			args: args{4421},
			want: 21,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := subtractProductAndSum(tt.args.n); got != tt.want {
				t.Errorf("Test: %s, subtractProductAndSum() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
