package easy

import "sort"

// https://leetcode.com/problems/how-many-numbers-are-smaller-than-the-current-number/

func smallerNumbersThanCurrent(nums []int) []int {
	tmp := make([]int, len(nums))
	hash := make(map[int]int, len(nums))

	copy(tmp, nums)
	sort.Ints(tmp)

	for i, num := range tmp {
		if _, ok := hash[num]; !ok {
			hash[num] = i
		}
	}

	for i, num := range nums {
		nums[i] = hash[num]
	}

	return nums
}
