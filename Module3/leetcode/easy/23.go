package easy

import "strings"

// https://leetcode.com/problems/goal-parser-interpretation/

func interpret(command string) string {
	r := strings.NewReplacer("()", "o", "(al)", "al")
	return r.Replace(command)
}
