package easy

import "testing"

func Test_interpret(t *testing.T) {
	type args struct {
		command string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "command = \"G()(al)\"",
			args: args{command: "G()(al)"},
			want: "Goal",
		},
		{
			name: "command = \"G()()()()(al)\"",
			args: args{command: "G()()()()(al)"},
			want: "Gooooal",
		},
		{
			name: "command = \"(al)G(al)()()G\"",
			args: args{command: "(al)G(al)()()G"},
			want: "alGalooG",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := interpret(tt.args.command); got != tt.want {
				t.Errorf("Test: %s, interpret() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
