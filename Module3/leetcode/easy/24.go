package easy

// https://leetcode.com/problems/decode-xored-array/

func decode(encoded []int, first int) []int {
	result := make([]int, len(encoded)+1)
	result[0] = first

	for i, val := range encoded {
		result[i+1] = result[i] ^ val
	}

	return result
}
