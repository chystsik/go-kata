package easy

// https://leetcode.com/problems/create-target-array-in-the-given-order/

func createTargetArray(nums []int, index []int) []int {
	target := make([]int, 0, len(index))

	for i, idx := range index {
		target = append(target[:idx+1], target[idx:]...)
		target[idx] = nums[i]
	}

	return target
}
