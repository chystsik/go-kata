package easy

import (
	"reflect"
	"testing"
)

func Test_createTargetArray(t *testing.T) {
	type args struct {
		nums  []int
		index []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "nums = [0,1,2,3,4], index = [0,1,2,2,1]",
			args: args{
				nums:  []int{0, 1, 2, 3, 4},
				index: []int{0, 1, 2, 2, 1},
			},
			want: []int{0, 4, 1, 3, 2},
		},
		{
			name: "nums = [1,2,3,4,0], index = [0,1,2,3,0]",
			args: args{
				nums:  []int{1, 2, 3, 4, 0},
				index: []int{0, 1, 2, 3, 0},
			},
			want: []int{0, 1, 2, 3, 4},
		},
		{
			name: "nums = [1], index = [0]",
			args: args{
				nums:  []int{1},
				index: []int{0},
			},
			want: []int{1},
		},
		{
			name: "nums = [4,2,4,3,2], index = [0,0,1,3,1]",
			args: args{
				nums:  []int{4, 2, 4, 3, 2},
				index: []int{0, 0, 1, 3, 1},
			},
			want: []int{2, 2, 4, 4, 3},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := createTargetArray(tt.args.nums, tt.args.index); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, createTargetArray() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
