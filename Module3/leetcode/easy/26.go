package easy

// https://leetcode.com/problems/decompress-run-length-encoded-list/

func decompressRLElist(nums []int) []int {
	result := []int{}

	for i := 0; i < len(nums); i += 2 {
		for j := nums[i]; j > 0; j-- {
			result = append(result, nums[i+1])
		}
	}

	return result
}
