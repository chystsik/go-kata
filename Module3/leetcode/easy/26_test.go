package easy

import (
	"reflect"
	"testing"
)

func Test_decompressRLElist(t *testing.T) {
	type args struct {
		nums []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "nums = [1,2,3,4]",
			args: args{nums: []int{1, 2, 3, 4}},
			want: []int{2, 4, 4, 4},
		},
		{
			name: "nums = [1,1,2,3]",
			args: args{nums: []int{1, 1, 2, 3}},
			want: []int{1, 3, 3},
		},
		{
			name: "nums = [2, 4, 4, 4]",
			args: args{nums: []int{2, 4, 4, 4}},
			want: []int{4, 4, 4, 4, 4, 4},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := decompressRLElist(tt.args.nums); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, decompressRLElist() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
