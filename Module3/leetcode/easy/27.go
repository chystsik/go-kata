package easy

// https://leetcode.com/problems/split-a-string-in-balanced-strings/

func balancedStringSplit(s string) int {
	result, curr := 0, 0

	for _, c := range s {
		if c == 'L' {
			curr++
		} else {
			curr--
		}
		if curr == 0 {
			result++
		}
	}

	return result
}
