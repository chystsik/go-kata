package easy

import "testing"

func Test_balancedStringSplit(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "s = \"RLRRLLRLRL\"",
			args: args{"RLRRLLRLRL"},
			want: 4,
		},
		{
			name: "s = \"RLRRRLLRLL\"",
			args: args{"RLRRRLLRLL"},
			want: 2,
		},
		{
			name: "s = \"LLLLRRRR\"",
			args: args{"LLLLRRRR"},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := balancedStringSplit(tt.args.s); got != tt.want {
				t.Errorf("Test: %s, balancedStringSplit() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
