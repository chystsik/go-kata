package easy

import (
	"fmt"
)

// https://leetcode.com/problems/count-the-digits-that-divide-a-number/

func countDigits(num int) int {
	n, result := num, 0

	for _, number := range fmt.Sprint(num) {
		if n%int(number-'0') == 0 {
			result++
		}
	}

	return result
}
