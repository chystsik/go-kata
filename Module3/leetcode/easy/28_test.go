package easy

import "testing"

func Test_countDigits(t *testing.T) {
	type args struct {
		num int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "num = 7",
			args: args{7},
			want: 1,
		},
		{
			name: "num = 121",
			args: args{121},
			want: 2,
		},
		{
			name: "num = 1248",
			args: args{1248},
			want: 4,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := countDigits(tt.args.num); got != tt.want {
				t.Errorf("Test: %s, countDigits() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
