package easy

import "testing"

func Test_xorOperation(t *testing.T) {
	type args struct {
		n     int
		start int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "n = 5, start = 0",
			args: args{n: 5, start: 0},
			want: 8,
		},
		{
			name: "n = 4, start = 3",
			args: args{n: 4, start: 3},
			want: 8,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := xorOperation(tt.args.n, tt.args.start); got != tt.want {
				t.Errorf("Test: %s, xorOperation() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
