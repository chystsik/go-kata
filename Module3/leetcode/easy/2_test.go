package easy

import (
	"reflect"
	"testing"
)

func Test_getConcatenation(t *testing.T) {
	type args struct {
		nums []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "nums = [1,2,1]",
			args: args{[]int{1, 2, 1}},
			want: []int{1, 2, 1, 1, 2, 1},
		},
		{
			name: "nums = [1,3,2,1]",
			args: args{[]int{1, 3, 2, 1}},
			want: []int{1, 3, 2, 1, 1, 3, 2, 1},
		},
		{
			name: "nums = [1]",
			args: args{[]int{1}},
			want: []int{1, 1},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getConcatenation(tt.args.nums); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, getConcatenation() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
