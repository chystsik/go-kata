package easy

// https://leetcode.com/problems/count-good-triplets/

func countGoodTriplets(arr []int, a int, b int, c int) int {
	result, arrLen := 0, len(arr)

	abs := func(num int) int {
		if num < 0 {
			num *= -1
		}
		return num
	}

	for i := 0; i < arrLen-2; i++ {
		for j := i + 1; j < arrLen-1; j++ {
			if abs(arr[i]-arr[j]) <= a {
				for k := j + 1; k < arrLen; k++ {
					if abs(arr[j]-arr[k]) <= b && abs(arr[i]-arr[k]) <= c {
						result++
					}
				}
			}
		}
	}

	return result
}
