package easy

import "sort"

// https://leetcode.com/problems/sort-the-people/

func sortPeople(names []string, heights []int) []string {
	hash := make([]person, len(names))
	result := make([]string, len(names))

	for i := range hash {
		hash[i].name = names[i]
		hash[i].height = heights[i]
	}

	sort.Slice(hash, func(i, j int) bool {
		return hash[i].height > hash[j].height
	})

	for i := range result {
		result[i] = hash[i].name
	}

	return result
}

type person struct {
	name   string
	height int
}
