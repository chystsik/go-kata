package easy

import (
	"reflect"
	"testing"
)

func Test_sortPeople(t *testing.T) {
	type args struct {
		names   []string
		heights []int
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "names = [\"Mary\",\"John\",\"Emma\"], heights = [180,165,170]",
			args: args{
				names:   []string{"Mary", "John", "Emma"},
				heights: []int{180, 165, 170},
			},
			want: []string{"Mary", "Emma", "John"},
		},
		{
			name: "names = [\"Alice\",\"Bob\",\"Bob\"], heights = [155,185,150]",
			args: args{
				names:   []string{"Alice", "Bob", "Bob"},
				heights: []int{155, 185, 150},
			},
			want: []string{"Bob", "Alice", "Bob"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sortPeople(tt.args.names, tt.args.heights); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, sortPeople() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
