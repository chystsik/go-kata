package easy

import (
	"reflect"
	"testing"
)

func Test_convertTemperature(t *testing.T) {
	type args struct {
		celsius float64
	}
	tests := []struct {
		name string
		args args
		want []float64
	}{
		{
			name: "celsius = 36.50",
			args: args{36.50},
			want: []float64{309.65000, 97.70000},
		},
		{
			name: "celsius = 122.11",
			args: args{122.11},
			want: []float64{395.26000, 251.79800},
		},
		{
			name: "celsius = 0",
			args: args{0},
			want: []float64{273.15, 32.00},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := convertTemperature(tt.args.celsius); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, convertTemperature() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
