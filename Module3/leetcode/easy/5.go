package easy

// https://leetcode.com/problems/count-of-matches-in-tournament/

func numberOfMatches(n int) int {
	if n%2 == 0 {
		return n - 1
	} else {
		return (2*n - 1) >> 1
	}
}

func numberOfMatchesOn(n int) int {
	var totalMatches int

	for n > 1 {
		currMatches := n >> 1
		totalMatches += currMatches
		n = currMatches + n%2
	}

	return totalMatches
}
