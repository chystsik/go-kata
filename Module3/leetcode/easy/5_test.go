package easy

import "testing"

func BenchmarkOn(b *testing.B) {
	for i := 0; i < b.N; i++ {
		numberOfMatchesOn(i)
	}
}

func BenchmarkO1(b *testing.B) {
	for i := 0; i < b.N; i++ {
		numberOfMatches(i)
	}
}

func Test_numberOfMatches(t *testing.T) {
	type args struct {
		n int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "n = 7",
			args: args{7},
			want: 6,
		},
		{
			name: "n = 14",
			args: args{14},
			want: 13,
		},
		{
			name: "n = 2",
			args: args{2},
			want: 1,
		},
		{
			name: "n = 1",
			args: args{1},
			want: 0,
		},
		{
			name: "n = 4",
			args: args{4},
			want: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := numberOfMatches(tt.args.n); got != tt.want {
				t.Errorf("Test: %s, numberOfMatches() = %v, want %v", tt.name, got, tt.want)
			}
			if got := numberOfMatchesOn(tt.args.n); got != tt.want {
				t.Errorf("Test: %s, numberOfMatches() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
