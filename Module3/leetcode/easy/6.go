package easy

import "strings"

// https://leetcode.com/problems/unique-morse-code-words/

func uniqueMorseRepresentations(words []string) int {
	morse := [26]string{".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."}
	result := make(map[string]struct{})

	for _, word := range words {
		var code strings.Builder
		for i := range word {
			code.WriteString(morse[word[i]-'a'])
		}
		result[code.String()] = struct{}{}
	}

	return len(result)
}
