package easy

import "testing"

func Test_uniqueMorseRepresentations(t *testing.T) {
	type args struct {
		words []string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "words = [\"gin\",\"zen\",\"gig\",\"msg\"]",
			args: args{[]string{"gin", "zen", "gig", "msg"}},
			want: 2,
		},
		{
			name: "words = [\"a\"]",
			args: args{[]string{"a"}},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := uniqueMorseRepresentations(tt.args.words); got != tt.want {
				t.Errorf("Test: %s, uniqueMorseRepresentations() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
