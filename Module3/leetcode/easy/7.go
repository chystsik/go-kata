package easy

import "strings"

// https://leetcode.com/problems/defanging-an-ip-address/

func defangIPaddr(address string) string {
	return strings.ReplaceAll(address, ".", "[.]")
}
