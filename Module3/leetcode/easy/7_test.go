package easy

import "testing"

func Test_defangIPaddr(t *testing.T) {
	type args struct {
		address string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "address = \"1.1.1.1\"",
			args: args{"1.1.1.1"},
			want: "1[.]1[.]1[.]1",
		},
		{
			name: "address = \"255.100.50.0\"",
			args: args{"255.100.50.0"},
			want: "255[.]100[.]50[.]0",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := defangIPaddr(tt.args.address); got != tt.want {
				t.Errorf("Test: %s, defangIPaddr() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
