package easy

// https://leetcode.com/problems/kth-missing-positive-number/

func findKthPositive(arr []int, k int) int {
	tmp := make([]int, arr[len(arr)-1]+k)
	copy(tmp, arr)

	for i, counter, arrIndex := 1, 0, 0; i <= len(tmp); i++ {
		if i == tmp[arrIndex] {
			arrIndex++
			continue
		}
		counter++
		if k == counter {
			return i
		}
	}
	return 0
}
