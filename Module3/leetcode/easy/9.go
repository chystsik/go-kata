package easy

// https://leetcode.com/problems/final-value-of-variable-after-performing-operations/

func finalValueAfterOperations(operations []string) int {
	result := 0

	for _, operation := range operations {
		switch operation {
		case "X++":
			result++
		case "++X":
			result++
		default:
			result--
		}
	}

	return result
}
