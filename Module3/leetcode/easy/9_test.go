package easy

import "testing"

func Test_finalValueAfterOperations(t *testing.T) {
	type args struct {
		operations []string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "operations = [\"--X\",\"X++\",\"X++\"]",
			args: args{[]string{"--X", "X++", "X++"}},
			want: 1,
		},
		{
			name: "operations = [\"X++\",\"++X\",\"--X\",\"X--\"]",
			args: args{[]string{"X++", "++X", "--X", "X--"}},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := finalValueAfterOperations(tt.args.operations); got != tt.want {
				t.Errorf("Test: %s, finalValueAfterOperations() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
