package medium

// https://leetcode.com/problems/deepest-leaves-sum/

/* type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
} */

func deepestLeavesSum(root *TreeNode) int {
	targetDepth, result := 0, 0

	var dfs = func(r *TreeNode, l int) {}
	dfs = func(r *TreeNode, l int) {
		if r == nil {
			return
		}
		if targetDepth < l {
			targetDepth = l
			result = r.Val
		} else if targetDepth == l {
			result += r.Val
		}
		dfs(r.Left, l+1)
		dfs(r.Right, l+1)
	}

	dfs(root, 1)
	return result
}
