package medium

// https://leetcode.com/problems/letter-tile-possibilities/

func numTilePossibilities(tiles string) int {
	var hash [26]int
	var numOfSequences int
	var dfs func()

	for i := range tiles {
		hash[tiles[i]-'A']++
	}

	dfs = func() {
		for i := 0; i < len(hash); i++ {
			if hash[i] == 0 {
				continue
			}
			numOfSequences++
			hash[i]--
			dfs()
			hash[i]++
		}
	}

	dfs()
	return numOfSequences
}
