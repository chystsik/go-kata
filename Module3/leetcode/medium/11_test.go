package medium

import (
	"reflect"
	"testing"
)

func Test_removeLeafNodes(t *testing.T) {
	type args struct {
		root   *TreeNode
		target int
	}
	tests := []struct {
		name string
		args args
		want *TreeNode
	}{
		{
			name: "1 root = [1,2,3,2,null,2,4], target = 2",
			args: args{
				root:   intsToBinaryTreeSimple([]int{1, 2, 3, 2, -1, 2, 4}, 0),
				target: 2,
			},
			want: intsToBinaryTreeSimple([]int{1, -1, 3, -1, -1, -1, 4}, 0),
		},
		{
			name: "2 root = [1,3,3,3,2], target = 3",
			args: args{
				root:   intsToBinaryTreeSimple([]int{1, 3, 3, 3, 2}, 0),
				target: 3,
			},
			want: intsToBinaryTreeSimple([]int{1, 3, -1, -1, 2}, 0),
		},
		{
			name: "3 root = [1,2,null,2,null,2], target = 2",
			args: args{
				root:   intsToBinaryTreeSimple([]int{1, 2, -1, 2, -1, 2}, 0),
				target: 2,
			},
			want: intsToBinaryTreeSimple([]int{1}, 0),
		},
		{
			name: "4 root = [1,1,1], target = 1",
			args: args{
				root:   intsToBinaryTreeSimple([]int{1, 1, 1}, 0),
				target: 1,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := removeLeafNodes(tt.args.root, tt.args.target); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, removeLeafNodes() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
