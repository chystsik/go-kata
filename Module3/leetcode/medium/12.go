package medium

// https://leetcode.com/problems/merge-in-between-linked-lists/

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	p := list1
	var an, bn, prev *ListNode

	for i := 0; p != nil; i, prev, p = i+1, p, p.Next {
		if i == a {
			an = prev
		}
		if i == b {
			bn = p.Next
		}
	}

	an.Next = list2
	for ; list2.Next != nil; list2 = list2.Next {
	}
	list2.Next = bn

	return list1
}
