package medium

import (
	"reflect"
	"testing"
)

func Test_mergeInBetween(t *testing.T) {
	type args struct {
		list1 *ListNode
		a     int
		b     int
		list2 *ListNode
	}
	tests := []struct {
		name string
		args args
		want *ListNode
	}{
		{
			name: "list1 = [0,1,2,3,4,5], a = 3, b = 4, list2 = [1000000,1000001,1000002]",
			args: args{
				list1: intsToLinkedList([]int{0, 1, 2, 3, 4, 5}, 0),
				a:     3,
				b:     4,
				list2: intsToLinkedList([]int{1000000, 1000001, 1000002}, 0),
			},
			want: intsToLinkedList([]int{0, 1, 2, 1000000, 1000001, 1000002, 5}, 0),
		},
		{
			name: "list1 = [0,1,2,3,4,5,6], a = 2, b = 5, list2 = [1000000,1000001,1000002,1000003,1000004]",
			args: args{
				list1: intsToLinkedList([]int{0, 1, 2, 3, 4, 5, 6}, 0),
				a:     2,
				b:     5,
				list2: intsToLinkedList([]int{1000000, 1000001, 1000002, 1000003, 1000004}, 0),
			},
			want: intsToLinkedList([]int{0, 1, 1000000, 1000001, 1000002, 1000003, 1000004, 6}, 0),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := mergeInBetween(tt.args.list1, tt.args.a, tt.args.b, tt.args.list2); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, mergeInBetween() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
