package medium

// https://leetcode.com/problems/maximum-sum-of-an-hourglass/

func maxSum(grid [][]int) int {
	var sum int

	for i := 0; i+3 <= len(grid); i += 1 {
		for j := 0; j+3 <= len(grid[i]); j += 1 {
			currSum := grid[i][j] + grid[i][j+1] + grid[i][j+2] + grid[i+1][j+1] + grid[i+2][j] + grid[i+2][j+1] + grid[i+2][j+2]
			if currSum > sum {
				sum = currSum
			}
		}
	}
	return sum
}
