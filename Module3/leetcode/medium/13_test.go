package medium

import "testing"

func Test_maxSum(t *testing.T) {
	type args struct {
		grid [][]int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "grid = [[6,2,1,3],[4,2,1,5],[9,2,8,7],[4,1,2,9]]",
			args: args{grid: [][]int{{6, 2, 1, 3}, {4, 2, 1, 5}, {9, 2, 8, 7}, {4, 1, 2, 9}}},
			want: 30,
		},
		{
			name: "grid = [[1,2,3],[4,5,6],[7,8,9]]",
			args: args{grid: [][]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}},
			want: 35,
		},
		{
			name: "grid = [[520626,685427,788912,800638,717251,683428],[23602,608915,697585,957500,154778,209236],[287585,588801,818234,73530,939116,252369]]",
			args: args{grid: [][]int{{520626, 685427, 788912, 800638, 717251, 683428}, {23602, 608915, 697585, 957500, 154778, 209236}, {287585, 588801, 818234, 73530, 939116, 252369}}},
			want: 5095181,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := maxSum(tt.args.grid); got != tt.want {
				t.Errorf("Test: %s, maxSum() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
