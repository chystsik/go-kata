package medium

// https://leetcode.com/problems/xor-queries-of-a-subarray/

func xorQueries(arr []int, queries [][]int) []int {
	result := make([]int, len(queries))

	for i := range queries {
		var val int
		for _, num := range arr[queries[i][0] : queries[i][1]+1] {
			val ^= num
		}
		result[i] = val
	}
	return result
}
