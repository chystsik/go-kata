package medium

// https://leetcode.com/problems/partitioning-into-minimum-number-of-deci-binary-numbers/

func minPartitions(n string) int {
	result := 0

	for i := range n {
		result = max(result, int(n[i]-'0'))
	}
	return result
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
