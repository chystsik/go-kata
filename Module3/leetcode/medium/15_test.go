package medium

import "testing"

func Test_minPartitions(t *testing.T) {
	type args struct {
		n string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "n = \"32\"",
			args: args{
				n: "32",
			},
			want: 3,
		},
		{
			name: "n = \"82734\"",
			args: args{
				n: "82734",
			},
			want: 8,
		},
		{
			name: "n = \"27346209830709182346\"",
			args: args{
				n: "27346209830709182346",
			},
			want: 9,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := minPartitions(tt.args.n); got != tt.want {
				t.Errorf("Test: %s, minPartitions() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
