package medium

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRectangle(t *testing.T) {
	reactangle := [][]int{{1, 2, 1}, {4, 3, 4}, {3, 2, 1}, {1, 1, 1}}

	rect := Constructor(reactangle)
	assert.Equal(t, reactangle, rect.Rectangle)
	assert.Equal(t, 1, rect.GetValue(0, 2))

	updateSubrectangle := [][]int{{0, 0, 3, 2, 5}, {3, 0, 3, 2, 10}}
	getValue := [][]int{{0, 2}, {3, 1}, {3, 1}, {0, 2}}
	expected := []int{5, 5, 10, 5}

	for i, sub := range updateSubrectangle {
		rect.UpdateSubrectangle(sub[0], sub[1], sub[2], sub[3], sub[4])
		for j := i * 2; j <= i*2+1; j++ {
			assert.Equal(t, expected[j], rect.GetValue(getValue[j][0], getValue[j][1]))
		}
	}

}
