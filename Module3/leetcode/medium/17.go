package medium

// https://leetcode.com/problems/queries-on-number-of-points-inside-a-circle/

// (x - center_x)² + (y - center_y)² <= radius²

func countPoints(points [][]int, queries [][]int) []int {
	result := make([]int, len(queries))

	for i := range queries {
		var count int
		for j := range points {
			if (points[j][0]-queries[i][0])*(points[j][0]-queries[i][0])+(points[j][1]-queries[i][1])*(points[j][1]-queries[i][1]) <= queries[i][2]*queries[i][2] {
				count++
			}
		}
		result[i] = count
	}
	return result
}
