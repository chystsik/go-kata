package medium

import (
	"reflect"
	"testing"
)

func Test_countPoints(t *testing.T) {
	type args struct {
		points  [][]int
		queries [][]int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "points = [[1,3],[3,3],[5,3],[2,2]], queries = [[2,3,1],[4,3,1],[1,1,2]]",
			args: args{
				points:  [][]int{{1, 3}, {3, 3}, {5, 3}, {2, 2}},
				queries: [][]int{{2, 3, 1}, {4, 3, 1}, {1, 1, 2}},
			},
			want: []int{3, 2, 2},
		},
		{
			name: "points = [[1,1],[2,2],[3,3],[4,4],[5,5]], queries = [[1,2,2],[2,2,2],[4,3,2],[4,3,3]]",
			args: args{
				points:  [][]int{{1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}},
				queries: [][]int{{1, 2, 2}, {2, 2, 2}, {4, 3, 2}, {4, 3, 3}},
			},
			want: []int{2, 3, 2, 4},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := countPoints(tt.args.points, tt.args.queries); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, countPoints() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
