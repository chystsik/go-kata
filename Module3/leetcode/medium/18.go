package medium

// https://leetcode.com/problems/group-the-people-given-the-group-size-they-belong-to/

func groupThePeople(groupSizes []int) [][]int {
	result := make([][]int, 0)
	hash := make(map[int][]int)

	for i, size := range groupSizes {
		hash[size] = append(hash[size], i)
	}

	for k, v := range hash {
		lenVal := len(v)
		for lenVal > 0 {
			result = append(result, v[lenVal-k:lenVal])
			lenVal -= k
		}
	}
	return result
}
