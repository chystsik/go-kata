package medium

import (
	"reflect"
	"sort"
	"testing"
)

func Test_groupThePeople(t *testing.T) {
	type args struct {
		groupSizes []int
	}
	tests := []struct {
		name string
		args args
		want [][]int
	}{
		{
			name: "groupSizes = [3,3,3,3,3,1,3]",
			args: args{
				groupSizes: []int{3, 3, 3, 3, 3, 1, 3},
			},
			want: [][]int{{5}, {0, 1, 2}, {3, 4, 6}},
		},
		{
			name: "groupSizes = [2,1,3,3,3,2]",
			args: args{
				groupSizes: []int{2, 1, 3, 3, 3, 2},
			},
			want: [][]int{{1}, {0, 5}, {2, 3, 4}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := groupThePeople(tt.args.groupSizes); !reflect.DeepEqual(toMap(got), toMap(tt.want)) {
				t.Errorf("Test: %s, groupThePeople() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}

func toMap(arr [][]int) map[int][]int {
	result := make(map[int][]int)
	for i := 0; i < len(arr); i++ {
		result[len(arr[i])] = append(result[len(arr[i])], arr[i]...)
		sort.Ints(result[len(arr[i])])
	}
	return result
}
