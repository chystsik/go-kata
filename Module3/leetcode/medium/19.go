package medium

// https://leetcode.com/problems/count-nodes-equal-to-average-of-subtree/

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func averageOfSubtree(root *TreeNode) int {
	result := 0
	var dfs func(*TreeNode) (int, int)
	dfs = func(tn *TreeNode) (int, int) {
		if tn == nil {
			return 0, 0
		}
		lSum, lCount := dfs(tn.Left)
		rSum, rCount := dfs(tn.Right)
		sum := lSum + rSum + tn.Val
		count := lCount + rCount + 1
		if sum/count == tn.Val {
			result++
		}
		return sum, count
	}
	dfs(root)
	return result
}
