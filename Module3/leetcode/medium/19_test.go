package medium

import "testing"

func Test_averageOfSubtree(t *testing.T) {
	type args struct {
		root *TreeNode
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "root = [4,8,5,0,1,null,6]",
			args: args{
				root: intsToBinaryTreeSimple([]int{4, 8, 5, 0, 1, -1, 6}, 0),
			},
			want: 5,
		},
		{
			name: "root = [1]",
			args: args{
				root: intsToBinaryTreeSimple([]int{1}, 0),
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := averageOfSubtree(tt.args.root); got != tt.want {
				t.Errorf("Test: %s, averageOfSubtree() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
