package medium

import (
	"testing"
)

/* Constraints:

The number of nodes in the tree is in the range [1, 104].
1 <= Node.val <= 100 */

func Test_deepestLeavesSum(t *testing.T) {
	type args struct {
		root *TreeNode
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "root = [1,2,3,4,5,null,6,7,null,null,null,null,8]",
			args: args{root: intsToBinaryTree([]int{1, 2, 3, 4, 5, -1, 6, 7, -1, -1, -1, -1, 8}, 0)},
			want: 15,
		},
		{
			name: "root = [6,7,8,2,7,1,3,9,null,1,4,null,null,null,5]",
			args: args{root: intsToBinaryTree([]int{6, 7, 8, 2, 7, 1, 3, 9, -1, 1, 4, -1, -1, -1, 5}, 0)},
			want: 19,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := deepestLeavesSum(tt.args.root); got != tt.want {
				t.Errorf("Test: %s, deepestLeavesSum() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
