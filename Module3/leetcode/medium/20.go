package medium

// https://leetcode.com/problems/queries-on-a-permutation-with-key/

func processQueries(queries []int, m int) []int {
	permutation := make([]int, m)
	result := make([]int, len(queries))

	for i := 1; i <= m; i++ {
		permutation[i-1] = i
	}

	for i, q := range queries {
		for j, v := range permutation {
			if q == v {
				result[i] = j
				copy(permutation[1:], permutation[:j])
				permutation[0] = v
			}
		}
	}
	return result
}
