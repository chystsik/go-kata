package medium

import (
	"reflect"
	"testing"
)

func Test_processQueries(t *testing.T) {
	type args struct {
		queries []int
		m       int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "queries = [3,1,2,1], m = 5",
			args: args{
				queries: []int{3, 1, 2, 1},
				m:       5,
			},
			want: []int{2, 1, 2, 1},
		},
		{
			name: "queries = [4,1,2,2], m = 4",
			args: args{
				queries: []int{4, 1, 2, 2},
				m:       4,
			},
			want: []int{3, 1, 2, 0},
		},
		{
			name: "queries = [7,5,5,8,3], m = 8",
			args: args{
				queries: []int{7, 5, 5, 8, 3},
				m:       8,
			},
			want: []int{6, 5, 0, 7, 5},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := processQueries(tt.args.queries, tt.args.m); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, processQueries() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
