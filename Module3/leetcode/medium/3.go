package medium

// https://leetcode.com/problems/merge-nodes-in-between-zeros/

/* type ListNode struct {
	Val  int
	Next *ListNode
} */

func mergeNodes(head *ListNode) *ListNode {
	var sum int
	modified := &ListNode{}
	p := modified

	for head != nil {
		if head.Val != 0 {
			sum += head.Val
		}
		if head.Val == 0 && sum != 0 {
			p.Next = &ListNode{Val: sum, Next: nil}
			p = p.Next
			sum = 0
		}
		head = head.Next
	}

	return modified.Next
}
