package medium

import (
	"reflect"
	"testing"
)

func Test_mergeNodes(t *testing.T) {
	type args struct {
		head *ListNode
	}
	tests := []struct {
		name string
		args args
		want *ListNode
	}{
		{
			name: "head = [0,3,1,0,4,5,2,0]",
			args: args{head: intsToLinkedList([]int{0, 3, 1, 0, 4, 5, 2, 0}, 0)},
			want: intsToLinkedList([]int{4, 11}, 0), //&ListNode{Val: 4, Next: &ListNode{Val: 11}},
		},
		{
			name: "head = [0,1,0,3,0,2,2,0]",
			args: args{head: intsToLinkedList([]int{0, 1, 0, 3, 0, 2, 2, 0}, 0)},
			want: intsToLinkedList([]int{1, 3, 4}, 0), //&ListNode{Val: 1, Next: &ListNode{Val: 3, Next: &ListNode{Val: 4}}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := mergeNodes(tt.args.head); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, mergeNodes() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
