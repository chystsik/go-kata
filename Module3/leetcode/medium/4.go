package medium

// https://leetcode.com/problems/binary-search-tree-to-greater-sum-tree/

/* type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
} */

func bstToGst(root *TreeNode) *TreeNode {
	sum := 0
	return dfs(root, &sum)
}

func dfs(node *TreeNode, sum *int) *TreeNode {
	if node != nil {
		dfs(node.Right, sum)
		*sum += node.Val
		node.Val = *sum
		dfs(node.Left, sum)
	}
	return node
}
