package medium

import (
	"reflect"
	"testing"
)

func Test_bstToGst(t *testing.T) {
	type args struct {
		root *TreeNode
	}
	tests := []struct {
		name string
		args args
		want *TreeNode
	}{
		{
			name: "root = [4,1,6,0,2,5,7,null,null,null,3,null,null,null,8]",
			args: args{root: intsToBinaryTreeSimple([]int{4, 1, 6, 0, 2, 5, 7, -1, -1, -1, 3, -1, -1, -1, 8}, 0)},
			want: intsToBinaryTreeSimple([]int{30, 36, 21, 36, 35, 26, 15, -1, -1, -1, 33, -1, -1, -1, 8}, 0),
		},
		{
			name: "root = [0,null,1]",
			args: args{root: intsToBinaryTreeSimple([]int{0, -1, 1}, 0)},
			want: intsToBinaryTreeSimple([]int{1, -1, 1}, 0),
		},
		{
			name: "root = [0,null,1]",
			args: args{root: intsToBinaryTreeSimple([]int{0, -1, 1}, 0)},
			want: &TreeNode{Val: 1, Left: nil, Right: &TreeNode{Val: 1}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := bstToGst(tt.args.root); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, bstToGst() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
