package medium

// https://leetcode.com/problems/maximum-twin-sum-of-a-linked-list/

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func pairSum(head *ListNode) int {
	var maxTwinSum int
	var twinSum int

	middle := head
	p := head

	// find middle of the list
	for p != nil && p.Next != nil {
		p = p.Next.Next
		middle = middle.Next
	}

	// reverse middle
	middle = reverseList(middle)

	// find max sum
	for ; middle != nil; head, middle = head.Next, middle.Next {
		twinSum = head.Val + middle.Val
		if twinSum > maxTwinSum {
			maxTwinSum = twinSum
		}
	}
	return maxTwinSum
}

func reverseList(head *ListNode) *ListNode {
	var reverced *ListNode

	for ; head != nil; head = head.Next {
		reverced = &ListNode{Val: head.Val, Next: reverced}
	}
	return reverced
}
