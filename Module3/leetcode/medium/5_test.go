package medium

import "testing"

func Test_pairSum(t *testing.T) {
	type args struct {
		head *ListNode
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "head = [5,4,2,1]",
			args: args{head: intsToLinkedList([]int{5, 4, 2, 1}, 0)},
			want: 6,
		},
		{
			name: "head = [4,2,2,3]",
			args: args{head: intsToLinkedList([]int{4, 2, 2, 3}, 0)},
			want: 7,
		},
		{
			name: "head = [1,100000]",
			args: args{head: intsToLinkedList([]int{1, 100000}, 0)},
			want: 100001,
		},
		{
			name: "head = [47,22,81,46,94,95,90,22,55,91,6,83,49,65,10,32,41,26,83,99,14,85,42,99,89,69,30,92,32,74,9,81,5,9]",
			args: args{head: intsToLinkedList([]int{47, 22, 81, 46, 94, 95, 90, 22, 55, 91, 6, 83, 49, 65, 10, 32, 41, 26, 83, 99, 14, 85, 42, 99, 89, 69, 30, 92, 32, 74, 9, 81, 5, 9}, 0)},
			want: 182,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := pairSum(tt.args.head); got != tt.want {
				t.Errorf("Test: %s, pairSum() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
