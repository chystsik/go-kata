package medium

// https://leetcode.com/problems/maximum-binary-tree/

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func constructMaximumBinaryTree(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}
	var max, idx int
	for i, num := range nums {
		if num > max {
			max = num
			idx = i
		}
	}
	return &TreeNode{Val: max, Left: constructMaximumBinaryTree(nums[:idx]), Right: constructMaximumBinaryTree(nums[idx+1:])}
}
