package medium

import (
	"reflect"
	"testing"
)

func Test_constructMaximumBinaryTree(t *testing.T) {
	type args struct {
		nums []int
	}
	tests := []struct {
		name string
		args args
		want *TreeNode
	}{
		{
			name: "nums = [3,2,1,6,0,5]",
			args: args{
				nums: []int{3, 2, 1, 6, 0, 5},
			},
			want: intsToBinaryTreeSimple([]int{6, 3, 5, -1, 2, 0, -1, -1, -1, -1, 1}, 0),
		},
		{
			name: "nums = [3,2,1]",
			args: args{
				nums: []int{3, 2, 1},
			},
			want: intsToBinaryTreeSimple([]int{3, -1, 2, -1, -1, -1, 1}, 0),
		},
		{
			name: "nums = [3,2,1]",
			args: args{
				nums: []int{3, 2, 1},
			},
			want: &TreeNode{Val: 3, Left: nil, Right: &TreeNode{Val: 2, Left: nil, Right: &TreeNode{Val: 1}}}, //intsToBinaryTree([]int{3,-1,2,-1,1}, 0),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := constructMaximumBinaryTree(tt.args.nums); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, constructMaximumBinaryTree() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
