package medium

import (
	"sort"
)

// https://leetcode.com/problems/balance-a-binary-search-tree/

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func balanceBST(root *TreeNode) *TreeNode {
	var arr []int
	toArr(&arr, root)
	sort.Ints(arr)
	res := toBST(arr)
	return res
}

func toArr(arr *[]int, node *TreeNode) {
	if node == nil {
		return
	}
	toArr(arr, node.Left)
	*arr = append(*arr, node.Val)
	toArr(arr, node.Right)

}

func toBST(arr []int) *TreeNode {
	if len(arr) <= 0 {
		return nil
	}
	idx := len(arr) / 2
	return &TreeNode{
		Val:   arr[idx],
		Left:  toBST(arr[:idx]),
		Right: toBST(arr[idx+1:]),
	}
}
