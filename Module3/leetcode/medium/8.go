package medium

// https://leetcode.com/problems/minimum-number-of-vertices-to-reach-all-nodes/

func findSmallestSetOfVertices(n int, edges [][]int) []int {
	isIncomingEdgeExists := make([]bool, n)

	for i := 0; i < len(edges); i++ {
		isIncomingEdgeExists[edges[i][1]] = true
	}

	requiredNodes := []int{}
	for i := 0; i < n; i++ {
		if !isIncomingEdgeExists[i] {
			requiredNodes = append(requiredNodes, i)
		}
	}
	return requiredNodes
}
