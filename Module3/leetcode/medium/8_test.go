package medium

import (
	"reflect"
	"testing"
)

func Test_findSmallestSetOfVertices(t *testing.T) {
	type args struct {
		n     int
		edges [][]int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "n = 6, edges = [[0,1],[0,2],[2,5],[3,4],[4,2]]",
			args: args{
				n:     6,
				edges: [][]int{{0, 1}, {0, 2}, {2, 5}, {3, 4}, {4, 2}},
			},
			want: []int{0, 3},
		},
		{
			name: "n = 5, edges = [[0,1],[2,1],[3,1],[1,4],[2,4]]",
			args: args{
				n:     5,
				edges: [][]int{{0, 1}, {2, 1}, {3, 1}, {1, 4}, {2, 4}},
			},
			want: []int{0, 2, 3},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := findSmallestSetOfVertices(tt.args.n, tt.args.edges); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Test: %s, findSmallestSetOfVertices() = %v, want %v", tt.name, got, tt.want)
			}
		})
	}
}
