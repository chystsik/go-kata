package medium

import (
	"sort"
)

// https://leetcode.com/problems/arithmetic-subarrays/

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	result := []bool{}

	for i := 0; i < len(l); i++ {
		arr := make([]int, r[i]-l[i]+1)
		copy(arr, nums[l[i]:r[i]+1])
		sort.Ints(arr)
		result = append(result, isSequence(arr))
	}
	return result
}

func isSequence(arr []int) bool {
	diff := arr[1] - arr[0]

	for i := 1; i < len(arr); i++ {
		if arr[i]-arr[i-1] != diff {
			return false
		}
	}
	return true
}
