package medium

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

type ListNode struct {
	Val  int
	Next *ListNode
}

// -1 is nil node
func intsToBinaryTree(nums []int, pos int) *TreeNode {
	if pos >= len(nums) {
		return nil
	} else if nums[pos] == -1 {
		return &TreeNode{
			Left:  intsToBinaryTree(nums, (pos-1)*(2+1)),
			Right: intsToBinaryTree(nums, (pos-1)*(2+2)),
		}
	}
	return &TreeNode{
		Val:   nums[pos],
		Left:  intsToBinaryTree(nums, pos*2+1),
		Right: intsToBinaryTree(nums, pos*2+2),
	}
}

func intsToLinkedList(nums []int, i int) *ListNode {
	if i >= len(nums) {
		return nil
	}
	return &ListNode{
		Val:  nums[i],
		Next: intsToLinkedList(nums, i+1),
	}
}

// -1 is nil node
func intsToBinaryTreeSimple(nums []int, pos int) *TreeNode {
	if pos >= len(nums) || nums[pos] == -1 {
		return nil
	}
	return &TreeNode{
		Val:   nums[pos],
		Left:  intsToBinaryTreeSimple(nums, pos*2+1),
		Right: intsToBinaryTreeSimple(nums, pos*2+2),
	}
}
