package main

import (
	"errors"
	"fmt"
	"time"
)

const (
	errorEmptyFeed   = "feed is empty"
	errorNotFindPost = "no such post in feed"
)

type Post struct {
	body        string
	publishDate int64 // Unix timestamp
	next        *Post
}

type Feed struct {
	length int // we'll use it later
	start  *Post
	end    *Post
}

// Реализуйте метод Append для добавления нового поста в конец потока
func (f *Feed) Append(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
		f.end = newPost
	} else {
		prev := f.end
		prev.next = newPost
		f.end = newPost
	}
	f.length++
}

// Реализуйте метод Remove для удаления поста по дате публикации из потока
func (f *Feed) Remove(publishDate int64) {
	checkIfEmpty(f.length)
	p := f.start
	var prev *Post

	for p.publishDate != publishDate {
		if p.next == nil {
			panic(errors.New(errorNotFindPost))
		}
		prev = p
		p = p.next
	}
	prev.next = p.next
	f.length--
}

// Реализуйте метод Insert для вставки нового поста в поток в соответствии с его датой публикации
func (f *Feed) Insert(newPost *Post) {
	checkIfEmpty(f.length)
	for p := f.start; p.publishDate < newPost.publishDate; p = p.next {
		newPost.next = p.next
		p.next = newPost
		f.length++
	}
}

// Реализуйте метод Inspect для вывода информации о потоке и его постах
func (f *Feed) Inspect() {
	checkIfEmpty(f.length)
	item := 0
	fmt.Printf("Feed Length: %d\n", f.length)
	for p := f.start; p != nil; p = p.next {
		fmt.Printf("Item: %d, %v\n", item, p)
		item++
	}
}

func checkIfEmpty(length int) {
	if length == 0 {
		panic(errors.New(errorEmptyFeed))
	}
}

func main() {
	rightNow := time.Now().Unix()
	f := &Feed{}
	p1 := &Post{
		body:        "Lorem ipsum",
		publishDate: rightNow,
	}
	p2 := &Post{
		body:        "Dolor sit amet",
		publishDate: rightNow + 10,
	}
	p3 := &Post{
		body:        "consectetur adipiscing elit",
		publishDate: rightNow + 20,
	}
	p4 := &Post{
		body:        "sed do eiusmod tempor incididunt",
		publishDate: rightNow + 30,
	}
	f.Append(p1)
	f.Append(p2)
	f.Append(p3)
	f.Append(p4)

	f.Inspect()

	newPost := &Post{
		body:        "This is a new post",
		publishDate: rightNow + 15,
	}
	f.Insert(newPost)
	f.Inspect()

	f.Remove(rightNow + 20)
	f.Inspect()
}
