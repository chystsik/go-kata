package main

import "fmt"

type PricingStrategy interface {
	Calculate(*Order) float64
}

type Order struct {
	item  string
	price float64
}

func (o *Order) ProcessOrder(calc PricingStrategy) float64 {
	return calc.Calculate(o)
}

type RegularPricing struct {
}

func (rp *RegularPricing) Calculate(o *Order) float64 {
	return o.price
}

type SalePricing struct {
	name    string
	percent int
}

func (sp *SalePricing) Calculate(o *Order) float64 {
	return o.price - (float64(sp.percent)*o.price)/100
}

func main() {
	order := Order{
		price: 700,
		item:  "iphone20",
	}

	strategies := []PricingStrategy{
		&RegularPricing{},
		&SalePricing{percent: 10, name: "!spring sale!"},
		&SalePricing{percent: 25, name: "!black friday!"},
		&SalePricing{percent: 50, name: "!cristmas sale!"},
		&RegularPricing{},
	}

	for _, strategy := range strategies {
		var saleName string
		var discount int

		if sale, ok := strategy.(*SalePricing); ok {
			saleName = sale.name
			discount = sale.percent
		}

		fmt.Printf("Total cost with %T %s strategy: %.2f. Discount: %d%% Product: %s\n", strategy, saleName, order.ProcessOrder(strategy), discount, order.item)
	}
}

/* Output:
Total cost with *main.RegularPricing  strategy: 700.00. Discount: 0% Product: iphone20
Total cost with *main.SalePricing !spring sale! strategy: 630.00. Discount: 10% Product: iphone20
Total cost with *main.SalePricing !black friday! strategy: 525.00. Discount: 25% Product: iphone20
Total cost with *main.SalePricing !cristmas sale! strategy: 350.00. Discount: 50% Product: iphone20
Total cost with *main.RegularPricing  strategy: 700.00. Discount: 0% Product: iphone20
*/
