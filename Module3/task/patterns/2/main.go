package main

import "fmt"

// AirConditioner is the interface that defines the methods for controlling the air conditioner
type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

// RealAirConditioner is the implementation of the air conditioner
type RealAirConditioner struct{}

func (realAC *RealAirConditioner) TurnOn() {
	fmt.Println("Turning on the air conditioner")
}

func (realAC *RealAirConditioner) TurnOff() {
	fmt.Println("Turning off the air conditioner")
}

func (realAC *RealAirConditioner) SetTemperature(temp int) {
	fmt.Printf("Setting the air conditioner temperature to %d\n", temp)
}

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

func (adapterAC *AirConditionerAdapter) TurnOn() {
	adapterAC.airConditioner.TurnOn()
}

func (adapterAC *AirConditionerAdapter) TurnOff() {
	adapterAC.airConditioner.TurnOff()
}

func (adapterAC *AirConditionerAdapter) SetTemperature(temp int) {
	adapterAC.airConditioner.SetTemperature(temp)
}

type AirConditionerProxy struct {
	adapter       *AirConditionerAdapter
	authenticated bool
}

func (proxyAC *AirConditionerProxy) TurnOn() {
	if proxyAC.authenticated {
		proxyAC.adapter.TurnOn()
	} else {
		fmt.Println("Access denied: authentication required to turn on the air conditioner")
	}
}

func (proxyAC *AirConditionerProxy) TurnOff() {
	if proxyAC.authenticated {
		proxyAC.adapter.TurnOff()
	} else {
		fmt.Println("Access denied: authentication required to turn off the air conditioner")
	}

}

func (proxyAC *AirConditionerProxy) SetTemperature(temp int) {
	if proxyAC.authenticated {
		proxyAC.adapter.SetTemperature(temp)
	} else {
		fmt.Println("Access denied: authentication required to set the temperature of the air conditioner")
	}
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	return &AirConditionerProxy{
		authenticated: authenticated,
		adapter:       &AirConditionerAdapter{},
	}
}

func main() {
	airConditioner := NewAirConditionerProxy(false) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = NewAirConditionerProxy(true) // with auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
}

/* Output:
Access denied: authentication required to turn on the air conditioner
Access denied: authentication required to turn off the air conditioner
Access denied: authentication required to set the temperature of the air conditioner
Turning on the air conditioner
Turning off the air conditioner
Setting the air conditioner temperature to 25 */
