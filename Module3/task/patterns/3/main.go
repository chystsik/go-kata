package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

// WeatherAPI is the interface that defines the methods for accessing weather information
type WeatherAPI interface {
	GetTemperature(location string) int
	GetHumidity(location string) int
	GetWindSpeed(location string) int
	query(location string)
}

// OpenWeatherAPI is the implementation of the weather API
type OpenWeatherAPI struct {
	apiKey      string
	weatherInfo *CurrentWeatherInfo
	currInfoUrl string
}

type CurrentWeatherInfo struct {
	Main Main `json:"main"`
	Wind Wind `json:"wind"`
}

type Main struct {
	Temperature float64 `json:"temp"`
	Humidity    float64 `json:"humidity"`
}

type Wind struct {
	Speed float64 `json:"speed"`
}

func (o *OpenWeatherAPI) GetTemperature(location string) int {
	// Make a request to the open weather API to retrieve temperature information
	// and return the result
	// ...

	return int(o.weatherInfo.Main.Temperature)
}

func (o *OpenWeatherAPI) GetHumidity(location string) int {
	// Make a request to the open weather API to retrieve humidity information
	// and return the result
	// ...

	return int(o.weatherInfo.Main.Humidity)
}

func (o *OpenWeatherAPI) GetWindSpeed(location string) int {
	// Make a request to the open weather API to retrieve wind speed information
	// and return the result
	// ...

	return int(o.weatherInfo.Wind.Speed)
}

func (o *OpenWeatherAPI) query(location string) {
	url := fmt.Sprintf(o.currInfoUrl, location, o.apiKey)
	res, err := http.Get(url)
	if err != nil {
		panic(err)
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(body, &o.weatherInfo)
	if err != nil {
		panic(err)
	}
}

// WeatherFacade is the facade that provides a simplified interface to the weather API
type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(location string) (int, int, int) {
	w.weatherAPI.query(location)

	temperature := w.weatherAPI.GetTemperature(location)
	humidity := w.weatherAPI.GetHumidity(location)
	windSpeed := w.weatherAPI.GetWindSpeed(location)

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{
			apiKey:      apiKey,
			currInfoUrl: "http://api.openweathermap.org/data/2.5/weather?q=%s&units=metric&appid=%s",
			weatherInfo: &CurrentWeatherInfo{},
		},
	}
}

func main() {
	weatherFacade := NewWeatherFacade("83b1a1735388d20d2440b8b00f6dba78")
	cities := []string{"Минск", "Киев", "Вильнюс", "Якутск"}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %d\n", temperature)
		fmt.Printf("Humidity in "+city+": %d\n", humidity)
		fmt.Printf("Wind speed in "+city+": %d\n\n", windSpeed)
	}
}

/* Output:
Temperature in Минск: 7
Humidity in Минск: 56
Wind speed in Минск: 3

Temperature in Киев: 16
Humidity in Киев: 46
Wind speed in Киев: 0

Temperature in Вильнюс: 9
Humidity in Вильнюс: 29
Wind speed in Вильнюс: 4

Temperature in Якутск: 0
Humidity in Якутск: 64
Wind speed in Якутск: 3 */
