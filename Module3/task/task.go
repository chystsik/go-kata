package task

import (
	"fmt"
	"io"
	"os"
	"sort"
	"time"

	"github.com/brianvoe/gofakeit/v6"
	jsoniter "github.com/json-iterator/go"
)

const (
	errEmptySource         = "empty source"
	errSourceLength        = "no more than 3 sources allowed"
	errSameTypeSourceCount = "multiple source count of same type"
	errEmptyList           = "empty list"
	errOutOfLength         = "list length out of range [%d] with length %d"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message" fake:"{hackerphrase}"`
	UUID    string    `json:"uuid" fake:"{uuid}"`
	Date    time.Time `json:"date" fake:"{date}"`
}

type LinkedLister interface {
	// LoadData загрузка данных из подготовленного json файла по пути path (string), сгенерированных n (int) fake записей, слайса []Commit, либо все вместе
	LoadData(source ...interface{}) error
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
}

func NewDoubleLinkedList() *DoubleLinkedList {
	return &DoubleLinkedList{head: nil, tail: nil, curr: nil, len: 0}
}

// LoadData загрузка данных из подготовленного json файла по пути path (string), сгенерированных n (int) записей, слайса []Commit, либо все вместе
func (d *DoubleLinkedList) LoadData(source ...interface{}) error {
	// отсортировать список используя самописный QuickSort
	if source == nil {
		return fmt.Errorf(errEmptySource)
	}
	var path string
	var n int
	var commits []Commit
	// определяем источники данных
	if len(source) > 3 {
		return fmt.Errorf(errSourceLength)
	}
	for _, s := range source {
		switch v := s.(type) {
		case string:
			if path != "" {
				return fmt.Errorf(errSameTypeSourceCount)
			}
			path = v
		case int:
			if n != 0 {
				return fmt.Errorf(errSameTypeSourceCount)
			}
			n = v
		case []Commit:
			if len(commits) > 0 {
				return fmt.Errorf(errSameTypeSourceCount)
			}
			commits = v
		}

	}
	// загружаем из файла, если был указан путь
	if path != "" {
		fromFileCommits, err := ReadJSON(path)
		if err != nil {
			return err
		}
		commits = append(commits, fromFileCommits...)
	}
	// генерируем fake данные, если было указано количество
	if n != 0 {
		commits = append(commits, GenerateJSON(n)...)
	}
	// сортируем
	CustomQuickSort(commits)
	// заполняем двусвязный список
	d.head = &Node{}
	d.curr = d.head
	p := d.head
	p.prev = nil

	for i := 0; i < len(commits); i++ {
		p.data = &commits[i]
		p.next = &Node{prev: p}
		p = p.next
	}
	d.len += len(commits)
	d.tail = p.prev
	d.tail.next = nil
	return nil
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.curr == nil {
		return nil
	}
	if d.curr.next == nil {
		return nil
	}
	new := d.curr.next
	d.curr = new
	return d.curr
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.curr == nil {
		return nil
	}
	if d.curr.prev == nil {
		return nil
	}
	d.curr = d.curr.prev
	return d.curr
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	if d.head == nil {
		return fmt.Errorf(errEmptyList)
	}
	if d.len < n {
		return fmt.Errorf(errOutOfLength, n, d.len)
	}

	var new *Node
	p := d.head
	for i := 2; i <= n; i++ {
		p = p.next
	}

	if d.len == n {
		new = &Node{data: &c, prev: p, next: nil}
	} else {
		new = &Node{data: &c, prev: p, next: p.next}
		p.next.prev = new
	}

	p.next = new
	d.len++
	return nil
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if d.head == nil {
		return fmt.Errorf(errEmptyList)
	}
	if d.len < n {
		return fmt.Errorf(errOutOfLength, n, d.len)
	}

	p := d.head
	for i := 2; i <= n; i++ {
		p = p.next
	}

	if p.next == nil { // last node
		if d.curr == d.tail {
			d.curr = p.prev
		}
		d.tail = p.prev
		p.prev.next = nil
	} else if p.prev == nil { // first node
		if d.curr == d.head {
			d.curr = p.next
		}
		d.head = p.next
		p.next.prev = nil
	} else { // middle node
		if d.curr == p {
			d.curr = p.next
		}
		next := p.next
		prev := p.prev
		p.prev.next = next
		p.next.prev = prev
	}
	d.len--
	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.curr == nil {
		return fmt.Errorf(errEmptyList)
	}
	if d.len == 1 {
		d.curr = nil
		d.head = nil
		d.tail = nil
	} else if d.curr == d.head { // current is first element of the list
		curr := d.curr.next
		d.curr.next.prev = nil
		d.curr = curr
	} else if d.curr == d.tail { // current is last element of the list
		curr := d.curr.prev
		d.curr.prev.next = nil
		d.curr = curr
	} else { // current in the middle of the list
		next := d.curr.next
		prev := d.curr.prev
		d.curr.prev.next = next
		d.curr.next.prev = prev
		d.curr = next
	}
	d.len--
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.head == nil {
		return 0, fmt.Errorf(errEmptyList)
	}

	p, idx := d.head, 1
	for p != d.curr {
		idx++
		p = p.next
	}
	return idx, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	if d.head == nil {
		return nil
	}

	last := d.tail
	if d.curr == d.tail {
		d.curr = last.prev
	}

	d.tail = last.prev
	last.prev.next = nil
	d.len--
	return last
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	if d.head == nil {
		return nil
	}

	first := d.head
	if d.curr == d.head {
		d.curr = first.next
	}

	d.head = first.next
	first.next.prev = nil
	d.len--
	return first
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	p := d.head
	for p != nil {
		if p.data.UUID == uuID {
			break
		}
		p = p.next
	}
	return p
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	p := d.head
	for p != nil {
		if p.data.Message == message {
			break
		}
		p = p.next
	}
	return p
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	head, tail := d.head, d.tail

	p := d.tail
	for p != nil {
		p.next, p.prev = p.prev, p.next
		p = p.next
	}

	d.head, d.tail = tail, head
	return d
}

func GenerateJSON(count int) []Commit {
	// Дополнительное задание написать генератор данных
	// используя библиотеку gofakeit
	faker := gofakeit.New(0)
	commits := make([]Commit, count)

	for i := 0; i < count; i++ {
		err := faker.Struct(&commits[i])
		if err != nil {
			panic(err)
		}
	}
	return commits
}

func ReadJSON(name string) ([]Commit, error) {
	f, err := os.Open(name)

	if err != nil {
		return nil, err
	}
	defer f.Close()

	data, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}

	var commits []Commit

	err = jsoniter.Unmarshal(data, &commits)
	if err != nil {
		return nil, err
	}
	return commits, nil
}

func CustomQuickSort(commits []Commit) {
	if len(commits) < 2 {
		return
	}

	lo, hi := 0, len(commits)-1
	mid := len(commits) / 2

	commits[mid], commits[hi] = commits[hi], commits[mid]

	for i := range commits {
		if commits[i].Date.Before(commits[hi].Date) {
			commits[lo], commits[i] = commits[i], commits[lo]
			lo++
		}
	}

	commits[lo], commits[hi] = commits[hi], commits[lo]

	CustomQuickSort(commits[:lo])
	CustomQuickSort(commits[lo+1:])
}

func StdLibSliceSort(commits []Commit) {
	sort.Slice(commits, func(i, j int) bool { return commits[i].Date.Before(commits[j].Date) })
}

func BubbleSort(commits []Commit) {
	for i := 0; i < len(commits); i++ {
		for j := 0; j < len(commits); j++ {
			if commits[i].Date.Before(commits[j].Date) {
				commits[i], commits[j] = commits[j], commits[i]
			}
		}
	}
}
