package task

import "testing"

func Benchmark_Bubble(b *testing.B) {
	dataSet := generateData(b.N, 2000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSort(dataSet[i])
	}
}

func Benchmark_Quick(b *testing.B) {
	dataSet := generateData(b.N, 2000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		CustomQuickSort(dataSet[i])
	}
}

func Benchmark_StdLibSliceSort(b *testing.B) {
	dataSet := generateData(b.N, 2000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		StdLibSliceSort(dataSet[i])
	}
}

func Benchmark_FileBubble(b *testing.B) {
	dataSet := readDataFromFile(b.N)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSort(dataSet[i])
	}
}

func Benchmark_FileQuick(b *testing.B) {
	dataSet := readDataFromFile(b.N)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		CustomQuickSort(dataSet[i])
	}
}

func Benchmark_FileStdLibSliceSort(b *testing.B) {
	dataSet := readDataFromFile(b.N)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		StdLibSliceSort(dataSet[i])
	}
}

func readDataFromFile(n int) [][]Commit {
	data := make([][]Commit, n)
	fileData, err := ReadJSON("test.json")
	if err != nil {
		panic(err)
	}
	for i := 0; i < n; i++ {
		data[i] = make([]Commit, len(fileData))
		copy(data[i], fileData)
	}
	return data
}

func generateData(n, count int) [][]Commit {
	data := make([][]Commit, n)
	fakeData := GenerateJSON(count)
	for i := 0; i < n; i++ {
		data[i] = make([]Commit, count)
		copy(data[i], fakeData)
	}
	return data
}
