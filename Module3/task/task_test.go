package task

import (
	"errors"
	"io/fs"
	"syscall"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNewDLL_ReturnsNotNil(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()

	assert.NotNil(t, dll)
}

func TestLoadData_VariadicInput_NoError(t *testing.T) {
	t.Parallel()
	commit := []Commit{
		{Message: "1", UUID: "1", Date: time.Now()},
		{Message: "2", UUID: "2", Date: time.Now()},
		{Message: "3", UUID: "3", Date: time.Now()},
	}
	fake := 1000
	path := "test.json"

	dll := NewDoubleLinkedList()
	err := dll.LoadData(path, fake, commit)
	assert.NotNil(t, dll.head)
	assert.NotNil(t, dll.tail)
	assert.NoError(t, err)
}

func TestLoadData_VariadicInput_ReturnsError(t *testing.T) {
	t.Parallel()
	commit := []Commit{
		{Message: "1", UUID: "1", Date: time.Now()},
		{Message: "2", UUID: "2", Date: time.Now()},
		{Message: "3", UUID: "3", Date: time.Now()},
	}
	fake := 1000
	path := "test.json"

	dll := NewDoubleLinkedList()
	errLen := dll.LoadData(path, fake, commit, commit)
	errMultiplePath := dll.LoadData(path, fake, path)
	errMultipleFake := dll.LoadData(path, fake, fake)
	errMultipleCommits := dll.LoadData(commit, commit, fake)
	assert.EqualError(t, errLen, errSourceLength)
	assert.EqualError(t, errMultiplePath, errSameTypeSourceCount)
	assert.EqualError(t, errMultipleFake, errSameTypeSourceCount)
	assert.EqualError(t, errMultipleCommits, errSameTypeSourceCount)
}

func TestLoadData(t *testing.T) {
	t.Parallel()
	// Define test cases
	testCases := []struct {
		name   string
		input  interface{}
		expErr error
	}{
		{
			name:   "Load empty source",
			input:  nil,
			expErr: errors.New(errEmptySource),
		},
		{
			name:   "Wrong path to json file",
			input:  "path",
			expErr: &fs.PathError{Op: "open", Path: "path", Err: syscall.Errno(0x2)},
		},
		{
			name:   "Load valid json file",
			input:  "test.json",
			expErr: nil,
		},
		{
			name:   "Load fake data",
			input:  10,
			expErr: nil,
		},
		{
			name: "Load slice",
			input: []Commit{
				{Message: "1", UUID: "1", Date: time.Now()},
				{Message: "2", UUID: "2", Date: time.Now()},
				{Message: "3", UUID: "3", Date: time.Now()},
			},
			expErr: nil,
		},
	}
	// Run test cases
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var err error
			// Create a new doubly linked list
			dll := NewDoubleLinkedList()
			// Test function
			if tc.input == nil {
				err = dll.LoadData()
			} else {
				err = dll.LoadData(tc.input)
			}
			// Check the result
			assert.Equal(t, tc.expErr, err)
		})
	}
}

func TestLoadData_ValidateDll(t *testing.T) {
	t.Parallel()
	actualDll := NewDoubleLinkedList()
	expectedDll := NewDoubleLinkedList()
	input := []Commit{
		{Message: "1", UUID: "1", Date: time.Date(2009, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "5", UUID: "5", Date: time.Date(2013, time.November, 10, 15, 0, 0, 0, time.Local)},
	}

	errLoadActual := actualDll.LoadData(input)
	for i := range input {
		expectedDll.PushBack(&input[i])
	}
	expectedDll.len = len(input)

	assert.NoError(t, errLoadActual)
	assert.Equal(t, expectedDll.len, actualDll.len)
	// Check that the list has the expected values
	for a, e := actualDll.Front(), expectedDll.Front(); a != nil && e != nil; a, e = a.next, e.next {
		assert.Equal(t, e.data, a.data)
	}
}

func TestLen_ReturnsNil(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	length := dll.Len()
	expectedLen := 0

	assert.Equal(t, expectedLen, length)
}

func TestLen_ReturnsLen(t *testing.T) {
	t.Parallel()
	commits := []Commit{
		{Message: "1", UUID: "1", Date: time.Now()},
		{Message: "2", UUID: "2", Date: time.Now()},
		{Message: "3", UUID: "3", Date: time.Now()},
	}
	fake := 1000
	path := "test.json"
	expectedLen := 3003

	dll := NewDoubleLinkedList()
	err := dll.LoadData(path, fake, commits)
	length := dll.Len()

	assert.NoError(t, err)
	assert.Equal(t, expectedLen, length)
}

func TestCurrent_ReturnsNode(t *testing.T) {
	t.Parallel()
	commits := GenerateJSON(5)
	dll := NewDoubleLinkedList()
	err := dll.LoadData(commits)
	curr := dll.Current()

	assert.NoError(t, err)
	assert.Equal(t, dll.curr, curr)
}

func TestCurrent_ReturnsNil(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	curr := dll.Current()

	assert.Equal(t, dll.curr, curr)
}

func TestNext_ReturnsNode(t *testing.T) {
	t.Parallel()
	commits := GenerateJSON(5)
	dll := NewDoubleLinkedList()
	err := dll.LoadData(commits)
	exp := dll.curr.next
	next := dll.Next()

	assert.NoError(t, err)
	assert.Equal(t, exp, next)
}

func TestNext_WhenCurrentNil_ReturnsNil(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	next := dll.Next()

	assert.Equal(t, dll.curr, next)
}

func TestNext_WhenNextNil_ReturnsNil(t *testing.T) {
	t.Parallel()
	commits := GenerateJSON(1)
	dll := NewDoubleLinkedList()
	err := dll.LoadData(commits)
	next := dll.Next()

	assert.NoError(t, err)
	assert.Equal(t, dll.curr.next, next)
}

func TestPrev_ReturnsNode(t *testing.T) {
	t.Parallel()
	commits := GenerateJSON(5)
	dll := NewDoubleLinkedList()
	err := dll.LoadData(commits)
	dll.Next()
	exp := dll.curr.prev
	prev := dll.Prev()

	assert.NoError(t, err)
	assert.Equal(t, exp, prev)
}

func TestPrev_WhenCurrentNil_ReturnsNil(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	prev := dll.Prev()

	assert.Equal(t, dll.curr, prev)
}

func TestPrev_WhenPrevNil_ReturnsNil(t *testing.T) {
	t.Parallel()
	commits := GenerateJSON(1)
	dll := NewDoubleLinkedList()
	err := dll.LoadData(commits)
	prev := dll.Prev()

	assert.NoError(t, err)
	assert.Equal(t, dll.curr.next, prev)
}

func TestInsert(t *testing.T) {
	t.Parallel()
	// Define test cases
	testCases := []struct {
		name     string
		input    []Commit
		commit   Commit
		position int
		expected []Commit
		expErr   string
	}{
		{
			name:     "Empty dll",
			commit:   Commit{Message: "1", UUID: "1", Date: time.Now()},
			position: 2,
			expErr:   errEmptyList,
		},
		{
			name: "n out of dll length",
			input: []Commit{
				{Message: "1", UUID: "1", Date: time.Time{}},
				{Message: "2", UUID: "2", Date: time.Time{}},
				{Message: "3", UUID: "3", Date: time.Time{}},
			},
			commit:   Commit{Message: "1", UUID: "1", Date: time.Time{}},
			position: 5,
			expErr:   errOutOfLength,
		},
		{
			name: "insert to the middle of the list",
			input: []Commit{
				{Message: "1", UUID: "1", Date: time.Date(2009, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "5", UUID: "5", Date: time.Date(2013, time.November, 10, 15, 0, 0, 0, time.Local)},
			},
			commit:   Commit{Message: "3", UUID: "3", Date: time.Date(2011, time.November, 10, 15, 0, 0, 0, time.Local)},
			position: 2,
			expected: []Commit{
				{Message: "1", UUID: "1", Date: time.Date(2009, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "3", UUID: "3", Date: time.Date(2011, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "5", UUID: "5", Date: time.Date(2013, time.November, 10, 15, 0, 0, 0, time.Local)},
			},
		},
		{
			name: "insert after first list element",
			input: []Commit{
				{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
			},
			commit:   Commit{Message: "3", UUID: "3", Date: time.Date(2011, time.November, 10, 15, 0, 0, 0, time.Local)},
			position: 1,
			expected: []Commit{
				{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "3", UUID: "3", Date: time.Date(2011, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
			},
		},
		{
			name: "insert after last list element",
			input: []Commit{
				{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
			},
			commit:   Commit{Message: "3", UUID: "3", Date: time.Date(2011, time.November, 10, 15, 0, 0, 0, time.Local)},
			position: 2,
			expected: []Commit{
				{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "3", UUID: "3", Date: time.Date(2011, time.November, 10, 15, 0, 0, 0, time.Local)},
			},
		},
	}
	// Run test cases
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// Create a new doubly linked list
			dll := NewDoubleLinkedList()
			if tc.input != nil {
				err := dll.LoadData(tc.input)
				assert.NoError(t, err)
			}
			// Test function
			err := dll.Insert(tc.position, tc.commit)
			// Check expected error
			if tc.expErr != "" {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			// Check that the list has the expected values
			for i, e := dll.Front(), 0; e < len(tc.expected) && i != nil; i, e = i.next, e+1 {
				if *i.data != tc.expected[e] {
					t.Errorf("Test case %s failed: expected %v but got %v", tc.name, tc.expected[e], *i.data)
				}
			}
		})
	}
}

func TestDelete(t *testing.T) {
	t.Parallel()
	// Define test cases
	testCases := []struct {
		name     string
		input    []Commit
		position int
		expected []Commit
		expErr   string
	}{
		{
			name:     "Empty dll",
			position: 2,
			expErr:   errEmptyList,
		},
		{
			name: "n out of dll length",
			input: []Commit{
				{Message: "1", UUID: "1", Date: time.Time{}},
				{Message: "2", UUID: "2", Date: time.Time{}},
				{Message: "3", UUID: "3", Date: time.Time{}},
			},
			position: 5,
			expErr:   errOutOfLength,
		},
		{
			name: "delete from the middle of the list",
			input: []Commit{
				{Message: "1", UUID: "1", Date: time.Date(2009, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "5", UUID: "5", Date: time.Date(2013, time.November, 10, 15, 0, 0, 0, time.Local)},
			},
			position: 2,
			expected: []Commit{
				{Message: "1", UUID: "1", Date: time.Date(2009, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "5", UUID: "5", Date: time.Date(2013, time.November, 10, 15, 0, 0, 0, time.Local)},
			},
		},
		{
			name: "delete first list element",
			input: []Commit{
				{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
			},
			position: 1,
			expected: []Commit{
				{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
			},
		},
		{
			name: "delete last list element",
			input: []Commit{
				{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
				{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
			},
			position: 2,
			expected: []Commit{
				{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
			},
		},
	}
	// Run test cases
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// Create a new doubly linked list
			dll := NewDoubleLinkedList()
			if tc.input != nil {
				err := dll.LoadData(tc.input)
				assert.NoError(t, err)
			}
			// Test function
			err := dll.Delete(tc.position)
			// Check expected error
			if tc.expErr != "" {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
			// Check that the list has the expected values
			for i, e := dll.Front(), 0; e < len(tc.expected) && i != nil; i, e = i.next, e+1 {
				if *i.data != tc.expected[e] {
					t.Errorf("Test case %s failed: expected %v but got %v", tc.name, tc.expected[e], *i.data)
				}
			}
		})
	}
}

func TestDelete_CurrentIsHead(t *testing.T) {
	t.Parallel()
	input := []Commit{
		{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
	}
	expected := []Commit{
		{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
	}
	position := 1

	dll := NewDoubleLinkedList()
	errLoad := dll.LoadData(input)
	curr := dll.Current()
	errDelete := dll.Delete(position)
	newCurr := dll.Current()

	assert.NotNil(t, dll.head)
	assert.NoError(t, errLoad)
	assert.NoError(t, errDelete)
	assert.Equal(t, curr.next, newCurr)
	// Check that the list has the expected values
	for i, e := dll.Front(), 0; e < len(expected) && i != nil; i, e = i.next, e+1 {
		assert.Equal(t, expected[e], *i.data)
	}
}

func TestDelete_CurrentIsTail(t *testing.T) {
	t.Parallel()
	input := []Commit{
		{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
	}
	expected := []Commit{
		{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
	}
	position := 2

	dll := NewDoubleLinkedList()
	errLoad := dll.LoadData(input)
	dll.Next()
	curr := dll.Current()
	errDelete := dll.Delete(position)
	newCurr := dll.Current()

	assert.NotNil(t, dll.head)
	assert.NoError(t, errLoad)
	assert.NoError(t, errDelete)
	assert.Equal(t, curr.prev, newCurr)
	// Check that the list has the expected values
	for i, e := dll.Front(), 0; e < len(expected) && i != nil; i, e = i.next, e+1 {
		assert.Equal(t, expected[e], *i.data)
	}
}

func TestDelete_CurrentInMiddle(t *testing.T) {
	t.Parallel()
	input := []Commit{
		{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "3", UUID: "3", Date: time.Date(2011, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
	}
	expected := []Commit{
		{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
	}
	position := 2

	dll := NewDoubleLinkedList()
	errLoad := dll.LoadData(input)
	dll.Next()
	curr := dll.Current()
	errDelete := dll.Delete(position)
	newCurr := dll.Current()

	assert.NotNil(t, dll.head)
	assert.NoError(t, errLoad)
	assert.NoError(t, errDelete)
	assert.Equal(t, curr.next, newCurr)
	// Check that the list has the expected values
	for i, e := dll.Front(), 0; e < len(expected) && i != nil; i, e = i.next, e+1 {
		assert.Equal(t, expected[e], *i.data)
	}
}

func TestDeleteCurrent_CurrentIsHead(t *testing.T) {
	t.Parallel()
	input := []Commit{
		{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
	}
	dll := NewDoubleLinkedList()
	errLoad := dll.LoadData(input)
	curr := dll.Current()
	errDelete := dll.DeleteCurrent()
	newCurr := dll.Current()

	assert.NotNil(t, dll.head)
	assert.NoError(t, errLoad)
	assert.NoError(t, errDelete)
	assert.Equal(t, curr.next, newCurr)
}

func TestDeleteCurrent_CurrentIsTail(t *testing.T) {
	t.Parallel()
	input := []Commit{
		{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
	}
	dll := NewDoubleLinkedList()
	errLoad := dll.LoadData(input)
	dll.Next()
	curr := dll.Current()
	errDelete := dll.DeleteCurrent()
	newCurr := dll.Current()

	assert.NotNil(t, dll.head)
	assert.NoError(t, errLoad)
	assert.NoError(t, errDelete)
	assert.Equal(t, curr.prev, newCurr)
}

func TestDeleteCurrent_CurrentInMiddle(t *testing.T) {
	t.Parallel()
	input := []Commit{
		{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "3", UUID: "3", Date: time.Date(2011, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
	}
	dll := NewDoubleLinkedList()
	errLoad := dll.LoadData(input)
	dll.Next()
	curr := dll.Current()
	errDelete := dll.DeleteCurrent()
	newCurr := dll.Current()

	assert.NotNil(t, dll.head)
	assert.NoError(t, errLoad)
	assert.NoError(t, errDelete)
	assert.Equal(t, curr.next, newCurr)
}

func TestDeleteCurrent_CurrentIsLastElement(t *testing.T) {
	t.Parallel()
	input := []Commit{
		{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
	}
	dll := NewDoubleLinkedList()
	errLoad := dll.LoadData(input)
	errDelete := dll.DeleteCurrent()
	newCurr := dll.Current()

	assert.NoError(t, errLoad)
	assert.NoError(t, errDelete)
	assert.Nil(t, dll.head)
	assert.Nil(t, newCurr)
	assert.Nil(t, dll.tail)
}

func TestDeleteCurrent_ReturnsError(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	errDelete := dll.DeleteCurrent()

	assert.EqualError(t, errDelete, errEmptyList)
}

func TestIndex_ReturnsError(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	expexted := 0
	actual, errIndex := dll.Index()

	assert.Equal(t, expexted, actual)
	assert.EqualError(t, errIndex, errEmptyList)
}

func TestIndex_ReturnsIndex(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	input := GenerateJSON(10)
	errLoad := dll.LoadData(input)
	expexted := 5
	for i := 1; i < expexted; i++ {
		dll.Next()
	}
	actual, errIndex := dll.Index()

	assert.NoError(t, errLoad)
	assert.NoError(t, errIndex)
	assert.Equal(t, expexted, actual)
}

func TestPop_ReturnsNil(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	expected := dll.Pop()

	assert.Nil(t, expected)
}

func TestPop_ReturnsResult(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	input := GenerateJSON(10)
	errLoad := dll.LoadData(input)
	expected := dll.tail
	actual := dll.Pop()

	assert.NoError(t, errLoad)
	assert.Equal(t, expected, actual)
}

func TestPop_CurrentIsTail_ReturnsResult(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	input := GenerateJSON(10)
	errLoad := dll.LoadData(input)

	for i := 1; i < len(input); i++ {
		dll.Next()
	}

	expected := dll.Current()
	actual := dll.Pop()

	assert.NoError(t, errLoad)
	assert.Equal(t, expected, actual)
}

func TestShift_ReturnsNil(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	expected := dll.Shift()

	assert.Nil(t, expected)
}

func TestShift_ReturnsResult(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	input := GenerateJSON(10)
	errLoad := dll.LoadData(input)
	expected := dll.head
	actual := dll.Shift()

	assert.NoError(t, errLoad)
	assert.Equal(t, expected, actual)
}

func TestShift_CurrentIsHead_ReturnsResult(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	input := GenerateJSON(10)
	errLoad := dll.LoadData(input)
	expected := dll.Current()
	actual := dll.Shift()

	assert.NoError(t, errLoad)
	assert.Equal(t, expected, actual)
}

func TestSearchUUID_ReturnsNil(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	actual := dll.SearchUUID("abc")

	assert.Nil(t, actual)
}

func TestSearchUUID_ReturnsResult(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	input := GenerateJSON(100)
	expected := input[49]
	errLoad := dll.LoadData(input)
	actual := dll.SearchUUID(expected.UUID)

	assert.NoError(t, errLoad)
	assert.Equal(t, expected.UUID, actual.data.UUID)
}

func TestSearch_ReturnsNil(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	actual := dll.Search("abc")

	assert.Nil(t, actual)
}

func TestSearch_ReturnsResult(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	input := GenerateJSON(100)
	expected := input[49]
	errLoad := dll.LoadData(input)
	actual := dll.Search(expected.Message)

	assert.NoError(t, errLoad)
	assert.Equal(t, expected.Message, actual.data.Message)
}

func TestReverse(t *testing.T) {
	t.Parallel()
	dll := NewDoubleLinkedList()
	input := []Commit{
		{Message: "1", UUID: "1", Date: time.Date(2009, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "3", UUID: "3", Date: time.Date(2011, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
	}
	expected := []Commit{
		{Message: "4", UUID: "4", Date: time.Date(2012, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "3", UUID: "3", Date: time.Date(2011, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "2", UUID: "2", Date: time.Date(2010, time.November, 10, 15, 0, 0, 0, time.Local)},
		{Message: "1", UUID: "1", Date: time.Date(2009, time.November, 10, 15, 0, 0, 0, time.Local)},
	}
	errLoad := dll.LoadData(input)
	newDll := dll.Reverse()

	assert.NoError(t, errLoad)
	for i, e := newDll.Front(), 0; e < len(expected) && i != nil; i, e = i.next, e+1 {
		assert.Equal(t, expected[e], *i.data)
	}

}

func TestCustomSort(t *testing.T) {
	t.Parallel()
	input := GenerateJSON(100)
	CustomQuickSort(input)

	for i := 0; i < len(input)-1; i++ {
		assert.Less(t, input[i].Date, input[i+1].Date)
		if input[i].Date.After(input[i+1].Date) {
			t.Errorf("Test case %s failed: expected date %v before date %v", t.Name(), input[i].Date, input[i+1].Date)
		}
	}
}

func TestStdLibSliceSort(t *testing.T) {
	t.Parallel()
	input := GenerateJSON(100)
	StdLibSliceSort(input)

	for i := 0; i < len(input)-1; i++ {
		assert.Less(t, input[i].Date, input[i+1].Date)
	}
}

func TestBubbleSort(t *testing.T) {
	t.Parallel()
	input := GenerateJSON(100)
	BubbleSort(input)

	for i := 0; i < len(input)-1; i++ {
		assert.Less(t, input[i].Date, input[i+1].Date)
	}
}

// PushBack adds a new element to the back of the doubly linked list.
func (dll *DoubleLinkedList) PushBack(dat *Commit) {
	node := &Node{data: dat}

	if dll.head == nil {
		dll.head = node
		dll.tail = node
		return
	}

	node.prev = dll.tail
	dll.tail.next = node
	dll.tail = node
}

// Front returns the first element of the doubly linked list.
func (dll *DoubleLinkedList) Front() *Node {
	return dll.head
}
