package main

import (
	"fmt"
	"runtime"

	"gitlab.com/chystsik/go-kata/features"
)

func main() {
	hl := "Hello Kata!"
	f1 := features.FeatureOne()
	b, err := fmt.Printf("%s\n%s\n%s\n", hl, f1, features.Feature2())
	fmt.Printf("printed %d bytes, with errors: %v\n", b, err)
	fmt.Println(runtime.NumCPU())
}
