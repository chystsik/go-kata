package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const (
	timeout time.Duration = 30 * time.Second
	tick    time.Duration = 1 * time.Millisecond
)

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		fmt.Println("close ch main")
		close(mergedCh)
	}()

	return mergedCh
}

func generateData(done chan bool, ticker *time.Ticker) chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case <-done:
				fmt.Printf("stop data generator\nclose ch out\n")
				return
			case _, ok := <-out:
				if !ok {
					return
				}
			case <-ticker.C:
				out <- rand.Intn(100)
			}
		}
	}()

	return out
}

func main() {
	//rand.Seed(time.Now().UnixNano()) // rand.Seed has been deprecated since Go 1.20

	ticker := time.NewTicker(tick)
	done := make(chan bool)

	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData(done, ticker)

	go func() {
		for num := range out {
			a <- num
		}
		fmt.Println("close ch A")
		close(a)
	}()

	go func() {
		for num := range out {
			b <- num
		}
		fmt.Println("close ch B")
		close(b)
	}()

	go func() {
		for num := range out {
			c <- num
		}
		fmt.Println("close ch C")
		close(c)
	}()

	go func() {
		time.Sleep(timeout)
		fmt.Println("Time out")
		ticker.Stop()
		done <- true
	}()

	mainChan := joinChannels(a, b, c)

	for num := range mainChan {
		fmt.Println(num)
	}

	_, okM := <-mainChan
	_, okO := <-out
	_, okA := <-a
	_, okB := <-b
	_, okC := <-c
	fmt.Printf("Some channel still open? Main:%t, Out:%t, A:%t, B:%t, C:%t\n", okM, okO, okA, okB, okC)
}
