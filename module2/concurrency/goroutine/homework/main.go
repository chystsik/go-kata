package main

import (
	"fmt"
	"net/http"
	"sync"
	"time"
)

func main() {
	urls := []string{
		"https://google.com",
		"https://github.com",
		"https://youtube.com",
		"https://facebook.com",
		"https://instagram.com",
	}

	waitGroup(urls)
	messages()
}

func waitGroup(urls []string) {
	var wg sync.WaitGroup
	for _, url := range urls {

		wg.Add(1)
		url := url

		go func() {
			doHTTP(url)
			wg.Done()
		}()
	}

	wg.Wait()
}

func doHTTP(url string) {
	t := time.Now()

	resp, err := http.Get(url)
	if err != nil {
		fmt.Printf("Failed to get %s: %v\n", url, err)
	}
	defer resp.Body.Close()

	fmt.Printf("%s - Status Code %d - Latency %d ms\n",
		url, resp.StatusCode, time.Since(t).Milliseconds())
}

func messages() {
	mes1 := make(chan string)
	mes2 := make(chan string)
	timeout := make(chan string)

	go func() {
		for {
			time.Sleep(time.Millisecond * 500)
			mes1 <- "Прошло пол секунды"
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 2)
			mes2 <- "Прошло 2 секунды"
		}
	}()

	go func() {
		for {
			time.Sleep(time.Second * 5)
			timeout <- "Таймаут 5 секунд"
		}
	}()

	for {
		select {
		case msg := <-mes1:
			fmt.Println(msg)
		case msg := <-mes2:
			fmt.Println(msg)
		case msg := <-timeout:
			fmt.Println(msg)
			return
		}
	}
}
