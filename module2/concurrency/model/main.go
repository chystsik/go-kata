package main

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
)

func add(numbers []int) int {
	var sum int

	for _, n := range numbers {
		sum += n
	}

	return sum
}

func addConcurrent(goroutines int, numbers []int) int {
	var sum int64

	totalNumbers := len(numbers)
	lastGoroutine := goroutines - 1
	stride := totalNumbers / goroutines

	var wg sync.WaitGroup
	wg.Add(goroutines)

	for g := 0; g < goroutines; g++ {

		go func(g int) {
			start := g * stride
			end := start + stride

			if g == lastGoroutine {
				end = totalNumbers
			}

			var lv int
			for _, n := range numbers[start:end] {
				lv += n
			}

			atomic.AddInt64(&sum, int64(lv))
			wg.Done()
		}(g)
	}

	wg.Wait()

	return int(sum)
}

func main() {
	fmt.Println(add([]int{1, 2, 3, 4, 5}))
	fmt.Println(addConcurrent(runtime.NumCPU(), []int{1, 2, 3, 4, 5}))
}
