package main

import (
	"runtime"
	"testing"
)

/*	concurrent run				- GOGC=off go test -cpu 1 -run none -bench . -benchmem -benchtime 3s
Benchmark_Add                       6762            504933 ns/op               0 B/op          0 allocs/op 	1 goroutine with 1 core
Benchmark_AddConcurrent             2886           1091513 ns/op             856 B/op         18 allocs/op 	8 goroutine with 1 core
	concurrent parrallel run	- GOGC=off go test -cpu 8 -run none -bench . -benchmem -benchtime 3s
Benchmark_Add-8                     6772            526645 ns/op               0 B/op          0 allocs/op	1 goroutine with 8 core
Benchmark_AddConcurrent-8           7851            456414 ns/op             856 B/op         18 allocs/op	8 goroutine with 8 core
*/

var numbers = make([]int, 2_000_000)

func init() {
	for i := 0; i < len(numbers); i++ {
		numbers[i] = i
	}
}

func Benchmark_Add(b *testing.B) {

	for i := 0; i < b.N; i++ {
		add(numbers)
	}
}

func Benchmark_AddConcurrent(b *testing.B) {

	for i := 0; i < b.N; i++ {
		addConcurrent(runtime.NumCPU(), numbers)
	}
}
