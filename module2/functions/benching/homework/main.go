package main

import (
	"fmt"
	"math/rand"

	"github.com/brianvoe/gofakeit/v6"
)

type User struct {
	ID       int64
	Name     string `fake:"{firstname}"`
	Products []Product
}

type Product struct {
	UserID int64
	Name   string `fake:"{sentence:3}"`
}

func main() {
	users := genUsers()
	users2 := make([]User, len(users))
	users3 := make([]User, len(users))
	copy(users2, users)
	copy(users3, users)
	//fmt.Println(users)
	//fmt.Println(users2)
	products := genProducts()
	//fmt.Println(products)
	users = MapUserProducts(users, products)
	users2 = MapUserProducts2(users2, products)
	users3 = MapUserProducts3(users3, products)
	fmt.Println(users[98])
	fmt.Println(users2[98])
	fmt.Println(users3[98])
}

func MapUserProducts(users []User, products []Product) []User {
	// Проинициализируйте карту продуктов по айди пользователей
	for i, user := range users {
		for _, product := range products { // избавьтесь от цикла в цикле
			if product.UserID == user.ID {
				users[i].Products = append(users[i].Products, product)
			}
		}
	}

	return users
}

func MapUserProducts2(users []User, products []Product) []User {
	// Проинициализируйте карту продуктов по айди пользователей
	tmp := make(map[int64][]Product, len(products))

	for _, p := range products {
		tmp[p.UserID] = append(tmp[p.UserID], p)
	}

	for i, user := range users { // избавьтесь от цикла в цикле
		users[i].Products = tmp[user.ID]
	}

	return users
}

func MapUserProducts3(users []User, products []Product) []User {
	// we can merge 2 slices without using map - best performance way :)
	for _, p := range products {
		users[p.UserID].Products = append(users[p.UserID].Products, p)
	}

	return users
}

func genProducts() []Product {
	products := make([]Product, 1000)
	for i, product := range products {
		_ = gofakeit.Struct(&product)
		product.UserID = int64(rand.Intn(100)) // + 1) // we have 0-99 userIDs // half-open interval [0,n)
		products[i] = product
	}

	return products
}

func genUsers() []User {
	users := make([]User, 100)
	for i, user := range users {
		//_ = gofakeit.Struct(&user) // adds bad products with incorrect userIDs
		user.ID = int64(i)
		user.Name = gofakeit.FirstName()
		users[i] = user
	}

	return users
}
