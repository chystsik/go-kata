package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	users    = genUsers()
	products = genProducts()
)

func BenchmarkSample(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts(users, products)
	}
}

func BenchmarkSample2_usingMap(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users, products)
	}
}

func BenchmarkSample3_mergeSlices(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts3(users, products)
	}
}

func TestUsersSlicesEqual(t *testing.T) {
	u1 := MapUserProducts(users, products)
	u2 := MapUserProducts2(users, products)
	u3 := MapUserProducts3(users, products)
	assert.Equal(t, u1, u2)
	assert.Equal(t, u1, u3)
	assert.True(t, usersSlicesEqual(u1, u2))
	assert.True(t, usersSlicesEqual(u1, u3))
}

func usersSlicesEqual(a, b []User) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v.ID != b[i].ID || v.Name != b[i].Name || len(v.Products) != len(b[i].Products) {
			return false
		}
	}
	return true
}
