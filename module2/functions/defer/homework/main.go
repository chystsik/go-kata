package main

import (
	"fmt"

	"gitlab.com/chystsik/go-kata/module2/functions/defer/homework/solution"
)

func main() {
	jobs := []solution.MergeDictsJob{
		{},
		{Dicts: []map[string]string{{"a": "b"}, nil}},
		{Dicts: []map[string]string{{"a": "b"}, {"b": "c"}}},
	}

	for i, job := range jobs {
		fmt.Println("Job ", i+1)
		fmt.Printf("Input: Dicts: %#v\n", job.Dicts)
		result, err := solution.ExecuteMergeDictsJob(&job)
		fmt.Printf("Result: IsFinished: %#v, Merged: %#v, %v\n", result.IsFinished, result.Merged, err)
	}
}
