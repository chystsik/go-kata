package solution

import (
	"errors"
)

// MergeDictsJob is a job to merge dictionaries into a single dictionary.
type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

// BEGIN (write your solution here)

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	var err error
	job.Merged = make(map[string]string, len(job.Dicts))

	defer func() {
		job.IsFinished = true
	}()

	var numOfDicts int

	for _, v := range job.Dicts {
		if v == nil {
			err = errNilDict
		}
		for i, j := range v {
			job.Merged[i] = j
		}
		numOfDicts++
	}

	if numOfDicts < 2 {
		err = errNotEnoughDicts
	}

	return job, err
}

// END

// Пример работы
// ExecuteMergeDictsJob(&MergeDictsJob{}) // &MergeDictsJob{IsFinished: true}, "at least 2 dictionaries are required"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},nil}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b"},nil}}, "nil dictionary"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},{"b": "c"}}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b", "b": "c"}}}, nil
