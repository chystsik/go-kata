package main

import (
	"fmt"
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCalc(t *testing.T) {

	var (
		casesPerOperaion int = 8

		testCalc = NewCalc()

		expected = map[string]func(a, b float64) float64{
			"multiply": func(a, b float64) float64 { return a * b },
			"divide":   func(a, b float64) float64 { return a / b },
			"sum":      func(a, b float64) float64 { return a + b },
			"average":  func(a, b float64) float64 { return (a + b) / 2 },
		}

		actual = map[string]func(a, b float64) float64{
			"multiply": multiply,
			"divide":   divide,
			"sum":      sum,
			"average":  average,
		}
	)

	type args struct {
		a, b float64
	}

	type testCase struct {
		opName string
		opCase int
		args   args
		want   float64
	}

	tests := make([]testCase, casesPerOperaion*len(expected))
	opShift := 0

	for op, res := range expected {
		for i := opShift; i < opShift+casesPerOperaion; i++ {
			tests[i].opName = op
			tests[i].opCase = (i - opShift) + 1
			tests[i].args = args{a: float64(rand.Intn(100) + 1), b: float64(rand.Intn(100) + 1)}
			tests[i].want = res(tests[i].args.a, tests[i].args.b)
			fmt.Printf("%s(case %d): (%f, %f) = %f\n", tests[i].opName, tests[i].opCase, tests[i].args.a, tests[i].args.b, tests[i].want)
		}
		opShift += casesPerOperaion
	}

	fmt.Println(tests)

	for _, test := range tests {
		t.Run(test.opName+fmt.Sprintf("_case_%d", test.opCase), func(t *testing.T) {
			get := testCalc.SetA(test.args.a).SetB(test.args.b).Do(actual[test.opName]).Result()
			assert.Equalf(t, test.want, get, "error operation \"%s\" with operands (%f, %f). Expected: %f. Actual: %f", test.opName, test.args.a, test.args.b, test.want, get)
		})
	}
}
