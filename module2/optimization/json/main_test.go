package main

import (
	"io"
	"os"
	"testing"
)

/*
https://petstore.swagger.io/#/pet/findPetsByStatus
https://app.quicktype.io/

goos: linux
goarch: amd64
pkg: gitlab.com/chystsik/go-kata/module2/optimization/json
cpu: Intel(R) Core(TM) i5-10300H CPU @ 2.50GHz
Benchmark_StandartJson-8           13609            250834 ns/op           67040 B/op        408 allocs/op
Benchmark_Jsoniter-8               35430            111140 ns/op           62427 B/op        451 allocs/op
PASS*/

var jsonData = getJsonData()

func Benchmark_StandartJson(b *testing.B) {
	var (
		pets Pets
		err  error
		data []byte
	)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets(jsonData)
		if err != nil {
			panic(err)
		}

		data, err = pets.Marshal()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}

func Benchmark_Jsoniter(b *testing.B) {
	var (
		pets Pets
		err  error
		data []byte
	)

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets2(jsonData)
		if err != nil {
			panic(err)
		}

		data, err = pets.Marshal2()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}

func getJsonData() []byte {
	f, err := os.Open(string("pets.json"))

	if err != nil {
		panic(err)
	}
	defer f.Close()

	dat, err := io.ReadAll(f)
	if err != nil {
		panic(err)
	}

	return dat
}
