package main

import "testing"

/*
goos: windows
goarch: amd64
pkg: gitlab.com/chystsik/go-kata/module2/optimization/regexp
cpu: Intel(R) Core(TM) i5-10300H CPU @ 2.50GHz
Benchmark_SanitizeText-8    	    1002	   1204442 ns/op	  340376 B/op	    1949 allocs/op
Benchmark_SanitizeText2-8   	    1477	    785732 ns/op	  164192 B/op	     734 allocs/op
PASS */

func Benchmark_SanitizeText(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for i := range data {
			SanitizeText(data[i])
		}
	}
}

func Benchmark_SanitizeText2(b *testing.B) {
	ft := NewFilterText()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		for i := range data {
			ft.SanitizeText2(data[i])
		}
	}
}
