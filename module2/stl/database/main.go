package main

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	db, err := sql.Open("sqlite3", "./test.db")
	check(err)
	defer db.Close()

	statement, err := db.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, firstname TEXT, lastname TEXT)")
	check(err)
	_, err = statement.Exec()
	check(err)

	statement, err = db.Prepare("INSERT INTO people (firstname, lastname) VALUES (?,?)")
	check(err)
	_, err = statement.Exec("Lorem", "Ipsum")
	check(err)

	rows, err := db.Query("SELECT id, firstname, lastname FROM people")
	check(err)
	var id int
	var firstname, lastname string

	for rows.Next() {
		err := rows.Scan(&id, &firstname, &lastname)
		check(err)
		fmt.Printf("%d: %s, %s\n", id, firstname, lastname)
	}
}
