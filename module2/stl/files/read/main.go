package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	cont := "Hello go"

	tmp, err := os.CreateTemp("", "tmp_*")
	check(err)
	_, err = tmp.Write([]byte(cont))
	check(err)
	tmp1, err := os.Open(tmp.Name())
	check(err)
	dat, err := io.ReadAll(tmp1)
	check(err)
	tmp.Close()

	fmt.Printf("Read %d bytes from file \"%s\":\n%s\n", len(dat), tmp.Name(), string(dat))

	f, err := os.Open(tmp.Name())
	check(err)
	defer f.Close()

	b1 := make([]byte, 5)
	n1, err := f.Read(b1)
	check(err)
	fmt.Printf("%d bytes: %s\n", n1, string(b1[:n1]))

	o2, err := f.Seek(6, 0)
	check(err)
	b2 := make([]byte, 2)
	n2, err := f.Read(b2)
	check(err)
	fmt.Printf("%d bytes @ %d: %s\n", n2, o2, string(b2))

	o3, err := f.Seek(6, 0)
	check(err)
	b3 := make([]byte, 2)
	n3, err := io.ReadAtLeast(f, b3, 2)
	check(err)
	fmt.Printf("%d bytes @ %d: %s\n", n3, o3, string(b3))

	_, err = f.Seek(0, 0)
	check(err)

	r4 := bufio.NewReader(f)
	b4, err := r4.Peek(5)
	check(err)
	fmt.Printf("%d bytes: %s\n", 5, string(b4))
}
