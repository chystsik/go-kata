package main

import (
	"bufio"
	"fmt"
	"os"

	"github.com/essentialkaos/translit"
)

const (
	fileNameEncoded  = "example.processed.txt"
	fileNameCyryllic = "example.txt"
	str              = "В белом плаще с кровавым подбоем, шаркающей кавалерийской походкой, ранним утром четырнадцатого числа весеннего месяца нисана в крытую колоннаду между двумя крыльями дворца Ирода Великого вышел прокуратор Иудеи Понтий Пилат."
)

type file struct {
	name, path string
	content    []byte
}

func (f file) write() error {
	f.path = fmt.Sprintf("/tmp/%s", f.name)
	file, err := os.Create(f.path)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.Write(f.content)
	if err != nil {
		return err
	}

	return nil
}

func (f file) read() ([]byte, error) {
	f.path = fmt.Sprintf("/tmp/%s", f.name)
	file, err := os.Open(f.path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	b := make([]byte, len(f.content))
	_, err = file.Read(b)
	if err != nil {
		return nil, err
	}

	return b, nil
}

func (f file) translit() error {
	file, err := f.read()
	if err != nil {
		return err
	}

	tr := translit.EncodeToICAO(string(file))
	f.content = []byte(tr)
	err = f.write()
	if err != nil {
		return err
	}

	return nil
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter your name: ")

	name, _ := reader.ReadString('\n')
	fmt.Printf("Hello %s\n", name)

	f := file{content: []byte(name), name: "stdin"}
	err := f.write() // записываем данные из stdin в файл
	check(err)

	r, err := f.read()
	check(err)
	fmt.Println(string(r))

	// записываем файл с текстом для транслита
	f = file{content: []byte(str), name: fileNameCyryllic}
	err = f.write()
	check(err)

	// читаем записанный файл
	r, err = f.read()
	check(err)
	fmt.Println(string(r))

	// записываем трансилт в файл с именем example.processed.txt
	f.name = fileNameEncoded
	err = f.translit()
	check(err)

	// читаем транслит из файла example.processed.txt
	r, err = f.read()
	check(err)
	fmt.Println(string(r))
}
