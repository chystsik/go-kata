package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"os"
)

type Config struct {
	AppName    string `json:"AppName"`
	Production bool   `json:"Production"`
	FilePath   string `json:"-"`
}

func (c *Config) parceArgs() *Config {
	flag.StringVar(&c.FilePath, "config", "", "Config file path")
	flag.Parse()

	if c.FilePath == "" {
		fmt.Println("-config flag has to be specified, use -h for help")
		os.Exit(1)
	}

	return c
}

func (c *Config) readConfig(writer io.Writer) error {
	f, err := os.Open(string(c.FilePath))

	if err != nil {
		return err
	}
	defer f.Close()

	err = json.NewDecoder(f).Decode(&c)
	if err != nil {
		return err
	}

	_, err = writer.Write([]byte(fmt.Sprintf("Production: %t\nAppName: %s\n", c.Production, c.AppName)))
	if err != nil {
		return err
	}

	return nil
}

func main() {
	c := &Config{}

	if err := c.parceArgs().readConfig(os.Stdout); err != nil {
		panic(err)
	}
}

// Пример вывода конфига
// Production: true
// AppName: мое тестовое приложение
