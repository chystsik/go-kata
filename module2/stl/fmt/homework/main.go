package main

import (
	"fmt"
	"log"

	"github.com/brianvoe/gofakeit/v6"
)

const (
	personCount = 100
	greeting    = "Hello! My name is %s. I'm %d y.o. And I also have $%.2f in my wallet right now."
)

type Person struct {
	Name  string  `fake:"{firstname}"`
	Age   int     `fake:"{number:18, 99}"`
	Money float64 `fake:"{float64range:10.00, 100.00}"`
}

func generateSelfStory(name string, age int, money float64) string {
	return fmt.Sprintf(greeting, name, age, money)
}

func main() {
	p := Person{
		Name:  "Andy",
		Age:   18,
		Money: 10.00,
	}

	// вывод значений структуры
	fmt.Println("simple struct:", p)

	// вывод названий полей и их значений
	fmt.Printf("detailed struct: %+v\n", p)

	// вывод названий полей и их значений в виде инициализации
	fmt.Printf("Golang struct: %#v\n", p)

	fmt.Println(generateSelfStory(p.Name, p.Age, p.Money))

	person := make([]Person, personCount)
	//var err error

	for i, p := range person {
		if err := gofakeit.Struct(&p); err != nil {
			log.Fatalln(err)
		}
		person[i] = p
	}

	for _, p := range person {
		fmt.Println(generateSelfStory(p.Name, p.Age, p.Money))
	}
}
