// You can edit this code!
// Click here and start typing.
package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
)

func check(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	// здесь расположите буфер
	b := new(bytes.Buffer)
	// запишите данные в буфер
	for i, line := range data {
		if i != len(data)-1 {
			line = fmt.Sprintf("%s\n", line)
		}
		b.WriteString(line)
	}
	// создайте файл
	f, err := os.CreateTemp("", "example.txt")
	check(err)
	defer f.Close()
	// запишите данные в файл
	_, err = io.Copy(f, b)
	check(err)
	// прочтите данные в новый буфе
	b1 := new(bytes.Buffer)
	f1, err := os.Open(f.Name())
	check(err)

	_, err = io.Copy(b1, f1)
	check(err)

	fmt.Println(b1.String()) // выведите данные из буфера buffer.String()
	// у вас все получится!
}
