package main

import (
	"fmt"
	"reflect"
	"time"
)

var reflectedStructs map[string][]Field
var tagKeys = []string{"json", "db", "db_type", "db_default", "db_ops", "db_index"}

type Field struct {
	Name string
	Tags map[string]string
}

type UserDTO struct {
	ID            int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Name          string    `json:"name" db:"name" db_type:"varchar(55)" db_default:"default null" db_ops:"create,update"`
	Phone         string    `json:"phone" db:"phone" db_type:"varchar(34)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Email         string    `json:"email" db:"email" db_type:"varchar(89)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Password      string    `json:"password" db:"password" db_type:"varchar(144)" db_default:"default null" db_ops:"create,update"`
	Status        int       `json:"status" db:"status" db_type:"int" db_default:"default 0" db_ops:"create,update"`
	Role          int       `json:"role" db:"role" db_type:"int" db_default:"not null" db_ops:"create,update"`
	Verified      bool      `json:"verified" db:"verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	EmailVerified bool      `json:"email_verified" db:"email_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	PhoneVerified bool      `json:"phone_verified" db:"phone_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	CreatedAt     time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt     time.Time `json:"updated_at" db:"updated_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt     time.Time `json:"deleted_at" db:"deleted_at" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

type EmailVerifyDTO struct {
	ID        int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Email     string    `json:"email" db:"email" db_type:"varchar(89)" db_default:"not null" db_index:"index,unique" db_ops:"create,update"`
	UserID    int       `json:"user_id,omitempty" db:"user_id" db_ops:"create" db_type:"int" db_default:"not null" db_index:"index"`
	Hash      string    `json:"hash,omitempty" db:"hash" db_ops:"create" db_type:"char(36)" db_default:"not null" db_index:"index"`
	Verified  bool      `json:"verified" db:"verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	CreatedAt time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
}

func reflectStructs(structure []interface{}, result map[string][]Field) {
	for _, str := range structure {
		s := reflect.ValueOf(str)

		for i := 0; i < s.NumField(); i++ {
			tags := make(map[string]string, len(tagKeys))

			for _, tagKey := range tagKeys {
				tagVal, ok := s.Type().Field(i).Tag.Lookup(tagKey)
				if ok {
					tags[tagKey] = tagVal
				}
			}
			result[s.Type().Name()] = append(result[s.Type().Name()], Field{Name: s.Type().Field(i).Name, Tags: tags})
		}
	}
}

func main() {
	structs := []interface{}{
		UserDTO{},
		EmailVerifyDTO{},
	}

	// заполни данными структур согласно заданиям
	reflectedStructs = make(map[string][]Field, len(structs))
	reflectStructs(structs, reflectedStructs)
	fmt.Println(reflectedStructs)
	for k, v := range reflectedStructs {
		fmt.Printf("\n%s", k)
		for _, f := range v {
			fmt.Printf("\n\t%-20s", f.Name)
			for fk, fv := range f.Tags {
				fmt.Printf(" %s: \"%s\";", fk, fv)
			}
		}
	}
	fmt.Printf("\n")
}
