package main

import (
	"fmt"
	"strings"
)

type Cache struct {
	data map[string]*User
}

func NewCache() *Cache {
	return &Cache{data: make(map[string]*User, 100)}
}

func (c *Cache) Set(key string, u *User) {
	c.data[key] = u
}

func (c *Cache) Get(key string) *User {
	return c.data[key]
}

type User struct {
	ID       int
	Nickname string
	Email    string
}

func main() {
	var users []*User
	emails := []string{"robpike@gmail.com", "davecheney@gmail.com", "bradfitzpatrick@email.ru", "eliben@gmail.com", "quasilyte@mail.ru"}
	for i := range emails {
		users = append(users, &User{
			ID:    i + 1,
			Email: emails[i],
		})
	}
	cache := NewCache()
	for i := range users {
		parts := strings.Split(emails[i], "@")
		users[i].Nickname = parts[0]
		parts[1] = fmt.Sprintf("%d", users[i].ID)
		// Положить пользователей в кэш с ключом Nickname:userid
		// ...
		cache.Set(strings.Join(parts, ":"), users[i])
	}
	keys := []string{"robpike:1", "davecheney:2", "bradfitzpatrick:3", "eliben:4", "quasilyte:5"}
	for i := range keys {
		fmt.Println(cache.Get(keys[i]))
	}
}
