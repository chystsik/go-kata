// https://go.dev/blog/slices
package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	s1 := []int{1, 2, 3}

	Append(&s)
	fmt.Println(s)

	s1 = Append1(s1)
	fmt.Println(s1)
}

func Append(s *[]int) {
	*s = append(*s, 4)
}

func Append1(s []int) []int {
	s = append(s, 4)
	return s
}
