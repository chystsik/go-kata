// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
)

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 28600,
		},
		{
			Name:  "https://github.com/golang/go",
			Stars: 108000,
		},
		{
			Name:  "https://github.com/caddyserver/caddy",
			Stars: 45800,
		},
		{
			Name:  "https://github.com/etcd-io/etcd",
			Stars: 42500,
		},
		{
			Name:  "https://github.com/gogs/gogs",
			Stars: 41600,
		},
		{
			Name:  "https://github.com/avelino/awesome-go",
			Stars: 96300,
		},
		{
			Name:  "https://github.com/kubernetes/kubernetes",
			Stars: 95800,
		},
		{
			Name:  "https://github.com/gin-gonic/gin",
			Stars: 66400,
		},
		{
			Name:  "https://github.com/gohugoio/hugo",
			Stars: 65300,
		},
		{
			Name:  "https://github.com/moby/moby",
			Stars: 65100,
		},
		{
			Name:  "https://github.com/fatedier/frp",
			Stars: 64200,
		},
		{
			Name:  "https://github.com/prometheus/prometheus",
			Stars: 46700,
		},
	}

	projectsMap := make(map[string]int, len(projects))

	// в цикле запишите в map
	for _, project := range projects {
		projectsMap[project.Name] = project.Stars
	}

	// в цикле пройдитесь по мапе и выведите значения в консоль
	for name, stars := range projectsMap {
		fmt.Println(stars, "\t", name)
	}
}
