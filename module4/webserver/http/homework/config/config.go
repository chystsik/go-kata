package config

import (
	"fmt"

	"github.com/spf13/viper"
)

type (
	AppConfig struct {
		Http    Http
		Service Service
	}

	Http struct {
		Host string
		Port int
	}

	Service struct {
		PublicFolder string
		Loglevel     uint32
	}
)

const (
	errDecode = "unable to decode app config into struct, %v"
)

func New() (*AppConfig, error) {
	var cfg *AppConfig
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./config")
	viper.SetConfigName("config.yaml")
	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}
	if err := viper.Unmarshal(&cfg); err != nil {
		return nil, fmt.Errorf(errDecode, err)
	}
	return cfg, nil
}
