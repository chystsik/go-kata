package adapters

import (
	"encoding/json"
	"net/http"

	handlers "gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/adapters/rest_api_handlers"
	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/service"

	"github.com/sirupsen/logrus"
)

type serviceHandlers struct {
	handlers.UserHandlers
	handlers.FileHandlers
	logger logrus.FieldLogger
}

type ServiceHandlers interface {
	Greeting(w http.ResponseWriter, r *http.Request)
	handlers.UserHandlers
	handlers.FileHandlers
}

func NewHandlers(userService service.UserService, fileService service.FileService, logger logrus.FieldLogger) ServiceHandlers {
	return &serviceHandlers{
		handlers.NewUserHandlers(userService, logger),
		handlers.NewFileHandlers(fileService, logger),
		logger,
	}
}

func (sh *serviceHandlers) Greeting(w http.ResponseWriter, r *http.Request) {
	var greeting = struct {
		Status  string `json:"status"`
		Message string `json:"message"`
		Version string `json:"version"`
	}{
		Status:  "Active",
		Message: "Web server up and running",
		Version: "1.0.0",
	}

	out, err := json.Marshal(greeting)
	if err != nil {
		sh.logger.Errorln(err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(out)
	if err != nil {
		sh.logger.Errorln(err)
	}
}
