package handlers

import (
	"bytes"
	"net/http"

	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/service"

	"github.com/go-chi/chi/v5"
	"github.com/sirupsen/logrus"
)

type FileHandlers interface {
	UploadFile(w http.ResponseWriter, r *http.Request)
	DownloadFile(w http.ResponseWriter, r *http.Request)
}

type fileHandlers struct {
	fileService service.FileService
	logger      logrus.FieldLogger
}

func NewFileHandlers(fs service.FileService, logger logrus.FieldLogger) FileHandlers {
	return &fileHandlers{
		fileService: fs,
		logger:      logger,
	}
}

func (fh *fileHandlers) UploadFile(w http.ResponseWriter, r *http.Request) {
	file, fileHeader, err := r.FormFile("file")
	if err != nil {
		errorJSON(w, err, fh.logger, http.StatusBadRequest)
		return
	}
	defer file.Close()

	err = fh.fileService.Create(fileHeader.Filename, file)
	if err != nil {
		errorJSON(w, err, fh.logger, http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
}

func (fh *fileHandlers) DownloadFile(w http.ResponseWriter, r *http.Request) {
	fileName := chi.URLParam(r, "name")
	reader, err := fh.fileService.Get(fileName)
	if err != nil {
		errorJSON(w, err, fh.logger, http.StatusBadRequest)
		return
	}

	buf := new(bytes.Buffer)
	_, err = buf.ReadFrom(reader)
	if err != nil {
		errorJSON(w, err, fh.logger, http.StatusBadRequest)
		return
	}

	fileContentType := http.DetectContentType(buf.Bytes())

	w.Header().Set("Content-Type", fileContentType)
	w.WriteHeader(http.StatusOK)

	_, err = w.Write(buf.Bytes())
	if err != nil {
		errorJSON(w, err, fh.logger, http.StatusInternalServerError)
		return
	}

	err = reader.Close()
	if err != nil {
		fh.logger.Errorln(err)
	}
}
