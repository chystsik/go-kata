package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
)

type JSONResponse struct {
	Error   bool        `json:"error"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

func writeJSON(w http.ResponseWriter, status int, contentType string, data interface{}, logger logrus.FieldLogger, headers ...http.Header) {
	out, err := json.Marshal(data)
	if err != nil {
		logger.Errorln(err)
		return
	}

	if len(headers) > 0 {
		for k, v := range headers[0] {
			w.Header()[k] = v
		}
	}

	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(status)
	_, err = w.Write(out)
	if err != nil {
		logger.Errorln(err)
		return
	}
}

func errorJSON(w http.ResponseWriter, err error, logger logrus.FieldLogger, status ...int) {
	statusCode := http.StatusBadRequest

	if len(status) > 0 {
		statusCode = status[0]
	}

	var payload JSONResponse
	payload.Error = true
	payload.Message = err.Error()

	writeJSON(w, statusCode, "application/json", payload, logger)
}
