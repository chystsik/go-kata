package handlers

import (
	"net/http"
	"strconv"

	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/service"

	"github.com/go-chi/chi/v5"
	"github.com/sirupsen/logrus"
)

type UserHandlers interface {
	GetUsers(w http.ResponseWriter, r *http.Request)
	GetUser(w http.ResponseWriter, r *http.Request)
}

type userHandlers struct {
	userService service.UserService
	logger      logrus.FieldLogger
}

func NewUserHandlers(userService service.UserService, logger logrus.FieldLogger) UserHandlers {
	return &userHandlers{
		userService: userService,
		logger:      logger,
	}
}

func (uh *userHandlers) GetUsers(w http.ResponseWriter, r *http.Request) {
	users, err := uh.userService.GetUsers()
	if err != nil {
		errorJSON(w, err, uh.logger, http.StatusInternalServerError)
		return
	}

	writeJSON(w, http.StatusOK, "application/json", users, uh.logger)
}

func (uh *userHandlers) GetUser(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		errorJSON(w, err, uh.logger, http.StatusBadRequest)
		return
	}

	user, err := uh.userService.GetUser(id)
	if err != nil {
		errorJSON(w, err, uh.logger, http.StatusBadRequest)
		return
	}

	writeJSON(w, http.StatusOK, "application/json", user, uh.logger)
}
