package app

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/config"
	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/adapters"
	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/infrastructure/repository"
	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/service"
	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/transport/restapi"

	"github.com/sirupsen/logrus"
)

const (
	errHttpListen   = "Failed to listen and serve (http): %v"
	errHttpShutdown = "HTTP server shutdown error: %v"

	logHttpStop        = "HTTP Server: stopped serving new connections..."
	logHttpShutdown    = "Graceful shutdown of HTTP Server complete."
	logSignalInterrupt = "Interrupt signal. Shutdown"
)

func Run(cfg *config.AppConfig) {
	// Initialising logger
	logger := logrus.New()
	logger.SetFormatter(&logrus.TextFormatter{
		ForceColors:     true,
		FullTimestamp:   true,
		TimestampFormat: time.RFC3339,
	})
	level := logrus.Level(cfg.Service.Loglevel)
	logger.SetLevel(level)

	// Initialising repository, service and handlers
	userRepository := repository.NewUserRepository(10)
	fileRepository := repository.NewFileRepository(cfg.Service.PublicFolder)
	userService := service.NewUserService(userRepository)
	fileService := service.NewFileService(fileRepository)
	handlers := adapters.NewHandlers(userService, fileService, logger)

	// Startup http server with handlers
	server := restapi.NewServer(cfg.Http, handlers, logger)
	go func() {
		if err := server.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			logger.Fatalf(errHttpListen, err)
		}
		logger.Infof(logHttpStop)
	}()

	// Graceful shutdown setup
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	<-quit
	logger.Infoln(logSignalInterrupt)
	ctxShutdown, shutdown := context.WithTimeout(context.Background(), 5*time.Second)
	defer shutdown()

	// Graceful shutdown HTTP Server
	if err := server.Shutdown(ctxShutdown); err != nil {
		logger.Fatalf(errHttpShutdown, err)
	}
	logger.Infoln(logHttpShutdown)
}
