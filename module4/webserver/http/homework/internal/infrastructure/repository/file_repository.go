package repository

import (
	"fmt"
	"io"
	"os"

	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/model"
	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/service"
)

type fileRepository struct {
	folderPath string
}

func NewFileRepository(folderPath string) service.FileRepository {
	return &fileRepository{folderPath: folderPath}
}

func (fr *fileRepository) Read(name string) (model.File, error) {
	filePath := fmt.Sprintf("%s/%s", fr.folderPath, name)
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	return file, nil
}
func (fr *fileRepository) Write(name string, file model.File) error {
	filePath := fmt.Sprintf("%s/%s", fr.folderPath, name)
	newFile, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer newFile.Close()

	_, err = io.Copy(newFile, file)
	if err != nil {
		return err
	}

	return nil
}
