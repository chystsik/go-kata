package repository

import (
	"fmt"

	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/model"
	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/service"

	"github.com/brianvoe/gofakeit/v6"
)

type userRepository struct {
	repo []model.User
}

func NewUserRepository(fakeUsersCount int) service.UserRepository {
	return &userRepository{repo: getFakeUsers(fakeUsersCount)}
}

func (ur *userRepository) GetList() ([]model.User, error) {
	return ur.repo, nil
}

func (ur *userRepository) GetOne(id int) (model.User, error) {
	for i := range ur.repo {
		if ur.repo[i].ID == id {
			return ur.repo[i], nil
		}
	}
	return model.User{}, fmt.Errorf("cant find user with id %d in user repository", id)
}

func getFakeUsers(count int) []model.User {
	faker := gofakeit.New(0)
	users := make([]model.User, count)

	for i := range users {
		users[i].ID = i + 1
		err := faker.Struct(&users[i])
		if err != nil {
			panic(err)
		}
	}

	return users
}
