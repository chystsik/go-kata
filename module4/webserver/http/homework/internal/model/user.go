package model

type User struct {
	ID        int    `json:"id" fake:"skip"`
	FirstName string `json:"first_name" fake:"{firstname}"`
	LastName  string `json:"last_name"  fake:"{lastname}"`
	Email     string `json:"email"  fake:"{email}"`
}
