package service

import (
	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/model"
)

type FileRepository interface {
	Read(fileName string) (model.File, error)
	Write(fileName string, file model.File) error
}

type FileService interface {
	Get(fileName string) (model.File, error)
	Create(fileName string, file model.File) error
}
