package service

import (
	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/model"
)

type fileService struct {
	fileRepo FileRepository
}

func NewFileService(fr FileRepository) FileService {
	return &fileService{fileRepo: fr}
}

func (fs *fileService) Get(name string) (model.File, error) {
	reader, err := fs.fileRepo.Read(name)
	if err != nil {
		return nil, err
	}

	return reader, nil
}
func (fs *fileService) Create(name string, file model.File) error {
	err := fs.fileRepo.Write(name, file)
	if err != nil {
		return err
	}

	return nil
}
