package service

import "gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/model"

type UserRepository interface {
	GetList() ([]model.User, error)
	GetOne(id int) (model.User, error)
}

type UserService interface {
	GetUsers() ([]model.User, error)
	GetUser(id int) (model.User, error)
}
