package service

import "gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/model"

type userService struct {
	userRepo UserRepository
}

func NewUserService(ur UserRepository) UserService {
	return &userService{userRepo: ur}
}

func (us *userService) GetUsers() ([]model.User, error) {
	users, err := us.userRepo.GetList()
	if err != nil {
		return nil, err
	}

	return users, nil
}

func (us *userService) GetUser(id int) (model.User, error) {
	user, err := us.userRepo.GetOne(id)
	if err != nil {
		return model.User{}, err
	}

	return user, nil
}
