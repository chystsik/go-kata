package restapi

import (
	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/adapters"

	"github.com/go-chi/chi/v5"
)

func RegisterHandlers(router *chi.Mux, handlers adapters.ServiceHandlers) {
	router.Get("/", handlers.Greeting)
	router.Get("/users", handlers.GetUsers)
	router.Get("/user/{id}", handlers.GetUser)
	router.Post("/upload", handlers.UploadFile)
	router.Get("/public/{name}", handlers.DownloadFile)
}
