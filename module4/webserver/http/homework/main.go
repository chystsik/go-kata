package main

import (
	"log"

	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/config"
	"gitlab.com/chystsik/go-kata/module4/webserver/http/homework/internal/app"
)

func main() {
	cfg, err := config.New()
	if err != nil {
		log.Fatal(err)
	}

	app.Run(cfg)
}
