package adapters

import (
	"html/template"
	"net/http"
	"time"

	handlers "gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/adapters/rest_api_handlers"
	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/service"
)

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
	<style>
		body {
			margin: 0;
		}
	</style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/public/swagger.json?{{.Time}}",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

type ServiceHandlers interface {
	handlers.PetHandlers
	handlers.StoreHandlers
	handlers.UserHandlers
	SwaggerUI(w http.ResponseWriter, r *http.Request)
}

type serviceHandlers struct {
	handlers.PetHandlers
	handlers.StoreHandlers
	handlers.UserHandlers
}

func NewServiceHandlers(ps service.PetService, ss service.StoreService, us service.UserService) ServiceHandlers {
	return &serviceHandlers{
		handlers.NewPetHandlers(ps),
		handlers.NewStoreHandlers(ss),
		handlers.NewUserHandlers(us),
	}
}

func (sh *serviceHandlers) SwaggerUI(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		return
	}
}
