package handlers

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/models"
	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/service"

	"github.com/go-chi/chi"
)

const secretApiKey = "super_secret_key"

type PetHandlers interface {
	PetCreate(w http.ResponseWriter, r *http.Request)
	PetGetByID(w http.ResponseWriter, r *http.Request)
	PetUploadImage(w http.ResponseWriter, r *http.Request)
	PetUpdate(w http.ResponseWriter, r *http.Request)
	PetGetByStatus(w http.ResponseWriter, r *http.Request)
	PetUpdateWithForm(w http.ResponseWriter, r *http.Request)
	PetDelete(w http.ResponseWriter, r *http.Request)
}

type petHandlers struct {
	petService service.PetService
}

func NewPetHandlers(ps service.PetService) PetHandlers {
	return &petHandlers{petService: ps}
}

func (p *petHandlers) PetCreate(w http.ResponseWriter, r *http.Request) {
	var (
		pet models.Pet
		err error
	)

	err = json.NewDecoder(r.Body).Decode(&pet) // считываем приходящий json из *http.Request в структуру models.Pet
	if err != nil {                            // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet = p.petService.CreatePet(pet) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)                             // записываем результат models.Pet json в http.ResponseWriter
	if err != nil {                                                  // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *petHandlers) PetGetByID(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		pet      models.Pet
		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petId") // получаем petID из chi router

	petID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                     // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet, err = p.petService.GetPetByID(petID) // пытаемся получить models.Pet по id
	if err != nil {                           // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *petHandlers) PetUploadImage(w http.ResponseWriter, r *http.Request) {
	type photoUrls struct { // структура для парсинга xml файла. <photoUrls><url>url1</url>...</photoUrls>
		XMLName xml.Name `xml:"photoUrls"`
		Url     []string `xml:"url"`
	}

	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		pet      models.Pet
		err      error
		petIDRaw string
		urls     photoUrls
	)

	petIDRaw = chi.URLParam(r, "petId") // получаем petID из chi router

	pet.ID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                      // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	file, fileHeader, err := r.FormFile("file") // получаем файл из запроса
	if err != nil {                             // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	_ = fileHeader

	err = xml.NewDecoder(file).Decode(&urls) // считываем xml file из *http.Request в структуру models.Pet
	if err != nil {                          // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet, err = p.petService.GetPetByID(pet.ID) // получаем models.Pet из PetStorage
	if err != nil {                            // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet.PhotoUrls = urls.Url // обновляем поле PhotoUrls

	pet, err = p.petService.UpdatePet(pet) // обновляем только models.Pet.PhotoUrls для нужного models.Pet.ID в нашем storage
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)                             // записываем результат models.Pet json в http.ResponseWriter
	if err != nil {                                                  // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *petHandlers) PetUpdate(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		pet models.Pet
		err error
	)

	err = json.NewDecoder(r.Body).Decode(&pet) // считываем приходящий json из *http.Request в структуру models.Pet
	if err != nil {                            // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	petId := pet.ID

	pet, err = p.petService.UpdatePet(pet) // обновляем запись в нашем storage
	if err != nil {
		if err.Error() == fmt.Sprintf("pet with id %d not found", petId) {
			http.Error(w, err.Error(), http.StatusNotFound)
			return // не забываем прекратить обработку нашего handler (ручки)
		}
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)                             // записываем результат models.Pet json в http.ResponseWriter
	if err != nil {                                                  // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *petHandlers) PetGetByStatus(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		pets []models.Pet
		err  error
	)

	status := r.URL.Query()["status"] // считываем приходящий статус из *http.Request

	if len(status) == 0 || len(status) > 3 { // в отсутсвии статуса отправляем Bad request code 400
		http.Error(w, "invalid status value", http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pets = p.petService.GetPetListByStatus(status) // получаем []models.Pet с искомым статусом

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pets)                            // записываем результат models.Pet json в http.ResponseWriter
	if err != nil {                                                  // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *petHandlers) PetUpdateWithForm(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		pet      models.Pet
		err      error
		petIDRaw string
	)

	petIDRaw = chi.URLParam(r, "petId") // получаем petID из chi router

	pet.ID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                      // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	err = r.ParseForm() // прсим данные form
	if err != nil {     // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet, err = p.petService.GetPetByID(pet.ID) // получаем models.Pet из PetStorage
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	err = r.ParseMultipartForm(32 << 20)
	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	// считываем значения из formData *http.Request в структуру models.Pet
	// обновляем только нужные поля
	pet.Name = r.Form.Get("name")
	pet.Status = r.Form.Get("status")

	pet, err = p.petService.UpdatePet(pet) // обновляем запись в нашем storage
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)                             // записываем результат models.Pet json в http.ResponseWriter
	if err != nil {                                                  // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *petHandlers) PetDelete(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		err      error
		petIDRaw string
		petID    int
		apiKey   string
	)

	apiKey = r.Header.Get("api_key") // парсим api_key из header value
	if apiKey != secretApiKey {      // проверяем ключ
		http.Error(w, "wrong api key", http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	petIDRaw = chi.URLParam(r, "petId") // получаем petID из chi router

	petID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                     // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	err = p.petService.DeletePet(petID) // удаляем запись из storage
	if err != nil {
		if err.Error() == fmt.Sprintf("pet with id %d not found", petID) {
			http.Error(w, err.Error(), http.StatusNotFound) // отправляем ошибку NotFound, если не нашли models.Pet
			return                                          // не забываем прекратить обработку нашего handler (ручки)
		}
		http.Error(w, err.Error(), http.StatusInternalServerError) // отправляем ServerError во всех остальных случаях
		return                                                     // не забываем прекратить обработку нашего handler (ручки)
	}

	w.WriteHeader(http.StatusNoContent) // уведомляем что сервер успешно обработал запрос
}
