package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/models"
	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/service"

	"github.com/go-chi/chi"
)

type StoreHandlers interface {
	OrderCreate(w http.ResponseWriter, r *http.Request)
	OrderGetById(w http.ResponseWriter, r *http.Request)
	OrderDelete(w http.ResponseWriter, r *http.Request)
	OrderInventory(w http.ResponseWriter, r *http.Request)
}

type storeHandlers struct {
	storeService service.StoreService
}

func NewStoreHandlers(ss service.StoreService) StoreHandlers {
	return &storeHandlers{storeService: ss}
}

func (p *storeHandlers) OrderCreate(w http.ResponseWriter, r *http.Request) {
	var (
		order models.Order
		err   error
	)

	err = json.NewDecoder(r.Body).Decode(&order) // считываем приходящий json из *http.Request в структуру models.Order
	if err != nil {                              // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	order = p.storeService.CreateOrder(order) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(order)                           // записываем результат models.Order json в http.ResponseWriter
	if err != nil {                                                  // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *storeHandlers) OrderGetById(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		order      models.Order
		err        error
		orderIDRaw string
		orderID    int
	)

	orderIDRaw = chi.URLParam(r, "orderId") // получаем orderID из chi router

	orderID, err = strconv.Atoi(orderIDRaw) // конвертируем в int
	if err != nil {                         // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	order, err = p.storeService.GetOrderByID(orderID) // пытаемся получить models.Order по id
	if err != nil {                                   // в случае ошибки отправляем Not Found код 404
		if err.Error() == fmt.Sprintf("order with id %d not found", orderID) {
			http.Error(w, err.Error(), http.StatusNotFound)
			return // не забываем прекратить обработку нашего handler (ручки)
		}
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(order)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *storeHandlers) OrderDelete(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		order      models.Order
		err        error
		orderIDRaw string
	)

	orderIDRaw = chi.URLParam(r, "orderId") // получаем orderID из chi router

	order.ID, err = strconv.Atoi(orderIDRaw) // конвертируем в int
	if err != nil {                          // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	err = p.storeService.DeleteOrderByID(order.ID) // удаляем запись из storage
	if err != nil {
		if err.Error() == fmt.Sprintf("order with id %d not found", order.ID) { // отправляем ошибку NotFound, если не нашли models.Order
			http.Error(w, err.Error(), http.StatusNotFound)
			return // не забываем прекратить обработку нашего handler (ручки)
		}
		http.Error(w, err.Error(), http.StatusInternalServerError) // отправляем ServerError во всех остальных случаях
		return                                                     // не забываем прекратить обработку нашего handler (ручки)
	}

	w.WriteHeader(http.StatusNoContent) // уведомляем что сервер успешно обработал запрос
}

func (p *storeHandlers) OrderInventory(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		err       error
		inventory models.Inventory
	)

	inventory = p.storeService.Inventory()

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(inventory)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}
