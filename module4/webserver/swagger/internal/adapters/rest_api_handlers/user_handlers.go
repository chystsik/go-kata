package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/models"
	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/service"

	"github.com/go-chi/chi"
)

type UserHandlers interface {
	UserCreate(w http.ResponseWriter, r *http.Request)
	UserCreateList(w http.ResponseWriter, r *http.Request)
	UserGetByName(w http.ResponseWriter, r *http.Request)
	UserUpdate(w http.ResponseWriter, r *http.Request)
	UserDelete(w http.ResponseWriter, r *http.Request)
}

type userHandlers struct {
	userService service.UserService
}

func NewUserHandlers(us service.UserService) UserHandlers {
	return &userHandlers{userService: us}
}

func (p *userHandlers) UserCreate(w http.ResponseWriter, r *http.Request) {
	var (
		user models.User
		err  error
	)

	err = json.NewDecoder(r.Body).Decode(&user) // считываем приходящий json из *http.Request в структуру models.User
	if err != nil {                             // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	user, err = p.userService.CreateUser(user) // создаем запись в нашем storage
	if err != nil {                            // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(user)                            // записываем результат models.User json в http.ResponseWriter
	if err != nil {                                                  // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *userHandlers) UserCreateList(w http.ResponseWriter, r *http.Request) {
	var (
		users []models.User
		err   error
	)

	err = json.NewDecoder(r.Body).Decode(&users) // считываем приходящий json из *http.Request в структуру Users
	if err != nil {                              // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	for i := range users {
		users[i], err = p.userService.CreateUser(users[i]) // создаем запись в нашем storage
		if err != nil {                                    // в случае ошибки отправляем ошибку Bad request code 400
			http.Error(w, err.Error(), http.StatusBadRequest)
			return // не забываем прекратить обработку нашего handler (ручки)
		}
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(users)                           // записываем результат models.User json в http.ResponseWriter
	if err != nil {                                                  // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *userHandlers) UserGetByName(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		user     models.User
		err      error
		userName string
	)

	userName = chi.URLParam(r, "userName") // получаем userName из chi router

	user, err = p.userService.GetUserByName(userName) // пытаемся получить models.User по userName
	if err != nil {                                   // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *userHandlers) UserUpdate(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		user     models.User
		userName string
		err      error
	)

	userName = chi.URLParam(r, "userName")

	err = json.NewDecoder(r.Body).Decode(&user) // считываем приходящий json из *http.Request в структуру models.User
	if err != nil {                             // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	user.Username = userName // обновляем поле UserName

	user, err = p.userService.UpdateUserByName(user) // обновляем запись в нашем storage
	if err != nil {
		if err.Error() == fmt.Sprintf("user with username %s not found", userName) {
			http.Error(w, err.Error(), http.StatusNotFound)
			return // не забываем прекратить обработку нашего handler (ручки)
		}
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(user)                            // записываем результат models.User json в http.ResponseWriter
	if err != nil {                                                  // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (p *userHandlers) UserDelete(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		err      error
		userName string
	)

	userName = chi.URLParam(r, "userName") // получаем userName из chi router

	err = p.userService.DeleteUserByName(userName) // удаляем запись из storage
	if err != nil {
		if err.Error() == fmt.Sprintf("user with username %s not found", userName) {
			http.Error(w, err.Error(), http.StatusNotFound) // отправляем ошибку NotFound, если не нашли models.User
			return                                          // не забываем прекратить обработку нашего handler (ручки)
		}
		http.Error(w, err.Error(), http.StatusInternalServerError) // отправляем ServerError во всех остальных случаях
		return                                                     // не забываем прекратить обработку нашего handler (ручки)
	}

	w.WriteHeader(http.StatusNoContent) // уведомляем что сервер успешно обработал запрос
}
