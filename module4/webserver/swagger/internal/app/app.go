package app

import (
	"context"
	"errors"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/adapters"
	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/infrastructure/repository"
	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/service"
	restapi "gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/transport/rest_api"
)

const (
	port = 8080

	errHttpListen   = "Failed to listen and serve (http): %v"
	errHttpShutdown = "HTTP server shutdown error: %v"

	logHttpStop        = "HTTP Server: stopped serving new connections..."
	logHttpShutdown    = "Graceful shutdown of HTTP Server complete."
	logSignalInterrupt = "Interrupt signal. Shutdown"
)

func Run() {

	// Initialising repository, service and handlers
	petRepository := repository.NewPetStorage("./pet.json")
	storeRepository := repository.NewStoreStorage("./store.json")
	userRepository := repository.NewUserStorage("./user.json")

	petService := service.NewPetService(petRepository)
	storeService := service.NewStoreService(storeRepository)
	userService := service.NewUserService(userRepository)

	handlers := adapters.NewServiceHandlers(petService, storeService, userService)

	// Startup http server with handlers
	server := restapi.NewServer("", port, handlers)
	go func() {
		log.Printf("server started on port %d \n", port)
		if err := server.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf(errHttpListen, err)
		}
		log.Println(logHttpStop)
	}()

	// Graceful shutdown setup
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	<-quit
	log.Println(logSignalInterrupt)
	ctxShutdown, shutdown := context.WithTimeout(context.Background(), 5*time.Second)
	defer shutdown()

	// Graceful shutdown HTTP Server
	if err := server.Shutdown(ctxShutdown); err != nil {
		log.Fatalf(errHttpShutdown, err)
	}
	log.Println(logHttpShutdown)
}
