package repository

import (
	"encoding/json"
	"io"
	"os"
)

type FileDB struct {
	filePath string
}

func (fdb *FileDB) Read(data interface{}) error {
	file, err := os.OpenFile(fdb.filePath, os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		return err
	}
	defer file.Close()

	fileData, err := io.ReadAll(file)
	if err != nil {
		return err
	}

	if len(fileData) == 0 {
		return nil
	}

	return json.Unmarshal(fileData, &data)
}

func (fdb *FileDB) Write(data interface{}) error {
	fileData, err := json.Marshal(data)
	if err != nil {
		return err
	}

	return os.WriteFile(fdb.filePath, fileData, 0644)
}
