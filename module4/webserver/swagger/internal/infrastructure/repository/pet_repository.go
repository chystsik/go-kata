package repository

import (
	"fmt"
	"sync"

	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/models"
)

const (
	errPetNotFound = "pet with id %d not found"
)

type PetStorager interface {
	Create(pet models.Pet) models.Pet          // create
	Update(pet models.Pet) (models.Pet, error) // update
	Delete(petID int) error                    // delete
	Get(petID int) (models.Pet, error)         // getting by ID
	GetList(petStatus []string) []models.Pet   // getting list by status
}

type PetStorage struct {
	data               []*models.Pet
	primaryKeyIDx      map[int]*models.Pet
	autoIncrementCount int
	sync.Mutex
	FileDB
}

func NewPetStorage(filePath string) PetStorager {
	var fdb = FileDB{filePath: filePath}
	var data = make([]*models.Pet, 0)
	var hashData = make(map[int]*models.Pet, len(data))
	var initId int

	err := fdb.Read(&data)
	if err != nil {
		panic(err)
	}

	if len(data) > 0 {
		initId = data[len(data)-1].ID

		for i := range data {
			hashData[data[i].ID] = data[i]
		}
	}

	return &PetStorage{
		FileDB:             fdb,
		data:               data,
		primaryKeyIDx:      hashData,
		autoIncrementCount: initId,
	}
}

func (p *PetStorage) Create(pet models.Pet) models.Pet {
	p.Mutex.Lock()
	defer p.Mutex.Unlock()

	p.autoIncrementCount++
	pet.ID = p.autoIncrementCount

	p.data = append(p.data, &pet)
	p.primaryKeyIDx[pet.ID] = &pet

	err := p.FileDB.Write(p.data)
	if err != nil {
		panic(err)
	}

	return pet
}

func (p *PetStorage) Update(pet models.Pet) (models.Pet, error) {
	p.Mutex.Lock()
	defer p.Mutex.Unlock()

	if _, ok := p.primaryKeyIDx[pet.ID]; !ok {
		return models.Pet{}, fmt.Errorf(errPetNotFound, pet.ID)
	}

	p.primaryKeyIDx[pet.ID] = &pet
	for i := range p.data {
		if p.data[i].ID == pet.ID {
			p.data[i] = &pet
		}
	}

	err := p.FileDB.Write(p.data)
	if err != nil {
		return models.Pet{}, err
	}

	return *p.primaryKeyIDx[pet.ID], nil
}

func (p *PetStorage) Delete(petID int) error {
	p.Mutex.Lock()
	defer p.Mutex.Unlock()

	if _, ok := p.primaryKeyIDx[petID]; !ok {
		return fmt.Errorf(errPetNotFound, petID)
	}

	for i := range p.data {
		if p.data[i].ID == petID {
			p.data = append(p.data[:i], p.data[i+1:]...)
			break
		}
	}
	delete(p.primaryKeyIDx, petID)

	return p.FileDB.Write(p.data)
}

func (p *PetStorage) Get(petID int) (models.Pet, error) {
	p.Mutex.Lock()
	defer p.Mutex.Unlock()

	if pet, ok := p.primaryKeyIDx[petID]; ok {
		return *pet, nil
	}

	return models.Pet{}, fmt.Errorf(errPetNotFound, petID)
}

func (p *PetStorage) GetList(petStatus []string) []models.Pet {
	p.Mutex.Lock()
	defer p.Mutex.Unlock()
	hash := make(map[string]bool)

	for i := range petStatus {
		hash[petStatus[i]] = true
	}

	var pets = make([]models.Pet, 0)

	for i := range p.data {
		if hash[p.data[i].Status] {
			pets = append(pets, *p.data[i])
		}
	}

	return pets
}
