package repository

import (
	"fmt"
	"os"
	"reflect"
	"testing"

	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/models"

	"github.com/stretchr/testify/assert"
)

func TestPetStorage_Create(t *testing.T) {
	t.Parallel()

	var testDb string = "test_pet_create.json"
	pet := getTestPet()

	p := NewPetStorage(testDb)
	defer os.Remove(testDb)

	expected := p.Create(pet)
	actual, err := p.Get(1)

	assert.NoError(t, err)
	assert.Equal(t, expected, actual)
}

func TestPetStorage_New_OpenFilePanics(t *testing.T) {
	t.Parallel()

	expPanicFn := func() { NewPetStorage("") }

	assert.Panics(t, expPanicFn)
}

func TestPetStorage_New_ReadSlice(t *testing.T) {
	t.Parallel()

	var testDb string = "test_pet_read_slice.json"
	var fdb FileDB
	fdb.filePath = testDb

	pets := getTestPets(10)
	err := fdb.Write(pets)

	p := NewPetStorage(testDb)
	defer os.Remove(testDb)

	assert.NoError(t, err)
	assert.NotNil(t, p)
}

func TestPetStorage_GetById_ReturnsNoError(t *testing.T) {
	t.Parallel()

	var testDb string = "test_pet_getById.json"
	pet := getTestPet()

	p := NewPetStorage(testDb)
	defer os.Remove(testDb)

	expected := p.Create(pet)
	actual, err := p.Get(1)

	assert.NoError(t, err)
	assert.Equal(t, expected, actual)
}

func TestPetStorage_GetById_ReturnsNotFoundError(t *testing.T) {
	t.Parallel()

	var testDb string = "test_pet_getById_err.json"
	pet := getTestPet()

	p := NewPetStorage(testDb)
	defer os.Remove(testDb)

	_ = p.Create(pet)
	notFoundPetId := 4
	actual, err := p.Get(notFoundPetId)

	assert.Error(t, err)
	assert.Equal(t, err, fmt.Errorf(errPetNotFound, notFoundPetId))
	assert.Equal(t, models.Pet{}, actual)
}

func TestPetStorage_Update(t *testing.T) {
	t.Parallel()

	var testDb string = "test_pet_update.json"
	pets := getTestPets(10)

	p := NewPetStorage(testDb)
	defer os.Remove(testDb)

	for i := range pets {
		_ = p.Create(pets[i])
	}

	updatePetId := 4
	expected := pets[updatePetId]
	expected.Name = "newPetName"
	pets[updatePetId].Name = "newPetName"

	actual, errUpdate := p.Update(pets[updatePetId])

	assert.NoError(t, errUpdate)
	assert.Equal(t, expected, actual)

	notFoundPetId := 40
	notFoundPet, errNotFound := p.Update(models.Pet{ID: notFoundPetId})

	assert.Error(t, errNotFound)
	assert.Equal(t, errNotFound, fmt.Errorf(errPetNotFound, notFoundPetId))
	assert.Equal(t, models.Pet{}, notFoundPet)
}

func TestPetStorage_Delete_GetList(t *testing.T) {
	t.Parallel()

	var testDb string = "test_pet_delete_getList.json"
	pets := getTestPets(10)

	p := NewPetStorage(testDb)
	defer os.Remove(testDb)

	for i := range pets {
		_ = p.Create(pets[i])
	}

	deletePetId := 4
	expected := append(pets[:deletePetId-1], pets[deletePetId:]...)

	errDelete := p.Delete(deletePetId)
	actual := p.GetList([]string{"active"})

	assert.NoError(t, errDelete)
	assert.True(t, reflect.DeepEqual(expected, actual))

	notFoundPetId := 40
	errNotFound := p.Delete(notFoundPetId)

	assert.Error(t, errNotFound)
	assert.Equal(t, errNotFound, fmt.Errorf(errPetNotFound, notFoundPetId))
}

func getTestPet() models.Pet {
	return models.Pet{
		ID: 0,
		Category: models.Category{
			ID:   0,
			Name: "test category",
		},
		Name: "Alma",
		PhotoUrls: []string{
			"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
			"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
			"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
		},
		Tags:   []models.Category{},
		Status: "active",
	}
}

func getTestPets(count int) []models.Pet {
	var pets = make([]models.Pet, count)

	for i := range pets {
		pets[i] = getTestPet()
		pets[i].ID = i + 1
	}

	return pets
}
