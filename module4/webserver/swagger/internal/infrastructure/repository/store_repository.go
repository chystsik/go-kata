package repository

import (
	"fmt"
	"sync"

	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/models"
)

const (
	errOrderNotFound = "order with id %d not found"
)

type StoreStorager interface {
	Create(order models.Order) models.Order // Place an order for a pet
	Get(orderID int) (models.Order, error)  // Find purchase order by ID
	Delete(orderID int) error               // Delete purchase order by ID
	Inventory() models.Inventory            // Returns et inventories by status
}

type StoreStorage struct {
	data               []*models.Order
	primaryKeyIDx      map[int]*models.Order
	autoIncrementCount int
	sync.Mutex
	FileDB
}

func NewStoreStorage(filePath string) StoreStorager {
	var fdb = FileDB{filePath: filePath}
	var data = make([]*models.Order, 0)
	var hashData = make(map[int]*models.Order, len(data))
	var initId int

	err := fdb.Read(&data)
	if err != nil {
		panic(err)
	}

	if len(data) > 0 {
		initId = data[len(data)-1].ID

		for i := range data {
			hashData[data[i].ID] = data[i]
		}
	}

	return &StoreStorage{
		FileDB:             fdb,
		data:               data,
		primaryKeyIDx:      hashData,
		autoIncrementCount: initId,
	}
}

func (ss *StoreStorage) Create(order models.Order) models.Order {
	ss.Mutex.Lock()
	defer ss.Mutex.Unlock()

	ss.autoIncrementCount++
	order.ID = ss.autoIncrementCount

	ss.data = append(ss.data, &order)
	ss.primaryKeyIDx[order.ID] = &order

	err := ss.FileDB.Write(ss.data)
	if err != nil {
		panic(err)
	}

	return order
}

func (ss *StoreStorage) Get(orderID int) (models.Order, error) {
	ss.Mutex.Lock()
	defer ss.Mutex.Unlock()

	if order, ok := ss.primaryKeyIDx[orderID]; ok {
		return *order, nil
	}

	return models.Order{}, fmt.Errorf(errOrderNotFound, orderID)
}

func (ss *StoreStorage) Delete(orderID int) error {
	ss.Mutex.Lock()
	defer ss.Mutex.Unlock()

	if _, ok := ss.primaryKeyIDx[orderID]; !ok {
		return fmt.Errorf(errOrderNotFound, orderID)
	}

	for i := range ss.data {
		if ss.data[i].ID == orderID {
			ss.data = append(ss.data[:i], ss.data[i+1:]...)
			break
		}
	}
	delete(ss.primaryKeyIDx, orderID)

	return ss.FileDB.Write(ss.data)
}

func (ss *StoreStorage) Inventory() models.Inventory {
	result := make(map[string]int)

	for _, order := range ss.data {
		result[order.Status.Status] += order.Quantity
	}

	return result
}
