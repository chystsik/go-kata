package repository

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/models"
)

func TestOrderStorage_New_OpenFilePanics(t *testing.T) {
	t.Parallel()

	expPanicFn := func() { NewStoreStorage("") }

	assert.Panics(t, expPanicFn)
}

func TestOrderStorage_New_ReadSlice(t *testing.T) {
	t.Parallel()

	var testDb string = "test_order_read_slice.json"
	var fdb FileDB
	fdb.filePath = testDb

	orders := getTestOrders(10)
	err := fdb.Write(orders)

	o := NewStoreStorage(testDb)
	defer os.Remove(testDb)

	assert.NoError(t, err)
	assert.NotNil(t, o)
}

func TestOrderStorage_CreateOrder(t *testing.T) {
	t.Parallel()

	var testDb string = "test_order_create.json"
	order := getTestOrder()

	o := NewStoreStorage(testDb)
	defer os.Remove(testDb)

	expected := o.Create(order)
	actual, err := o.Get(1)

	assert.NoError(t, err)
	assert.Equal(t, expected, actual)
}

func TestOrderStorage_GetOrderById(t *testing.T) {
	t.Parallel()

	var testDb string = "test_order_getById.json"
	order := getTestOrder()

	o := NewStoreStorage(testDb)
	defer os.Remove(testDb)

	expected := o.Create(order)
	actual, err := o.Get(1)

	assert.NoError(t, err)
	assert.Equal(t, expected, actual)

	actual, err = o.Get(11)

	assert.Error(t, err)
	assert.Equal(t, models.Order{}, actual)
}

func TestOrderStorage_DeleteOrderById(t *testing.T) {
	t.Parallel()

	var testDb string = "test_order_deleteById.json"
	order := getTestOrder()

	o := NewStoreStorage(testDb)
	defer os.Remove(testDb)

	err := o.Delete(1)

	assert.Error(t, err)

	_ = o.Create(order)
	err = o.Delete(1)

	assert.NoError(t, err)
}

func TestOrderStorage_Inventory(t *testing.T) {
	t.Parallel()

	var testDb string = "test_order_inventory.json"
	orders := getTestOrders(10)

	o := NewStoreStorage(testDb)
	defer os.Remove(testDb)

	expectedSum := 0
	actualStatus := "pending"

	for i := range orders {
		orders[i].Quantity = i
		orders[i].Status = models.Status{Status: actualStatus}
		expectedSum += i
		_ = o.Create(orders[i])
	}

	actual := o.Inventory()

	assert.Equal(t, expectedSum, actual[actualStatus])
}

func getTestOrder() models.Order {
	return models.Order{
		ID:       0,
		PetID:    1,
		Quantity: 12,
		ShipDate: "10.02.2024",
		Status:   models.Status{Status: "pending"},
		Complete: false,
	}
}

func getTestOrders(count int) []models.Order {
	var orders = make([]models.Order, count)

	for i := range orders {
		orders[i] = getTestOrder()
		orders[i].ID = i + 1
	}

	return orders
}
