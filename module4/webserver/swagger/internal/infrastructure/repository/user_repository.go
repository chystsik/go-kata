package repository

import (
	"fmt"
	"sync"

	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/models"
)

const (
	errUserNotFound = "user with username %s not found"
	errUserExists   = "user with username %s already exists in the repository"
)

type UserStorager interface {
	Create(user models.User) (models.User, error) // Create user
	Get(name string) (models.User, error)         // Get user by Name
	Update(user models.User) (models.User, error) // Update user by name
	Delete(name string) error                     // Delete user by Name
}

type UserStorage struct {
	data               []*models.User
	primaryKeyIDx      map[string]*models.User
	autoIncrementCount int
	sync.Mutex
	FileDB
}

func NewUserStorage(filePath string) UserStorager {
	var fdb = FileDB{filePath: filePath}
	var data = make([]*models.User, 0)
	var hashData = make(map[string]*models.User, len(data))
	var initId int

	err := fdb.Read(&data)
	if err != nil {
		panic(err)
	}

	if len(data) > 0 {
		initId = data[len(data)-1].ID

		for i := range data {
			hashData[data[i].Username] = data[i]
		}
	}

	return &UserStorage{
		FileDB:             fdb,
		data:               data,
		primaryKeyIDx:      hashData,
		autoIncrementCount: initId,
	}
}

func (us *UserStorage) Create(user models.User) (models.User, error) {
	us.Mutex.Lock()
	defer us.Mutex.Unlock()

	if _, ok := us.primaryKeyIDx[user.Username]; ok {
		return models.User{}, fmt.Errorf(errUserExists, user.Username)
	}

	us.autoIncrementCount++
	user.ID = us.autoIncrementCount

	us.data = append(us.data, &user)
	us.primaryKeyIDx[user.Username] = &user

	err := us.FileDB.Write(us.data)
	if err != nil {
		panic(err)
	}

	return user, nil
}

func (us *UserStorage) Get(name string) (models.User, error) {
	us.Mutex.Lock()
	defer us.Mutex.Unlock()

	if user, ok := us.primaryKeyIDx[name]; ok {
		return *user, nil
	}

	return models.User{}, fmt.Errorf(errUserNotFound, name)
}

func (us *UserStorage) Update(user models.User) (models.User, error) {
	us.Mutex.Lock()
	defer us.Mutex.Unlock()

	if _, ok := us.primaryKeyIDx[user.Username]; !ok {
		return models.User{}, fmt.Errorf(errUserNotFound, user.Username)
	}

	id := us.primaryKeyIDx[user.Username].ID

	us.primaryKeyIDx[user.Username] = &user
	us.primaryKeyIDx[user.Username].ID = id
	for i := range us.data {
		if us.data[i].Username == user.Username {
			us.data[i] = &user
			us.data[i].ID = id
		}
	}

	err := us.FileDB.Write(us.data)
	if err != nil {
		return models.User{}, err
	}

	return *us.primaryKeyIDx[user.Username], nil
}

func (us *UserStorage) Delete(name string) error {
	us.Mutex.Lock()
	defer us.Mutex.Unlock()

	if _, ok := us.primaryKeyIDx[name]; !ok {
		return fmt.Errorf(errUserNotFound, name)
	}

	for i := range us.data {
		if us.data[i].Username == name {
			us.data = append(us.data[:i], us.data[i+1:]...)
			break
		}
	}
	delete(us.primaryKeyIDx, name)

	return us.FileDB.Write(us.data)
}
