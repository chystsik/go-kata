package repository

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/models"
)

func TestUserStorage_New_OpenFilePanics(t *testing.T) {
	t.Parallel()

	expPanicFn := func() { NewUserStorage("") }

	assert.Panics(t, expPanicFn)
}

func TestUserStorage_New_ReadSlice(t *testing.T) {
	t.Parallel()

	var testDb string = "test_user_read_slice.json"
	var fdb FileDB
	fdb.filePath = testDb

	users := getTestUsers(10)
	err := fdb.Write(users)

	u := NewUserStorage(testDb)
	defer os.Remove(testDb)

	assert.NoError(t, err)
	assert.NotNil(t, u)
}

func TestUserStorage_CreateUser(t *testing.T) {
	t.Parallel()

	var testDb string = "test_user_create.json"
	user := getTestUser()

	u := NewUserStorage(testDb)
	defer os.Remove(testDb)

	expected, errCreate := u.Create(user)
	actual, err := u.Get(user.Username)

	assert.NoError(t, errCreate)
	assert.NoError(t, err)
	assert.Equal(t, expected, actual)
}

func TestUserStorage_GetUserByName(t *testing.T) {
	t.Parallel()

	var testDb string = "test_user_getByName.json"
	user := getTestUser()

	u := NewUserStorage(testDb)
	defer os.Remove(testDb)

	expected, errCreate := u.Create(user)
	actual, err := u.Get(expected.Username)

	assert.NoError(t, errCreate)
	assert.NoError(t, err)
	assert.Equal(t, expected, actual)

	actual, err = u.Get("11")

	assert.Error(t, err)
	assert.Equal(t, models.User{}, actual)
}

func TestUserStorage_UpdateUserByName(t *testing.T) {
	t.Parallel()

	var testDb string = "test_user_updateByName.json"
	user := getTestUser()

	u := NewUserStorage(testDb)
	defer os.Remove(testDb)

	expected, errCreate := u.Create(user)
	actual, err := u.Update(expected)

	assert.NoError(t, errCreate)
	assert.NoError(t, err)
	assert.Equal(t, expected, actual)

	actual, err = u.Update(models.User{})

	assert.Error(t, err)
	assert.Equal(t, models.User{}, actual)
}

func TestUserStorage_DeleteUserByName(t *testing.T) {
	t.Parallel()

	var testDb string = "test_user_deleteByName.json"
	user := getTestUser()

	u := NewUserStorage(testDb)
	defer os.Remove(testDb)

	expected, errCreate := u.Create(user)
	err := u.Delete(expected.Username)

	assert.NoError(t, errCreate)
	assert.NoError(t, err)

	err = u.Delete("11")

	assert.Error(t, err)
}

func getTestUser() models.User {
	return models.User{
		Username:   "testUsername",
		FirstName:  "testFirstName",
		LastName:   "testLastName",
		Email:      "testEmail",
		Password:   "testPassword",
		Phone:      "testPhone",
		UserStatus: 101,
	}
}

func getTestUsers(count int) []models.User {
	var users = make([]models.User, count)

	for i := range users {
		users[i] = getTestUser()
		users[i].ID = i + 1
	}

	return users
}
