// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    pet, err := UnmarshalPet(bytes)
//    bytes, err = pet.Marshal()

package models

import (
	"encoding/json"
)

func UnmarshalPet(data []byte) (Pet, error) {
	var r Pet
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Pet) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Pet struct {
	ID        int        `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    string     `json:"status"`
}

type Category struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
