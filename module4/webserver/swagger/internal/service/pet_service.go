package service

import (
	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/infrastructure/repository"
	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/models"
)

type PetService interface {
	CreatePet(pet models.Pet) models.Pet
	UpdatePet(pet models.Pet) (models.Pet, error)
	DeletePet(petID int) error
	GetPetByID(petID int) (models.Pet, error)
	GetPetListByStatus(petStatus []string) []models.Pet
}

type petService struct {
	petRepo repository.PetStorager
}

func NewPetService(pr repository.PetStorager) PetService {
	return &petService{petRepo: pr}
}

func (ps *petService) CreatePet(pet models.Pet) models.Pet {
	return ps.petRepo.Create(pet)
}

func (ps *petService) UpdatePet(pet models.Pet) (models.Pet, error) {
	result, err := ps.petRepo.Update(pet)
	if err != nil {
		return models.Pet{}, err
	}

	return result, nil
}

func (ps *petService) DeletePet(petID int) error {
	return ps.petRepo.Delete(petID)
}

func (ps *petService) GetPetByID(petID int) (models.Pet, error) {
	result, err := ps.petRepo.Get(petID)
	if err != nil {
		return models.Pet{}, err
	}

	return result, nil
}

func (ps *petService) GetPetListByStatus(petStatus []string) []models.Pet {
	return ps.petRepo.GetList(petStatus)
}
