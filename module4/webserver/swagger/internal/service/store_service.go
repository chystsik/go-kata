package service

import (
	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/infrastructure/repository"
	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/models"
)

type StoreService interface {
	CreateOrder(order models.Order) models.Order
	GetOrderByID(orderID int) (models.Order, error)
	DeleteOrderByID(orderID int) error
	Inventory() models.Inventory
}

type storeService struct {
	storeRepo repository.StoreStorager
}

func NewStoreService(sr repository.StoreStorager) StoreService {
	return &storeService{storeRepo: sr}
}

func (ss *storeService) CreateOrder(order models.Order) models.Order {
	return ss.storeRepo.Create(order)
}

func (ss *storeService) GetOrderByID(orderID int) (models.Order, error) {
	result, err := ss.storeRepo.Get(orderID)
	if err != nil {
		return models.Order{}, err
	}

	return result, nil
}

func (ss *storeService) DeleteOrderByID(orderID int) error {
	return ss.storeRepo.Delete(orderID)
}

func (ss *storeService) Inventory() models.Inventory {
	return ss.storeRepo.Inventory()
}
