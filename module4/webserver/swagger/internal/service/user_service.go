package service

import (
	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/infrastructure/repository"
	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/models"
)

type UserService interface {
	CreateUser(user models.User) (models.User, error)
	GetUserByName(name string) (models.User, error)
	UpdateUserByName(user models.User) (models.User, error)
	DeleteUserByName(name string) error
}

type userService struct {
	userRepo repository.UserStorager
}

func NewUserService(ur repository.UserStorager) UserService {
	return &userService{userRepo: ur}
}

func (us *userService) CreateUser(user models.User) (models.User, error) {
	return us.userRepo.Create(user)
}

func (us *userService) GetUserByName(name string) (models.User, error) {
	result, err := us.userRepo.Get(name)
	if err != nil {
		return models.User{}, err
	}

	return result, nil
}

func (us *userService) UpdateUserByName(user models.User) (models.User, error) {
	result, err := us.userRepo.Update(user)
	if err != nil {
		return models.User{}, err
	}

	return result, nil
}

func (us *userService) DeleteUserByName(name string) error {
	return us.userRepo.Delete(name)
}
