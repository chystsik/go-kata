package restapi

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/adapters"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func NewServer(host string, port int, handlers adapters.ServiceHandlers) *http.Server {
	// Set up a basic chi router
	router := chi.NewRouter()

	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)

	// Register handlers
	registerHandlers(router, handlers)

	Server := &http.Server{
		Addr:           fmt.Sprintf("%s:%d", host, port),
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	return Server
}
