package restapi

import (
	"net/http"

	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/adapters"

	"github.com/go-chi/chi"
)

func registerHandlers(r *chi.Mux, handlers adapters.ServiceHandlers) {
	// pet handlers
	r.Post("/pet/{petId}/uploadImage", handlers.PetUploadImage)
	r.Post("/pet", handlers.PetCreate)
	r.Put("/pet", handlers.PetUpdate)
	r.Get("/pet/findByStatus", handlers.PetGetByStatus)
	r.Get("/pet/{petId}", handlers.PetGetByID)
	r.Post("/pet/{petId}", handlers.PetUpdateWithForm)
	r.Delete("/pet/{petId}", handlers.PetDelete)

	// store handlers
	r.Post("/store/order", handlers.OrderCreate)
	r.Get("/store/order/{orderId}", handlers.OrderGetById)
	r.Delete("/store/order/{orderId}", handlers.OrderDelete)
	r.Get("/store/inventory", handlers.OrderInventory)

	// user handlers
	r.Post("/user/createWithArray", handlers.UserCreateList)
	r.Post("/user/createWithList", handlers.UserCreateList)
	r.Get("/user/{userName}", handlers.UserGetByName)
	r.Put("/user/{userName}", handlers.UserUpdate)
	r.Delete("/user/{userName}", handlers.UserDelete)
	r.Post("/user", handlers.UserCreate)

	//SwaggerUI
	r.Get("/swagger", handlers.SwaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})

}
