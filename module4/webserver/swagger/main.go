package main

import (
	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/app"
)

func main() {
	app.Run()
}
