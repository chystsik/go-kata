package main

import (
	"bytes"

	"gitlab.com/chystsik/go-kata/module4/webserver/swagger/internal/models"
)

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /pet pet petAddRequest
// Добавление питомца.
// responses:
//   200: petAddResponse
//   400: description: Invalid input

// swagger:parameters petAddRequest
type petAddRequest struct {
	// Объект Pet, который нужно добавить в репозиторий
	//
	// in:body
	// required:true
	Body models.Pet
}

// Успешное добавление
// swagger:response petAddResponse
type petAddResponse struct {
	// in:body
	Body models.Pet
}

// swagger:route GET /pet/{petId} pet petGetByIDRequest
// Получение питомца по id.
//
// Возвращает одного питомца
// responses:
//   200: petGetByIDResponse
//   400: description: Invalid input
//   404: description: Not found

// swagger:parameters petGetByIDRequest
type petGetByIDRequest struct {
	// ID объекта Pet
	//
	// in:path
	// required:true
	ID string `json:"petId"`
}

// swagger:response petGetByIDResponse
type petGetByIDResponse struct {
	// in:body
	Body models.Pet
}

// swagger:route POST /pet/{petId}/uploadImage pet petUploadImageRequest
// Добавление фото питомца
//
// Возвращает одного питомца
// responses:
//   200: petUploadImageResponse
//   400: description: Invalid input
//   404: description: Not found

// swagger:parameters petUploadImageRequest
type petUploadImageRequest struct {
	// ID объекта Pet
	//
	// in:path
	// required:true
	ID string `json:"petId"`
	// Файл .xml для загрузки
	//
	// in:formData
	//
	// swagger:file
	File *bytes.Buffer `json:"file"`
}

// swagger:response petUploadImageResponse
type petUploadImageResponse struct {
	// in:body
	Body models.Pet
}

// swagger:route PUT /pet pet petUpdateRequest
// Обновление питомца
//
// Возвращает одного питомца
// responses:
//   200: petUpdateResponse
//   400: description: Invalid input
//   404: description: Not found

// swagger:parameters petUpdateRequest
type petUpdateRequest struct {
	// Объект Pet, который нужно обновить в репозитории
	//
	// in:body
	// required:true
	Body models.Pet
}

// swagger:response petUpdateResponse
type petUpdateResponse struct {
	// in:body
	Body models.Pet
}

// swagger:route GET /pet/findByStatus pet petFindByStatusRequest
// Поиск питомца по статусу
//
// Возвращает список питомцев
// responses:
//   200: petFindByStatusResponse
//   400: description: Invalid input
//   404: description: Not found

// swagger:parameters petFindByStatusRequest
type petFindByStatusRequest struct {
	// Значения статусов списком
	//
	// collectionFormat:multi
	// in:query
	// enum:available,pending,sold
	// required:true
	Status []string `json:"status"`
}

// swagger:response petFindByStatusResponse
type petFindByStatusResponse struct {
	// in:body
	Body models.Pet
}

// swagger:route POST /pet/{petId} pet petUpdateWithFormRequest
// Обновление питомца через formData
//
// Возвращает питомца
//
// Consumes:
//     - multipart/form-data
//     - application/x-www-form-urlencoded
// responses:
//   200: petUpdateWithFormResponse
//   400: description: Invalid input
//   404: description: Not found

// swagger:parameters petUpdateWithFormRequest
type petUpdateWithFormRequest struct {
	// ID объекта Pet
	//
	// in:path
	// required:true
	ID string `json:"petId"`
	// Новое имя для питомца
	//
	// in:formData
	Name string `json:"name"`
	// Новый статус для питомца
	//
	// in:formData
	Status string `json:"status"`
}

// swagger:response petUpdateWithFormResponse
type petUpdateWithFormResponse struct {
	// in:body
	Body models.Pet
}

// swagger:route DELETE /pet/{petId} pet petDeleteRequest
// Удаление питомца по id
//
// Удаляет питомца из БД
// responses:
//   204: description: Успешная операция
//   400: description: Invalid input
//   404: description: Not found

// swagger:parameters petDeleteRequest
type petDeleteRequest struct {
	// Ключ API
	//
	// in:header
	// required:true
	ApiKey string `json:"api_key"`
	// ID объекта Pet
	//
	// in:path
	// required:true
	ID string `json:"petId"`
}

// swagger:route POST /store/order order orderAddRequest
// Добавление заказа
//
// Возвращает заказ
// responses:
//	200: body:Order Успешное добавление
//	400: description: Invalid input

// swagger:parameters orderAddRequest
type orderAddRequest struct {
	// Объект Order, который нужно добавить в репозиторий
	//
	// in:body
	// required:true
	Body models.Order
}

// swagger:route GET /store/order/{orderId} order orderGetByIDRequest
// Получение заказа по id
//
// Возвращает заказ
// responses:
//   200: body:Order Успешная операция
//   400: description: Invalid input
//   404: description: Not found

// swagger:parameters orderGetByIDRequest
type orderGetByIDRequest struct {
	// ID объекта Order
	//
	// in:path
	// required:true
	ID string `json:"orderId"`
}

// swagger:route DELETE /store/order/{orderId} order orderDeleteRequest
// Удаление заказа по id
//
// Удаляет заказ. Вводить только цифры
// responses:
//   204: description: Успешная операция
//   400: description: Invalid input
//   404: description: Not found

// swagger:parameters orderDeleteRequest
type orderDeleteRequest struct {
	// ID объекта Order
	//
	// in:path
	// required:true
	ID string `json:"orderId"`
}

// swagger:route GET /store/inventory order request
// Инвентаризация магаза
//
// Возвращает список соответствия статуса зазаза его количеству
// responses:
//   200: orderInventoryResponse

// Успешная операция
// swagger:response orderInventoryResponse
type orderInventoryResponse struct {
	// in:body
	Body models.Inventory
}

// swagger:route POST /user user userAddRequest
// Добавление клиента
//
// Возвращает клиента
// responses:
//	200: body:User Успешное добавление
//	400: description: Invalid input

// swagger:parameters userAddRequest
type userAddRequest struct {
	// Объект User, который нужно добавить в репозиторий
	//
	// in:body
	// required:true
	Body models.User
}

// swagger:route POST /user/createWithArray user userCreateWithArrayRequest
// Добавление клиентов
//
// Возвращает список клиентов
// responses:
//	200: body:[]User Успешное добавление
//	400: description: Invalid input

// swagger:parameters userCreateWithArrayRequest
type userCreateWithArrayRequest struct {
	// Список клиентов, которых нужно добавить в репозиторий
	//
	// in:body
	// required:true
	Body []models.User
}

// swagger:route POST /user/createWithList user userCreateWithListRequest
// Добавление клиентов
//
// Возвращает список клиентов
// responses:
//	200: body:[]User Успешное добавление
//	400: description: Invalid input

// swagger:parameters userCreateWithListRequest
type userCreateWithListRequest struct {
	// Список клиентов, которых нужно добавить в репозиторий
	//
	// in:body
	// required:true
	Body []models.User
}

// swagger:route GET /user/{userName} user userGetByNameRequest
// Получение клиента по имени
//
// Возвращает клиента
// responses:
//   200: body:User Успешная операция
//   400: description: Invalid input
//   404: description: Not found

// swagger:parameters userGetByNameRequest
type userGetByNameRequest struct {
	// Имя клиента
	//
	// in:path
	// required:true
	UserName string `json:"userName"`
}

// swagger:route PUT /user/{userName} user userUpdateRequest
// Обновление клиента
//
// Возвращает обновленного клиента
// responses:
//   200: body:User Успешное обновление
//   400: description: Invalid input
//   404: description: Not found

// swagger:parameters userUpdateRequest
type userUpdateRequest struct {
	// Имя клиента
	//
	// in:path
	// required:true
	UserName string `json:"userName"`
	// Объект User, который нужно обновить в репозитории
	//
	// in:body
	// required:true
	Body models.User
}

// swagger:route DELETE /user/{userName} user userDeleteRequest
// Удаление клиента по имени
//
// Удаляет клиента
// responses:
//   204: description: Успешная операция
//   400: description: Invalid input
//   404: description: Not found

// swagger:parameters userDeleteRequest
type userDeleteRequest struct {
	// Имя клиента
	//
	// in:path
	// required:true
	UserName string `json:"userName"`
}

// prevent linter warnings
func init() {
	_ = petAddRequest{}
	_ = petAddResponse{}
	_ = petDeleteRequest{}
	_ = petFindByStatusRequest{}
	_ = petFindByStatusResponse{}
	_ = petGetByIDRequest{}
	_ = petGetByIDResponse{}
	_ = petUpdateRequest{}
	_ = petUpdateResponse{}
	_ = petUpdateWithFormRequest{}
	_ = petUpdateWithFormResponse{}
	_ = petUploadImageRequest{}
	_ = petUploadImageResponse{}
	_ = orderAddRequest{}
	_ = orderDeleteRequest{}
	_ = orderGetByIDRequest{}
	_ = orderInventoryResponse{}
	_ = userAddRequest{}
	_ = userCreateWithArrayRequest{}
	_ = userCreateWithListRequest{}
	_ = userDeleteRequest{}
	_ = userGetByNameRequest{}
	_ = userUpdateRequest{}
}
